]# Changelog

All Notable changes to `schema-org` will be documented in this file.

## 3.3.0 - 2021-01-02

- fix `@id` and `identifier` serialization for typed identifiers

## 3.2.1 - 2020-11-28

- add support for PHP 8
