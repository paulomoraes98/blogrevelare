<?php

namespace ApolloCMS\SchemaOrg;

use \ApolloCMS\SchemaOrg\Contracts\RadiationTherapyContract;
use \ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract;
use \ApolloCMS\SchemaOrg\Contracts\MedicalProcedureContract;
use \ApolloCMS\SchemaOrg\Contracts\MedicalTherapyContract;
use \ApolloCMS\SchemaOrg\Contracts\TherapeuticProcedureContract;
use \ApolloCMS\SchemaOrg\Contracts\ThingContract;

/**
 * A process of care using radiation aimed at improving a health condition.
 *
 * @see https://schema.org/RadiationTherapy
 * @see http://health-lifesci.schema.org
 *
 */
class RadiationTherapy extends BaseType implements RadiationTherapyContract, MedicalEntityContract, MedicalProcedureContract, MedicalTherapyContract, TherapeuticProcedureContract, ThingContract
{
    /**
     * An additional type for the item, typically used for adding more specific
     * types from external vocabularies in microdata syntax. This is a
     * relationship between something and a class that the thing is in. In RDFa
     * syntax, it is better to use the native RDFa syntax - the 'typeof'
     * attribute - for multiple types. Schema.org tools may have only weaker
     * understanding of extra types, in particular those defined externally.
     *
     * @param string|string[] $additionalType
     *
     * @return static
     *
     * @see https://schema.org/additionalType
     */
    public function additionalType($additionalType)
    {
        return $this->setProperty('additionalType', $additionalType);
    }

    /**
     * A possible complication and/or side effect of this therapy. If it is
     * known that an adverse outcome is serious (resulting in death, disability,
     * or permanent damage; requiring hospitalization; or is otherwise
     * life-threatening or requires immediate medical attention), tag it as a
     * seriouseAdverseOutcome instead.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract|\ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract[] $adverseOutcome
     *
     * @return static
     *
     * @see https://schema.org/adverseOutcome
     * @see http://health-lifesci.schema.org
     */
    public function adverseOutcome($adverseOutcome)
    {
        return $this->setProperty('adverseOutcome', $adverseOutcome);
    }

    /**
     * An alias for the item.
     *
     * @param string|string[] $alternateName
     *
     * @return static
     *
     * @see https://schema.org/alternateName
     */
    public function alternateName($alternateName)
    {
        return $this->setProperty('alternateName', $alternateName);
    }

    /**
     * Location in the body of the anatomical structure.
     *
     * @param string|string[] $bodyLocation
     *
     * @return static
     *
     * @see https://schema.org/bodyLocation
     * @see http://health-lifesci.schema.org
     */
    public function bodyLocation($bodyLocation)
    {
        return $this->setProperty('bodyLocation', $bodyLocation);
    }

    /**
     * A medical code for the entity, taken from a controlled vocabulary or
     * ontology such as ICD-9, DiseasesDB, MeSH, SNOMED-CT, RxNorm, etc.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalCodeContract|\ApolloCMS\SchemaOrg\Contracts\MedicalCodeContract[] $code
     *
     * @return static
     *
     * @see https://schema.org/code
     * @see http://health-lifesci.schema.org
     */
    public function code($code)
    {
        return $this->setProperty('code', $code);
    }

    /**
     * A contraindication for this therapy.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalContraindicationContract|\ApolloCMS\SchemaOrg\Contracts\MedicalContraindicationContract[]|string|string[] $contraindication
     *
     * @return static
     *
     * @see https://schema.org/contraindication
     * @see http://health-lifesci.schema.org
     */
    public function contraindication($contraindication)
    {
        return $this->setProperty('contraindication', $contraindication);
    }

    /**
     * A description of the item.
     *
     * @param string|string[] $description
     *
     * @return static
     *
     * @see https://schema.org/description
     */
    public function description($description)
    {
        return $this->setProperty('description', $description);
    }

    /**
     * A sub property of description. A short description of the item used to
     * disambiguate from other, similar items. Information from other properties
     * (in particular, name) may be necessary for the description to be useful
     * for disambiguation.
     *
     * @param string|string[] $disambiguatingDescription
     *
     * @return static
     *
     * @see https://schema.org/disambiguatingDescription
     */
    public function disambiguatingDescription($disambiguatingDescription)
    {
        return $this->setProperty('disambiguatingDescription', $disambiguatingDescription);
    }

    /**
     * A dosing schedule for the drug for a given population, either observed,
     * recommended, or maximum dose based on the type used.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\DoseScheduleContract|\ApolloCMS\SchemaOrg\Contracts\DoseScheduleContract[] $doseSchedule
     *
     * @return static
     *
     * @see https://schema.org/doseSchedule
     * @see http://health-lifesci.schema.org
     */
    public function doseSchedule($doseSchedule)
    {
        return $this->setProperty('doseSchedule', $doseSchedule);
    }

    /**
     * Specifying a drug or medicine used in a medication procedure
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\DrugContract|\ApolloCMS\SchemaOrg\Contracts\DrugContract[] $drug
     *
     * @return static
     *
     * @see https://schema.org/drug
     * @see http://health-lifesci.schema.org
     */
    public function drug($drug)
    {
        return $this->setProperty('drug', $drug);
    }

    /**
     * A therapy that duplicates or overlaps this one.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalTherapyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalTherapyContract[] $duplicateTherapy
     *
     * @return static
     *
     * @see https://schema.org/duplicateTherapy
     * @see http://health-lifesci.schema.org
     */
    public function duplicateTherapy($duplicateTherapy)
    {
        return $this->setProperty('duplicateTherapy', $duplicateTherapy);
    }

    /**
     * Typical or recommended followup care after the procedure is performed.
     *
     * @param string|string[] $followup
     *
     * @return static
     *
     * @see https://schema.org/followup
     * @see http://health-lifesci.schema.org
     */
    public function followup($followup)
    {
        return $this->setProperty('followup', $followup);
    }

    /**
     * A medical guideline related to this entity.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalGuidelineContract|\ApolloCMS\SchemaOrg\Contracts\MedicalGuidelineContract[] $guideline
     *
     * @return static
     *
     * @see https://schema.org/guideline
     * @see http://health-lifesci.schema.org
     */
    public function guideline($guideline)
    {
        return $this->setProperty('guideline', $guideline);
    }

    /**
     * How the procedure is performed.
     *
     * @param string|string[] $howPerformed
     *
     * @return static
     *
     * @see https://schema.org/howPerformed
     * @see http://health-lifesci.schema.org
     */
    public function howPerformed($howPerformed)
    {
        return $this->setProperty('howPerformed', $howPerformed);
    }

    /**
     * The identifier property represents any kind of identifier for any kind of
     * [[Thing]], such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides
     * dedicated properties for representing many of these, either as textual
     * strings or as URL (URI) links. See [background
     * notes](/docs/datamodel.html#identifierBg) for more details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\PropertyValueContract|\ApolloCMS\SchemaOrg\Contracts\PropertyValueContract[]|string|string[] $identifier
     *
     * @return static
     *
     * @see https://schema.org/identifier
     */
    public function identifier($identifier)
    {
        return $this->setProperty('identifier', $identifier);
    }

    /**
     * An image of the item. This can be a [[URL]] or a fully described
     * [[ImageObject]].
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ImageObjectContract|\ApolloCMS\SchemaOrg\Contracts\ImageObjectContract[]|string|string[] $image
     *
     * @return static
     *
     * @see https://schema.org/image
     */
    public function image($image)
    {
        return $this->setProperty('image', $image);
    }

    /**
     * The drug or supplement's legal status, including any controlled substance
     * schedules that apply.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\DrugLegalStatusContract|\ApolloCMS\SchemaOrg\Contracts\DrugLegalStatusContract[]|\ApolloCMS\SchemaOrg\Contracts\MedicalEnumerationContract|\ApolloCMS\SchemaOrg\Contracts\MedicalEnumerationContract[]|string|string[] $legalStatus
     *
     * @return static
     *
     * @see https://schema.org/legalStatus
     * @see http://health-lifesci.schema.org
     */
    public function legalStatus($legalStatus)
    {
        return $this->setProperty('legalStatus', $legalStatus);
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main
     * entity being described. See [background
     * notes](/docs/datamodel.html#mainEntityBackground) for details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|string|string[] $mainEntityOfPage
     *
     * @return static
     *
     * @see https://schema.org/mainEntityOfPage
     */
    public function mainEntityOfPage($mainEntityOfPage)
    {
        return $this->setProperty('mainEntityOfPage', $mainEntityOfPage);
    }

    /**
     * The system of medicine that includes this MedicalEntity, for example
     * 'evidence-based', 'homeopathic', 'chiropractic', etc.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicineSystemContract|\ApolloCMS\SchemaOrg\Contracts\MedicineSystemContract[] $medicineSystem
     *
     * @return static
     *
     * @see https://schema.org/medicineSystem
     * @see http://health-lifesci.schema.org
     */
    public function medicineSystem($medicineSystem)
    {
        return $this->setProperty('medicineSystem', $medicineSystem);
    }

    /**
     * The name of the item.
     *
     * @param string|string[] $name
     *
     * @return static
     *
     * @see https://schema.org/name
     */
    public function name($name)
    {
        return $this->setProperty('name', $name);
    }

    /**
     * Indicates a potential Action, which describes an idealized action in
     * which this thing would play an 'object' role.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ActionContract|\ApolloCMS\SchemaOrg\Contracts\ActionContract[] $potentialAction
     *
     * @return static
     *
     * @see https://schema.org/potentialAction
     */
    public function potentialAction($potentialAction)
    {
        return $this->setProperty('potentialAction', $potentialAction);
    }

    /**
     * Typical preparation that a patient must undergo before having the
     * procedure performed.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract|\ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract[]|string|string[] $preparation
     *
     * @return static
     *
     * @see https://schema.org/preparation
     * @see http://health-lifesci.schema.org
     */
    public function preparation($preparation)
    {
        return $this->setProperty('preparation', $preparation);
    }

    /**
     * The type of procedure, for example Surgical, Noninvasive, or
     * Percutaneous.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalProcedureTypeContract|\ApolloCMS\SchemaOrg\Contracts\MedicalProcedureTypeContract[] $procedureType
     *
     * @return static
     *
     * @see https://schema.org/procedureType
     * @see http://health-lifesci.schema.org
     */
    public function procedureType($procedureType)
    {
        return $this->setProperty('procedureType', $procedureType);
    }

    /**
     * If applicable, the organization that officially recognizes this entity as
     * part of its endorsed system of medicine.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\OrganizationContract|\ApolloCMS\SchemaOrg\Contracts\OrganizationContract[] $recognizingAuthority
     *
     * @return static
     *
     * @see https://schema.org/recognizingAuthority
     * @see http://health-lifesci.schema.org
     */
    public function recognizingAuthority($recognizingAuthority)
    {
        return $this->setProperty('recognizingAuthority', $recognizingAuthority);
    }

    /**
     * If applicable, a medical specialty in which this entity is relevant.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalSpecialtyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalSpecialtyContract[] $relevantSpecialty
     *
     * @return static
     *
     * @see https://schema.org/relevantSpecialty
     * @see http://health-lifesci.schema.org
     */
    public function relevantSpecialty($relevantSpecialty)
    {
        return $this->setProperty('relevantSpecialty', $relevantSpecialty);
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's
     * identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or
     * official website.
     *
     * @param string|string[] $sameAs
     *
     * @return static
     *
     * @see https://schema.org/sameAs
     */
    public function sameAs($sameAs)
    {
        return $this->setProperty('sameAs', $sameAs);
    }

    /**
     * A possible serious complication and/or serious side effect of this
     * therapy. Serious adverse outcomes include those that are
     * life-threatening; result in death, disability, or permanent damage;
     * require hospitalization or prolong existing hospitalization; cause
     * congenital anomalies or birth defects; or jeopardize the patient and may
     * require medical or surgical intervention to prevent one of the outcomes
     * in this definition.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract|\ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract[] $seriousAdverseOutcome
     *
     * @return static
     *
     * @see https://schema.org/seriousAdverseOutcome
     * @see http://health-lifesci.schema.org
     */
    public function seriousAdverseOutcome($seriousAdverseOutcome)
    {
        return $this->setProperty('seriousAdverseOutcome', $seriousAdverseOutcome);
    }

    /**
     * The status of the study (enumerated).
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\EventStatusTypeContract|\ApolloCMS\SchemaOrg\Contracts\EventStatusTypeContract[]|\ApolloCMS\SchemaOrg\Contracts\MedicalStudyStatusContract|\ApolloCMS\SchemaOrg\Contracts\MedicalStudyStatusContract[]|string|string[] $status
     *
     * @return static
     *
     * @see https://schema.org/status
     * @see http://health-lifesci.schema.org
     */
    public function status($status)
    {
        return $this->setProperty('status', $status);
    }

    /**
     * A medical study or trial related to this entity.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalStudyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalStudyContract[] $study
     *
     * @return static
     *
     * @see https://schema.org/study
     * @see http://health-lifesci.schema.org
     */
    public function study($study)
    {
        return $this->setProperty('study', $study);
    }

    /**
     * A CreativeWork or Event about this Thing.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|\ApolloCMS\SchemaOrg\Contracts\EventContract|\ApolloCMS\SchemaOrg\Contracts\EventContract[] $subjectOf
     *
     * @return static
     *
     * @see https://schema.org/subjectOf
     * @link https://github.com/schemaorg/schemaorg/issues/1670
     */
    public function subjectOf($subjectOf)
    {
        return $this->setProperty('subjectOf', $subjectOf);
    }

    /**
     * URL of the item.
     *
     * @param string|string[] $url
     *
     * @return static
     *
     * @see https://schema.org/url
     */
    public function url($url)
    {
        return $this->setProperty('url', $url);
    }

}
