<?php

namespace ApolloCMS\SchemaOrg\Exceptions;

use InvalidArgumentException;

class TypeNotInMultiTypedEntity extends InvalidArgumentException
{
}
