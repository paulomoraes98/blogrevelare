<?php

namespace ApolloCMS\SchemaOrg\Exceptions;

use InvalidArgumentException;

class InvalidType extends InvalidArgumentException
{
}
