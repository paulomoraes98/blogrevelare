<?php

namespace ApolloCMS\SchemaOrg\Exceptions;

use InvalidArgumentException;

class TypeAlreadyInGraph extends InvalidArgumentException
{
}
