<?php

namespace ApolloCMS\SchemaOrg\Exceptions;

use InvalidArgumentException;

class TypeNotInGraph extends InvalidArgumentException
{
}
