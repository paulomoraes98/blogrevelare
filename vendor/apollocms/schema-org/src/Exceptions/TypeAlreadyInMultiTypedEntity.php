<?php

namespace ApolloCMS\SchemaOrg\Exceptions;

use InvalidArgumentException;

class TypeAlreadyInMultiTypedEntity extends InvalidArgumentException
{
}
