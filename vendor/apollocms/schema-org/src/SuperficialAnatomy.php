<?php

namespace ApolloCMS\SchemaOrg;

use \ApolloCMS\SchemaOrg\Contracts\SuperficialAnatomyContract;
use \ApolloCMS\SchemaOrg\Contracts\MedicalEntityContract;
use \ApolloCMS\SchemaOrg\Contracts\ThingContract;

/**
 * Anatomical features that can be observed by sight (without dissection),
 * including the form and proportions of the human body as well as surface
 * landmarks that correspond to deeper subcutaneous structures. Superficial
 * anatomy plays an important role in sports medicine, phlebotomy, and other
 * medical specialties as underlying anatomical structures can be identified
 * through surface palpation. For example, during back surgery, superficial
 * anatomy can be used to palpate and count vertebrae to find the site of
 * incision. Or in phlebotomy, superficial anatomy can be used to locate an
 * underlying vein; for example, the median cubital vein can be located by
 * palpating the borders of the cubital fossa (such as the epicondyles of the
 * humerus) and then looking for the superficial signs of the vein, such as
 * size, prominence, ability to refill after depression, and feel of surrounding
 * tissue support. As another example, in a subluxation (dislocation) of the
 * glenohumeral joint, the bony structure becomes pronounced with the deltoid
 * muscle failing to cover the glenohumeral joint allowing the edges of the
 * scapula to be superficially visible. Here, the superficial anatomy is the
 * visible edges of the scapula, implying the underlying dislocation of the
 * joint (the related anatomical structure).
 *
 * @see https://schema.org/SuperficialAnatomy
 * @see http://health-lifesci.schema.org
 *
 */
class SuperficialAnatomy extends BaseType implements SuperficialAnatomyContract, MedicalEntityContract, ThingContract
{
    /**
     * An additional type for the item, typically used for adding more specific
     * types from external vocabularies in microdata syntax. This is a
     * relationship between something and a class that the thing is in. In RDFa
     * syntax, it is better to use the native RDFa syntax - the 'typeof'
     * attribute - for multiple types. Schema.org tools may have only weaker
     * understanding of extra types, in particular those defined externally.
     *
     * @param string|string[] $additionalType
     *
     * @return static
     *
     * @see https://schema.org/additionalType
     */
    public function additionalType($additionalType)
    {
        return $this->setProperty('additionalType', $additionalType);
    }

    /**
     * An alias for the item.
     *
     * @param string|string[] $alternateName
     *
     * @return static
     *
     * @see https://schema.org/alternateName
     */
    public function alternateName($alternateName)
    {
        return $this->setProperty('alternateName', $alternateName);
    }

    /**
     * If applicable, a description of the pathophysiology associated with the
     * anatomical system, including potential abnormal changes in the
     * mechanical, physical, and biochemical functions of the system.
     *
     * @param string|string[] $associatedPathophysiology
     *
     * @return static
     *
     * @see https://schema.org/associatedPathophysiology
     * @see http://health-lifesci.schema.org
     */
    public function associatedPathophysiology($associatedPathophysiology)
    {
        return $this->setProperty('associatedPathophysiology', $associatedPathophysiology);
    }

    /**
     * A medical code for the entity, taken from a controlled vocabulary or
     * ontology such as ICD-9, DiseasesDB, MeSH, SNOMED-CT, RxNorm, etc.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalCodeContract|\ApolloCMS\SchemaOrg\Contracts\MedicalCodeContract[] $code
     *
     * @return static
     *
     * @see https://schema.org/code
     * @see http://health-lifesci.schema.org
     */
    public function code($code)
    {
        return $this->setProperty('code', $code);
    }

    /**
     * A description of the item.
     *
     * @param string|string[] $description
     *
     * @return static
     *
     * @see https://schema.org/description
     */
    public function description($description)
    {
        return $this->setProperty('description', $description);
    }

    /**
     * A sub property of description. A short description of the item used to
     * disambiguate from other, similar items. Information from other properties
     * (in particular, name) may be necessary for the description to be useful
     * for disambiguation.
     *
     * @param string|string[] $disambiguatingDescription
     *
     * @return static
     *
     * @see https://schema.org/disambiguatingDescription
     */
    public function disambiguatingDescription($disambiguatingDescription)
    {
        return $this->setProperty('disambiguatingDescription', $disambiguatingDescription);
    }

    /**
     * A medical guideline related to this entity.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalGuidelineContract|\ApolloCMS\SchemaOrg\Contracts\MedicalGuidelineContract[] $guideline
     *
     * @return static
     *
     * @see https://schema.org/guideline
     * @see http://health-lifesci.schema.org
     */
    public function guideline($guideline)
    {
        return $this->setProperty('guideline', $guideline);
    }

    /**
     * The identifier property represents any kind of identifier for any kind of
     * [[Thing]], such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides
     * dedicated properties for representing many of these, either as textual
     * strings or as URL (URI) links. See [background
     * notes](/docs/datamodel.html#identifierBg) for more details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\PropertyValueContract|\ApolloCMS\SchemaOrg\Contracts\PropertyValueContract[]|string|string[] $identifier
     *
     * @return static
     *
     * @see https://schema.org/identifier
     */
    public function identifier($identifier)
    {
        return $this->setProperty('identifier', $identifier);
    }

    /**
     * An image of the item. This can be a [[URL]] or a fully described
     * [[ImageObject]].
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ImageObjectContract|\ApolloCMS\SchemaOrg\Contracts\ImageObjectContract[]|string|string[] $image
     *
     * @return static
     *
     * @see https://schema.org/image
     */
    public function image($image)
    {
        return $this->setProperty('image', $image);
    }

    /**
     * The drug or supplement's legal status, including any controlled substance
     * schedules that apply.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\DrugLegalStatusContract|\ApolloCMS\SchemaOrg\Contracts\DrugLegalStatusContract[]|\ApolloCMS\SchemaOrg\Contracts\MedicalEnumerationContract|\ApolloCMS\SchemaOrg\Contracts\MedicalEnumerationContract[]|string|string[] $legalStatus
     *
     * @return static
     *
     * @see https://schema.org/legalStatus
     * @see http://health-lifesci.schema.org
     */
    public function legalStatus($legalStatus)
    {
        return $this->setProperty('legalStatus', $legalStatus);
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main
     * entity being described. See [background
     * notes](/docs/datamodel.html#mainEntityBackground) for details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|string|string[] $mainEntityOfPage
     *
     * @return static
     *
     * @see https://schema.org/mainEntityOfPage
     */
    public function mainEntityOfPage($mainEntityOfPage)
    {
        return $this->setProperty('mainEntityOfPage', $mainEntityOfPage);
    }

    /**
     * The system of medicine that includes this MedicalEntity, for example
     * 'evidence-based', 'homeopathic', 'chiropractic', etc.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicineSystemContract|\ApolloCMS\SchemaOrg\Contracts\MedicineSystemContract[] $medicineSystem
     *
     * @return static
     *
     * @see https://schema.org/medicineSystem
     * @see http://health-lifesci.schema.org
     */
    public function medicineSystem($medicineSystem)
    {
        return $this->setProperty('medicineSystem', $medicineSystem);
    }

    /**
     * The name of the item.
     *
     * @param string|string[] $name
     *
     * @return static
     *
     * @see https://schema.org/name
     */
    public function name($name)
    {
        return $this->setProperty('name', $name);
    }

    /**
     * Indicates a potential Action, which describes an idealized action in
     * which this thing would play an 'object' role.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ActionContract|\ApolloCMS\SchemaOrg\Contracts\ActionContract[] $potentialAction
     *
     * @return static
     *
     * @see https://schema.org/potentialAction
     */
    public function potentialAction($potentialAction)
    {
        return $this->setProperty('potentialAction', $potentialAction);
    }

    /**
     * If applicable, the organization that officially recognizes this entity as
     * part of its endorsed system of medicine.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\OrganizationContract|\ApolloCMS\SchemaOrg\Contracts\OrganizationContract[] $recognizingAuthority
     *
     * @return static
     *
     * @see https://schema.org/recognizingAuthority
     * @see http://health-lifesci.schema.org
     */
    public function recognizingAuthority($recognizingAuthority)
    {
        return $this->setProperty('recognizingAuthority', $recognizingAuthority);
    }

    /**
     * Anatomical systems or structures that relate to the superficial anatomy.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\AnatomicalStructureContract|\ApolloCMS\SchemaOrg\Contracts\AnatomicalStructureContract[]|\ApolloCMS\SchemaOrg\Contracts\AnatomicalSystemContract|\ApolloCMS\SchemaOrg\Contracts\AnatomicalSystemContract[] $relatedAnatomy
     *
     * @return static
     *
     * @see https://schema.org/relatedAnatomy
     * @see http://health-lifesci.schema.org
     */
    public function relatedAnatomy($relatedAnatomy)
    {
        return $this->setProperty('relatedAnatomy', $relatedAnatomy);
    }

    /**
     * A medical condition associated with this anatomy.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalConditionContract|\ApolloCMS\SchemaOrg\Contracts\MedicalConditionContract[] $relatedCondition
     *
     * @return static
     *
     * @see https://schema.org/relatedCondition
     * @see http://health-lifesci.schema.org
     */
    public function relatedCondition($relatedCondition)
    {
        return $this->setProperty('relatedCondition', $relatedCondition);
    }

    /**
     * A medical therapy related to this anatomy.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalTherapyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalTherapyContract[] $relatedTherapy
     *
     * @return static
     *
     * @see https://schema.org/relatedTherapy
     * @see http://health-lifesci.schema.org
     */
    public function relatedTherapy($relatedTherapy)
    {
        return $this->setProperty('relatedTherapy', $relatedTherapy);
    }

    /**
     * If applicable, a medical specialty in which this entity is relevant.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalSpecialtyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalSpecialtyContract[] $relevantSpecialty
     *
     * @return static
     *
     * @see https://schema.org/relevantSpecialty
     * @see http://health-lifesci.schema.org
     */
    public function relevantSpecialty($relevantSpecialty)
    {
        return $this->setProperty('relevantSpecialty', $relevantSpecialty);
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's
     * identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or
     * official website.
     *
     * @param string|string[] $sameAs
     *
     * @return static
     *
     * @see https://schema.org/sameAs
     */
    public function sameAs($sameAs)
    {
        return $this->setProperty('sameAs', $sameAs);
    }

    /**
     * The significance associated with the superficial anatomy; as an example,
     * how characteristics of the superficial anatomy can suggest underlying
     * medical conditions or courses of treatment.
     *
     * @param string|string[] $significance
     *
     * @return static
     *
     * @see https://schema.org/significance
     * @see http://health-lifesci.schema.org
     */
    public function significance($significance)
    {
        return $this->setProperty('significance', $significance);
    }

    /**
     * A medical study or trial related to this entity.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\MedicalStudyContract|\ApolloCMS\SchemaOrg\Contracts\MedicalStudyContract[] $study
     *
     * @return static
     *
     * @see https://schema.org/study
     * @see http://health-lifesci.schema.org
     */
    public function study($study)
    {
        return $this->setProperty('study', $study);
    }

    /**
     * A CreativeWork or Event about this Thing.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|\ApolloCMS\SchemaOrg\Contracts\EventContract|\ApolloCMS\SchemaOrg\Contracts\EventContract[] $subjectOf
     *
     * @return static
     *
     * @see https://schema.org/subjectOf
     * @link https://github.com/schemaorg/schemaorg/issues/1670
     */
    public function subjectOf($subjectOf)
    {
        return $this->setProperty('subjectOf', $subjectOf);
    }

    /**
     * URL of the item.
     *
     * @param string|string[] $url
     *
     * @return static
     *
     * @see https://schema.org/url
     */
    public function url($url)
    {
        return $this->setProperty('url', $url);
    }

}
