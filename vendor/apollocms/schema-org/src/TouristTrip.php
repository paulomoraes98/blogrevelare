<?php

namespace ApolloCMS\SchemaOrg;

use \ApolloCMS\SchemaOrg\Contracts\TouristTripContract;
use \ApolloCMS\SchemaOrg\Contracts\IntangibleContract;
use \ApolloCMS\SchemaOrg\Contracts\ThingContract;
use \ApolloCMS\SchemaOrg\Contracts\TripContract;

/**
 * A tourist trip. A created itinerary of visits to one or more places of
 * interest ([[TouristAttraction]]/[[TouristDestination]]) often linked by a
 * similar theme, geographic area, or interest to a particular [[touristType]].
 * The [UNWTO](http://www2.unwto.org/) defines tourism trip as the Trip taken by
 * visitors.
 *   (See examples below).
 *
 * @see https://schema.org/TouristTrip
 * @see http://pending.schema.org
 *
 */
class TouristTrip extends BaseType implements TouristTripContract, IntangibleContract, ThingContract, TripContract
{
    /**
     * An additional type for the item, typically used for adding more specific
     * types from external vocabularies in microdata syntax. This is a
     * relationship between something and a class that the thing is in. In RDFa
     * syntax, it is better to use the native RDFa syntax - the 'typeof'
     * attribute - for multiple types. Schema.org tools may have only weaker
     * understanding of extra types, in particular those defined externally.
     *
     * @param string|string[] $additionalType
     *
     * @return static
     *
     * @see https://schema.org/additionalType
     */
    public function additionalType($additionalType)
    {
        return $this->setProperty('additionalType', $additionalType);
    }

    /**
     * An alias for the item.
     *
     * @param string|string[] $alternateName
     *
     * @return static
     *
     * @see https://schema.org/alternateName
     */
    public function alternateName($alternateName)
    {
        return $this->setProperty('alternateName', $alternateName);
    }

    /**
     * The expected arrival time.
     *
     * @param \DateTimeInterface|\DateTimeInterface[] $arrivalTime
     *
     * @return static
     *
     * @see https://schema.org/arrivalTime
     */
    public function arrivalTime($arrivalTime)
    {
        return $this->setProperty('arrivalTime', $arrivalTime);
    }

    /**
     * The expected departure time.
     *
     * @param \DateTimeInterface|\DateTimeInterface[] $departureTime
     *
     * @return static
     *
     * @see https://schema.org/departureTime
     */
    public function departureTime($departureTime)
    {
        return $this->setProperty('departureTime', $departureTime);
    }

    /**
     * A description of the item.
     *
     * @param string|string[] $description
     *
     * @return static
     *
     * @see https://schema.org/description
     */
    public function description($description)
    {
        return $this->setProperty('description', $description);
    }

    /**
     * A sub property of description. A short description of the item used to
     * disambiguate from other, similar items. Information from other properties
     * (in particular, name) may be necessary for the description to be useful
     * for disambiguation.
     *
     * @param string|string[] $disambiguatingDescription
     *
     * @return static
     *
     * @see https://schema.org/disambiguatingDescription
     */
    public function disambiguatingDescription($disambiguatingDescription)
    {
        return $this->setProperty('disambiguatingDescription', $disambiguatingDescription);
    }

    /**
     * The identifier property represents any kind of identifier for any kind of
     * [[Thing]], such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides
     * dedicated properties for representing many of these, either as textual
     * strings or as URL (URI) links. See [background
     * notes](/docs/datamodel.html#identifierBg) for more details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\PropertyValueContract|\ApolloCMS\SchemaOrg\Contracts\PropertyValueContract[]|string|string[] $identifier
     *
     * @return static
     *
     * @see https://schema.org/identifier
     */
    public function identifier($identifier)
    {
        return $this->setProperty('identifier', $identifier);
    }

    /**
     * An image of the item. This can be a [[URL]] or a fully described
     * [[ImageObject]].
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ImageObjectContract|\ApolloCMS\SchemaOrg\Contracts\ImageObjectContract[]|string|string[] $image
     *
     * @return static
     *
     * @see https://schema.org/image
     */
    public function image($image)
    {
        return $this->setProperty('image', $image);
    }

    /**
     * Destination(s) ( [[Place]] ) that make up a trip. For a trip where
     * destination order is important use [[ItemList]] to specify that order
     * (see examples).
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ItemListContract|\ApolloCMS\SchemaOrg\Contracts\ItemListContract[]|\ApolloCMS\SchemaOrg\Contracts\PlaceContract|\ApolloCMS\SchemaOrg\Contracts\PlaceContract[] $itinerary
     *
     * @return static
     *
     * @see https://schema.org/itinerary
     * @see http://pending.schema.org
     * @link http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#Tourism
     */
    public function itinerary($itinerary)
    {
        return $this->setProperty('itinerary', $itinerary);
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main
     * entity being described. See [background
     * notes](/docs/datamodel.html#mainEntityBackground) for details.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|string|string[] $mainEntityOfPage
     *
     * @return static
     *
     * @see https://schema.org/mainEntityOfPage
     */
    public function mainEntityOfPage($mainEntityOfPage)
    {
        return $this->setProperty('mainEntityOfPage', $mainEntityOfPage);
    }

    /**
     * The name of the item.
     *
     * @param string|string[] $name
     *
     * @return static
     *
     * @see https://schema.org/name
     */
    public function name($name)
    {
        return $this->setProperty('name', $name);
    }

    /**
     * An offer to provide this item&#x2014;for example, an offer to sell a
     * product, rent the DVD of a movie, perform a service, or give away tickets
     * to an event. Use [[businessFunction]] to indicate the kind of transaction
     * offered, i.e. sell, lease, etc. This property can also be used to
     * describe a [[Demand]]. While this property is listed as expected on a
     * number of common types, it can be used in others. In that case, using a
     * second type, such as Product or a subtype of Product, can clarify the
     * nature of the offer.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\DemandContract|\ApolloCMS\SchemaOrg\Contracts\DemandContract[]|\ApolloCMS\SchemaOrg\Contracts\OfferContract|\ApolloCMS\SchemaOrg\Contracts\OfferContract[] $offers
     *
     * @return static
     *
     * @see https://schema.org/offers
     * @link https://github.com/schemaorg/schemaorg/issues/2289
     */
    public function offers($offers)
    {
        return $this->setProperty('offers', $offers);
    }

    /**
     * Identifies that this [[Trip]] is a subTrip of another Trip.  For example
     * Day 1, Day 2, etc. of a multi-day trip.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\TripContract|\ApolloCMS\SchemaOrg\Contracts\TripContract[] $partOfTrip
     *
     * @return static
     *
     * @see https://schema.org/partOfTrip
     * @see http://pending.schema.org
     * @link http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#Tourism
     */
    public function partOfTrip($partOfTrip)
    {
        return $this->setProperty('partOfTrip', $partOfTrip);
    }

    /**
     * Indicates a potential Action, which describes an idealized action in
     * which this thing would play an 'object' role.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\ActionContract|\ApolloCMS\SchemaOrg\Contracts\ActionContract[] $potentialAction
     *
     * @return static
     *
     * @see https://schema.org/potentialAction
     */
    public function potentialAction($potentialAction)
    {
        return $this->setProperty('potentialAction', $potentialAction);
    }

    /**
     * The service provider, service operator, or service performer; the goods
     * producer. Another party (a seller) may offer those services or goods on
     * behalf of the provider. A provider may also serve as the seller.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\OrganizationContract|\ApolloCMS\SchemaOrg\Contracts\OrganizationContract[]|\ApolloCMS\SchemaOrg\Contracts\PersonContract|\ApolloCMS\SchemaOrg\Contracts\PersonContract[] $provider
     *
     * @return static
     *
     * @see https://schema.org/provider
     * @link https://github.com/schemaorg/schemaorg/issues/2289
     */
    public function provider($provider)
    {
        return $this->setProperty('provider', $provider);
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's
     * identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or
     * official website.
     *
     * @param string|string[] $sameAs
     *
     * @return static
     *
     * @see https://schema.org/sameAs
     */
    public function sameAs($sameAs)
    {
        return $this->setProperty('sameAs', $sameAs);
    }

    /**
     * Identifies a [[Trip]] that is a subTrip of this Trip.  For example Day 1,
     * Day 2, etc. of a multi-day trip.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\TripContract|\ApolloCMS\SchemaOrg\Contracts\TripContract[] $subTrip
     *
     * @return static
     *
     * @see https://schema.org/subTrip
     * @see http://pending.schema.org
     * @link http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#Tourism
     */
    public function subTrip($subTrip)
    {
        return $this->setProperty('subTrip', $subTrip);
    }

    /**
     * A CreativeWork or Event about this Thing.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract|\ApolloCMS\SchemaOrg\Contracts\CreativeWorkContract[]|\ApolloCMS\SchemaOrg\Contracts\EventContract|\ApolloCMS\SchemaOrg\Contracts\EventContract[] $subjectOf
     *
     * @return static
     *
     * @see https://schema.org/subjectOf
     * @link https://github.com/schemaorg/schemaorg/issues/1670
     */
    public function subjectOf($subjectOf)
    {
        return $this->setProperty('subjectOf', $subjectOf);
    }

    /**
     * Attraction suitable for type(s) of tourist. eg. Children, visitors from a
     * particular country, etc.
     *
     * @param \ApolloCMS\SchemaOrg\Contracts\AudienceContract|\ApolloCMS\SchemaOrg\Contracts\AudienceContract[]|string|string[] $touristType
     *
     * @return static
     *
     * @see https://schema.org/touristType
     */
    public function touristType($touristType)
    {
        return $this->setProperty('touristType', $touristType);
    }

    /**
     * URL of the item.
     *
     * @param string|string[] $url
     *
     * @return static
     *
     * @see https://schema.org/url
     */
    public function url($url)
    {
        return $this->setProperty('url', $url);
    }

}
