<?php

namespace ApolloCMS\ResponsiveImages\Listeners;

use ApolloCMS\ResponsiveImages\Breakpoint;
use ApolloCMS\ResponsiveImages\Responsive;
use ApolloCMS\Events\AssetUploaded;
use ApolloCMS\Tags\Parameters;

class GenerateResponsiveVersions
{
    public function handle(AssetUploaded $event): void
    {
        if (! $event->asset->isImage()) {
            return;
        }

        if ($event->asset->extension() === 'svg') {
            return;
        }

        if (! config('apollocms.assets.image_manipulation.cache')) {
            return;
        }

        if (! config('apollocms.responsive-images.generate_on_upload', true)) {
            return;
        }

        $responsive = new Responsive($event->asset, new Parameters());
        $responsive->breakPoints()->each(function (Breakpoint $breakpoint) {
            $breakpoint->dispatchImageJobs();
        });
    }
}
