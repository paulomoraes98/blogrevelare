<?php

namespace ApolloCMS\ResponsiveImages\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use ApolloCMS\Contracts\Assets\Asset;

abstract class GenerateImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \ApolloCMS\Contracts\Assets\Asset */
    protected $asset;

    /** @var array */
    protected $params;

    public function __construct(Asset $asset, array $params = [])
    {
        $this->asset = $asset;
        $this->params = $params;

        $this->queue = config('apollocms.responsive-images.queue', 'default');
    }

    public function handle(): string
    {
        return $this->imageUrl();
    }

    abstract protected function imageUrl(): string;
}
