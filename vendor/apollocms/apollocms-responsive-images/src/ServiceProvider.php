<?php

namespace ApolloCMS\ResponsiveImages;

use Illuminate\Support\Facades\Blade;
use ApolloCMS\ResponsiveImages\Commands\GenerateResponsiveVersionsCommand;
use ApolloCMS\ResponsiveImages\Commands\RegenerateResponsiveVersionsCommand;
use ApolloCMS\ResponsiveImages\Fieldtypes\ResponsiveFieldtype;
use ApolloCMS\ResponsiveImages\Jobs\GenerateImageJob;
use ApolloCMS\ResponsiveImages\Listeners\GenerateResponsiveVersions;
use ApolloCMS\ResponsiveImages\Tags\ResponsiveTag;
use ApolloCMS\Events\AssetUploaded;
use ApolloCMS\Providers\AddonServiceProvider;

class ServiceProvider extends AddonServiceProvider
{
    protected $tags = [
        ResponsiveTag::class,
    ];

    protected $fieldtypes = [
        ResponsiveFieldtype::class,
    ];

    protected $stylesheets = [
        __DIR__.'/../dist/css/responsive.css',
    ];

    protected $scripts = [
        __DIR__.'/../dist/js/responsive.js',
    ];

    protected $listen = [
        AssetUploaded::class => [
            GenerateResponsiveVersions::class,
        ],
    ];

    protected $commands = [
        GenerateResponsiveVersionsCommand::class,
        RegenerateResponsiveVersionsCommand::class,
    ];

    public function boot()
    {
        parent::boot();

        $this
            ->bootAddonViews()
            ->bootAddonConfig()
            ->bootDirectives()
            ->bindImageJob();
    }

    protected function bootAddonViews(): self
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'responsive-images');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/responsive-images'),
        ], 'responsive-images-views');

        return $this;
    }

    protected function bootAddonConfig(): self
    {
        $this->mergeConfigFrom(__DIR__.'/../config/responsive-images.php', 'apollocms.responsive-images');

        $this->publishes([
            __DIR__.'/../config/responsive-images.php' => config_path('apollocms/responsive-images.php'),
        ], 'responsive-images-config');

        return $this;
    }

    protected function bootDirectives(): self
    {
        Blade::directive('responsive', function ($arguments) {
            return "<?php echo \ApolloCMS\ResponsiveImages\Tags\ResponsiveTag::render({$arguments}) ?>";
        });

        return $this;
    }

    private function bindImageJob(): self
    {
        $this->app->bind(GenerateImageJob::class, config('apollocms.responsive-images.image_job'));

        return $this;
    }
}
