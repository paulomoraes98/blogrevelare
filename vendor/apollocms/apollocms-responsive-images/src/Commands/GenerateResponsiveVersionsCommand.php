<?php

namespace ApolloCMS\ResponsiveImages\Commands;

use Illuminate\Console\Command;
use ApolloCMS\ResponsiveImages\Breakpoint;
use ApolloCMS\ResponsiveImages\Responsive;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Contracts\Assets\Asset;
use ApolloCMS\Contracts\Assets\AssetRepository;
use ApolloCMS\Tags\Parameters;

class GenerateResponsiveVersionsCommand extends Command
{
    use RunsInPlease;

    protected $signature = 'apollocms:responsive:generate';

    protected $description = 'Generate responsive images';

    public function handle(AssetRepository $assets)
    {
        if (! config('apollocms.assets.image_manipulation.cache')) {
            $this->error('Caching is not enabled for image manipulations, generating them will have no benefit.');

            return;
        }

        $assets = $assets->all()->filter(function (Asset $asset) {
            return $asset->isImage() && $asset->extension() !== 'svg';
        });

        $this->info("Generating responsive image versions for {$assets->count()} assets.");

        $this->getOutput()->progressStart($assets->count());

        /** @var \ApolloCMS\Assets\AssetCollection $assets */
        $assets->each(function (Asset $asset) {
            $responsive = new Responsive($asset, new Parameters());
            $responsive->breakPoints()->each(function (Breakpoint $breakpoint) {
                $breakpoint->dispatchImageJobs();
            });

            $this->getOutput()->progressAdvance();
        });

        $this->getOutput()->progressFinish();
        $this->info("All jobs dispatched.");
    }
}
