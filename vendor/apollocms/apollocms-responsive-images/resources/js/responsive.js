import ResponsiveFieldtype from "./ResponsiveFieldtype";
import ResponsiveFieldtypeIndex from "./ResponsiveFieldtypeIndex";

ApolloCMS.booting(() => {
    ApolloCMS.$components.register('responsive-fieldtype', ResponsiveFieldtype);
    ApolloCMS.$components.register('responsive-fieldtype-index', ResponsiveFieldtypeIndex);
})
