<?php

namespace ApolloCMS\ResponsiveImages\Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use ApolloCMS\ResponsiveImages\Commands\GenerateResponsiveVersionsCommand;
use ApolloCMS\ResponsiveImages\Jobs\GenerateGlideImageJob;
use ApolloCMS\ResponsiveImages\Tests\TestCase;
use ApolloCMS\Facades\Asset;
use ApolloCMS\Facades\Apoche;

class GenerateResponsiveVersionsCommandTest extends TestCase
{
    /** @var \ApolloCMS\Assets\Asset */
    private $asset;

    protected function setUp(): void
    {
        parent::setUp();

        $file = new UploadedFile($this->getTestJpg(), 'test.jpg');
        $path = ltrim('/'.$file->getClientOriginalName(), '/');
        $this->asset = $this->assetContainer->makeAsset($path)->upload($file);

        Apoche::clear();
    }

    /** @test * */
    public function it_requires_caching_to_be_set()
    {
        config()->set('apollocms.assets.image_manipulation.cache', false);

        $this->artisan(GenerateResponsiveVersionsCommand::class)
            ->expectsOutput('Caching is not enabled for image manipulations, generating them will have no benefit.')
            ->assertExitCode(0);
    }

    /** @test * */
    public function it_dispatches_jobs_for_all_assets_that_are_images()
    {
        Queue::fake();
        config()->set('apollocms.assets.image_manipulation.cache', true);

        $this->artisan(GenerateResponsiveVersionsCommand::class)
            ->expectsOutput("Generating responsive image versions for 1 assets.")
            ->assertExitCode(0);

        Queue::assertPushed(GenerateGlideImageJob::class, 6);
    }

    /** @test * */
    public function it_dispatches_less_jobs_when_webp_is_disabled()
    {
        Queue::fake();
        config()->set('apollocms.assets.image_manipulation.cache', true);
        config()->set('apollocms.responsive-images.webp', false);

        $this->artisan(GenerateResponsiveVersionsCommand::class)
            ->expectsOutput("Generating responsive image versions for 1 assets.")
            ->assertExitCode(0);

        Queue::assertPushed(GenerateGlideImageJob::class, 3);
    }
}
