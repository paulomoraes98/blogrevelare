<?php

namespace ApolloCMS\ResponsiveImages\Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Orchestra\Testbench\TestCase as OrchestraTestCase;
use ApolloCMS\Assets\AssetContainer;
use ApolloCMS\Console\Commands\GlideClear;
use ApolloCMS\Extend\Manifest;
use ApolloCMS\Facades\Asset;
use ApolloCMS\Facades\Apoche;
use ApolloCMS\ApolloCMS;

class TestCase extends OrchestraTestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /** @var \ApolloCMS\Assets\AssetContainer */
    protected $assetContainer;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->faker = $this->makeFaker();

        $this->setUpTempTestFiles();

        $this->artisan(GlideClear::class);

        config(['filesystems.disks.test' => [
            'driver' => 'local',
            'root' => __DIR__.'/tmp',
            'url' => '/test',
        ]]);

        /** @var \ApolloCMS\Assets\AssetContainer $assetContainer */
        $this->assetContainer = (new AssetContainer)
            ->handle('test_container')
            ->disk('test')
            ->save();
    }

    protected function tearDown(): void
    {
        $this->assetContainer->delete();
        File::deleteDirectory(__DIR__ . '/tmp');
        Storage::disk('test')->delete('*');
        Asset::all()->each->delete();
        Apoche::clear();

        parent::tearDown();
    }

    protected function getPackageProviders($app)
    {
        return [
            \ApolloCMS\Providers\ApolloCMSServiceProvider::class,
            \Wilderborn\Partyline\ServiceProvider::class,
            \ApolloCMS\ResponsiveImages\ServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'ApolloCMS' => ApolloCMS::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $app->make(Manifest::class)->manifest = [
            'apollocms/apollocms-responsive-images' => [
                'id' => 'apollocms/apollocms-responsive-images',
                'namespace' => 'ApolloCMS\\ResponsiveImages\\',
            ],
        ];
    }

    protected function resolveApplicationConfiguration($app)
    {
        parent::resolveApplicationConfiguration($app);

        $configs = [
            'assets', 'cp', 'forms', 'routes', 'static_caching',
            'sites', 'apoche', 'system', 'users',
        ];

        foreach ($configs as $config) {
            $app['config']->set("apollocms.$config", require(__DIR__ . "/../vendor/apollocms/cms/config/{$config}.php"));
        }

        // Setting the user repository to the default flat file system
        $app['config']->set('apollocms.users.repository', 'file');

        // Assume the pro edition within tests
        $app['config']->set('apollocms.editions.pro', true);

        // Define config settings for all of our tests
        $app['config']->set("apollocms.responsive-images", require(__DIR__ . "/../config/responsive-images.php"));

        $app['config']->set('apollocms.assets.image_manipulation.driver', 'imagick');
    }

    protected function setUpTempTestFiles()
    {
        $this->initializeDirectory($this->getTestFilesDirectory());
        File::copyDirectory(__DIR__.'/TestSupport/testfiles', $this->getTestFilesDirectory());
    }

    protected function initializeDirectory($directory)
    {
        if (File::isDirectory($directory)) {
            File::deleteDirectory($directory);
        }

        File::makeDirectory($directory, 0755, true);
    }

    public function getTempDirectory($suffix = ''): string
    {
        return __DIR__.'/TestSupport/temp'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getTestFilesDirectory($suffix = ''): string
    {
        return $this->getTempDirectory().'/testfiles'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getTestJpg(): string
    {
        return $this->getTestFilesDirectory('test.jpg');
    }

    public function getSmallTestJpg(): string
    {
        return $this->getTestFilesDirectory('smallTest.jpg');
    }

    public function getTestSvg(): string
    {
        return $this->getTestFilesDirectory('test.svg');
    }

    public function getTestGif(): string
    {
        return $this->getTestFilesDirectory('hackerman.gif');
    }
}
