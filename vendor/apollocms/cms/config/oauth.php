<?php

return [

    'enabled' => env('APOLLOCMS_OAUTH_ENABLED', false),

    'providers' => [
        // 'github',
    ],

    'routes' => [
        'login' => 'oauth/{provider}',
        'callback' => 'oauth/{provider}/callback',
    ],

];
