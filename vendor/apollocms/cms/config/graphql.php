<?php

return [

    /*
    |--------------------------------------------------------------------------
    | GraphQL
    |--------------------------------------------------------------------------
    |
    | Here you may enable the GraphQL API, and select which resources
    | are available to be queried, depending on your site's needs.
    |
    | https://apollocms.siteturbo.com.br/graphql
    |
    */

    'enabled' => env('APOLLOCMS_GRAPHQL_ENABLED', false),

    'resources' => [
        'collections' => false,
        'navs' => false,
        'taxonomies' => false,
        'assets' => false,
        'globals' => false,
        'sites' => false,
        'users' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Queries
    |--------------------------------------------------------------------------
    |
    | Here you may list queries to be added to the ApolloCMS schema.
    |
    | https://apollocms.siteturbo.com.br/graphql#custom-queries
    |
    */

    'queries' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Here you may list middleware to be added to the ApolloCMS schema.
    |
    | https://apollocms.siteturbo.com.br/graphql#custom-middleware
    |
    */

    'middleware' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    |
    | By default, ApolloCMS will cache each request until the specified
    | expiry, or until content is changed. See the documentation for
    | more details on how to customize your cache implementation.
    |
    | https://apollocms.siteturbo.com.br/graphql#caching
    |
    */

    'cache' => [
        'expiry' => 60,
    ],

];
