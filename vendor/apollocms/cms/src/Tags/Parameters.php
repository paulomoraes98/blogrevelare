<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Facades\Lunar;
use ApolloCMS\Fields\Value;
use ApolloCMS\Support\Str;

class Parameters extends ArrayAccessor
{
    public static function make($items = [], $context = null)
    {
        if (! $context) {
            throw new \InvalidArgumentException('A Context object is expected.');
        }

        $items = collect($items)->mapWithKeys(function ($value, $key) use ($context) {
            // Values in parameters prefixed with a colon should be treated as the corresponding field's value in the context.
            if (Str::startsWith($key, ':')) {
                $key = substr($key, 1);
                $value = Lunar::parser()->getVariable($value, $context->all());
            }

            if ($value instanceof Value) {
                $value = $value->value();
            }

            if ($value === 'true') {
                $value = true;
            }

            if ($value === 'false') {
                $value = false;
            }

            return [$key => $value];
        })->all();

        return parent::make($items);
    }
}
