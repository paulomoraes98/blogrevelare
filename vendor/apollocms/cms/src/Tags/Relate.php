<?php

namespace ApolloCMS\Tags;

class Relate extends Tags
{
    public function wildcard($tag)
    {
        return $this->context->value($tag);
    }
}
