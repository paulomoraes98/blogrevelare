<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Exceptions\NotFoundHttpException;

class NotFound extends Tags
{
    protected static $aliases = ['404'];

    public function index()
    {
        throw new NotFoundHttpException();
    }
}
