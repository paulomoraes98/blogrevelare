<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Support\Html;

class Obfuscate extends Tags
{
    public function index()
    {
        return Html::obfuscate($this->content);
    }
}
