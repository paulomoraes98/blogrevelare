<?php

namespace ApolloCMS\Tags\Concerns;

use ApolloCMS\Support\Arr;

trait QueriesScopes
{
    public function queryScopes($query)
    {
        $this->parseQueryScopes()
            ->map(function ($handle) {
                return app('apollocms.scopes')->get($handle);
            })
            ->filter()
            ->each(function ($class) use ($query) {
                $scope = app($class);
                $scope->apply($query, $this->params);
            });
    }

    protected function parseQueryScopes()
    {
        $scopes = Arr::getFirst($this->params, ['query_scope', 'filter']);

        return collect(explode('|', $scopes));
    }
}
