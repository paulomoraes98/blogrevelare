<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Support\Str;

class Widont extends Tags
{
    public function index()
    {
        return Str::widont($this->content);
    }
}
