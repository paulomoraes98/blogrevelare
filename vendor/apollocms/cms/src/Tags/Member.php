<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Auth\UserTags;

class Member extends UserTags
{
    protected static $handle = 'member';
}
