<?php

namespace ApolloCMS\Tags;

use ApolloCMS\Fields\Value;

class Context extends ArrayAccessor
{
    public function raw($key, $default = null)
    {
        $value = parent::get($key, $default);

        return $value instanceof Value ? $value->raw() : $value;
    }

    public function value($key, $default = null)
    {
        $value = parent::get($key, $default);

        return $value instanceof Value ? $value->value() : $value;
    }
}
