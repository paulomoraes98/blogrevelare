<?php

namespace ApolloCMS\Extensions;

use Illuminate\Cache\FileStore as LaravelFileStore;
use Illuminate\Contracts\Cache\Store;
use ApolloCMS\Support\Str;

class FileStore extends LaravelFileStore implements Store
{
    protected function path($key)
    {
        if (! Str::startsWith($key, 'apoche::')) {
            return parent::path($key);
        }

        $key = Str::after($key, 'apoche::');

        return $this->directory.'/apoche/'.str_replace('::', '/', $key);
    }
}
