<?php

namespace ApolloCMS\Query\Scopes;

use ApolloCMS\Extend\HasHandle;
use ApolloCMS\Extend\RegistersItself;

abstract class Scope
{
    use RegistersItself, HasHandle;

    protected static $binding = 'scopes';

    /**
     * Apply the scope to a given query builder.
     *
     * @param \ApolloCMS\Query\Builder $builder
     * @param array $values
     * @return void
     */
    abstract public function apply($query, $values);
}
