<?php

namespace ApolloCMS\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;
use ApolloCMS\ApolloCMS;

class GlobalSetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource
            ->toAugmentedCollection()
            ->merge([
                'handle' => $this->resource->handle(),
                'api_url' => ApolloCMS::apiRoute('globals.show', [$this->resource->handle()]),
            ])
            ->withShallowNesting()
            ->toArray();
    }
}
