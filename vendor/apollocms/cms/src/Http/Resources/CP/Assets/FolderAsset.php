<?php

namespace ApolloCMS\Http\Resources\CP\Assets;

use Illuminate\Http\Resources\Json\JsonResource;
use ApolloCMS\Facades\Action;
use ApolloCMS\Support\Str;

class FolderAsset extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id(),
            'basename' => $this->basename(),
            'extension' => $this->extension(),
            'url' => $this->absoluteUrl(),

            'size_formatted' => Str::fileSizeForHumans($this->size(), 0),
            'last_modified_relative' => $this->lastModified()->diffForHumans(),

            $this->mergeWhen($this->isImage(), function () {
                return [
                    'is_image' => true,
                    'thumbnail' => $this->thumbnailUrl('small'),
                ];
            }),

            'actions' => Action::for($this->resource, [
                'container' => $this->container()->handle(),
            ]),
        ];
    }
}
