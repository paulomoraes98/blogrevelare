<?php

namespace ApolloCMS\Http\Middleware;

use Closure;
use ApolloCMS\Facades\Apoche;

class ApocheLock
{
    public function handle($request, Closure $next)
    {
        if (! config('apollocms.apoche.lock.enabled', true)) {
            return $next($request);
        }

        $start = time();
        $lock = Apoche::lock('apoche-warming');

        while (! $lock->acquire()) {
            if (time() - $start >= config('apollocms.apoche.lock.timeout', 30)) {
                return $this->outputRefreshResponse($request);
            }

            sleep(1);
        }

        $lock->release();

        return $next($request);
    }

    private function outputRefreshResponse($request)
    {
        $html = $request->ajax() || $request->wantsJson()
            ? __('Service Unavailable')
            : sprintf('<meta http-equiv="refresh" content="1; URL=\'%s\'" />', $request->getUri());

        return response($html, 503, ['Retry-After' => 1]);
    }
}
