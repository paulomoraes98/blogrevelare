<?php

namespace ApolloCMS\Http\Middleware;

use Closure;
use ApolloCMS\Exceptions\ApolloCMSProRequiredException;
use ApolloCMS\ApolloCMS;

class CheckMultisite
{
    public function handle($request, Closure $next)
    {
        if (ApolloCMS::pro() || $request->is('_ignition*')) {
            return $next($request);
        }

        $sites = config('apollocms.sites.sites');

        throw_if(count($sites) > 1, new ApolloCMSProRequiredException('ApolloCMS Pro is required to use multiple sites.'));

        return $next($request);
    }
}
