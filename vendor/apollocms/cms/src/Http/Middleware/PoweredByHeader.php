<?php

namespace ApolloCMS\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class PoweredByHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (config('apollocms.system.send_powered_by_header') && $response instanceof Response) {
            $response->header('X-Powered-By', 'ApolloCMS');
        }

        return $response;
    }
}
