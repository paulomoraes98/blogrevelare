<?php

namespace ApolloCMS\Http\Middleware\API;

use ApolloCMS\Exceptions\ApiExceptionHandler;
use ApolloCMS\Exceptions\ApiExceptionHandlerForLaravelSix;
use ApolloCMS\Http\Middleware\SwapExceptionHandler as Middleware;

class SwapExceptionHandler extends Middleware
{
    public function handler()
    {
        return version_compare(app()->version(), 7, '>=')
            ? ApiExceptionHandler::class
            : ApiExceptionHandlerForLaravelSix::class;
    }
}
