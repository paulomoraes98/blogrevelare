<?php

namespace ApolloCMS\Http\Middleware;

use Closure;
use ApolloCMS\Exceptions\ApolloCMSProAuthorizationException;
use ApolloCMS\ApolloCMS;

class RequireApolloCMSPro
{
    public function handle($request, Closure $next)
    {
        if (! ApolloCMS::pro()) {
            throw new ApolloCMSProAuthorizationException(__('ApolloCMS Pro is required.'));
        }

        return $next($request);
    }
}
