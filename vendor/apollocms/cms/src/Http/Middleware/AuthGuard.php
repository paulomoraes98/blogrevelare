<?php

namespace ApolloCMS\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthGuard
{
    public function handle($request, Closure $next)
    {
        Auth::shouldUse(config('apollocms.users.guards.web', 'web'));

        return $next($request);
    }
}
