<?php

namespace ApolloCMS\Http\Middleware\CP;

use Closure;
use ApolloCMS\Exceptions\ApolloCMSProRequiredException;
use ApolloCMS\Facades\User;
use ApolloCMS\ApolloCMS;

class CountUsers
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (! ApolloCMS::pro() && User::count() > 1) {
            throw new ApolloCMSProRequiredException('ApolloCMS Pro is required for multiple users.');
        }

        return $next($request);
    }
}
