<?php

namespace ApolloCMS\Http\Middleware\CP;

use ApolloCMS\Exceptions\ControlPanelExceptionHandler;
use ApolloCMS\Exceptions\ControlPanelExceptionHandlerForLaravelSix;
use ApolloCMS\Http\Middleware\SwapExceptionHandler as Middleware;

class SwapExceptionHandler extends Middleware
{
    public function handler()
    {
        return version_compare(app()->version(), 7, '>=')
            ? ControlPanelExceptionHandler::class
            : ControlPanelExceptionHandlerForLaravelSix::class;
    }
}
