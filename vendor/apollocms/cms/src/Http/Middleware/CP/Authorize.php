<?php

namespace ApolloCMS\Http\Middleware\CP;

use Closure;
use ApolloCMS\Exceptions\AuthenticationException;
use ApolloCMS\Exceptions\AuthorizationException;
use ApolloCMS\Facades\User;

class Authorize
{
    public function handle($request, Closure $next)
    {
        $user = User::current();

        if (! $user) {
            throw new AuthenticationException('Unauthenticated.');
        }

        if ($user->cant('access cp')) {
            // dd('theres a user but they are unauthorized', $user);
            throw new AuthorizationException('Unauthorized.');
        }

        return $next($request);
    }
}
