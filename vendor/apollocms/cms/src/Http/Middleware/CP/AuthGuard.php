<?php

namespace ApolloCMS\Http\Middleware\CP;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthGuard
{
    public function handle($request, Closure $next)
    {
        Auth::shouldUse(config('apollocms.users.guards.cp', 'web'));

        return $next($request);
    }
}
