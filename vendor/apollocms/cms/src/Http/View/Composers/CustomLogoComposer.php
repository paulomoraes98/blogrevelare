<?php

namespace ApolloCMS\Http\View\Composers;

use Illuminate\View\View;
use ApolloCMS\ApolloCMS;
use ApolloCMS\Support\Arr;

class CustomLogoComposer
{
    const VIEWS = [
        'apollocms::partials.global-header',
        'apollocms::partials.outside-logo',
    ];

    public function compose(View $view)
    {
        $view->with('customLogo', $this->customLogo($view));
    }

    protected function customLogo($view)
    {
        if (! ApolloCMS::pro()) {
            return false;
        }

        $config = config('apollocms.cp.custom_logo_url');

        switch ($view->name()) {
            case 'apollocms::partials.outside-logo':
                $type = 'outside';
                break;
            case 'apollocms::partials.global-header':
                $type = 'nav';
                break;
            default:
                $type = 'other';
        }

        if ($logo = Arr::get($config, $type)) {
            return $logo;
        }

        if (! is_array($config)) {
            return $config;
        }

        return false;
    }
}
