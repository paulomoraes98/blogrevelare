<?php

namespace ApolloCMS\Http\View\Composers;

use Facades\ApolloCMS\Fields\FieldtypeRepository;
use Illuminate\View\View;
use ApolloCMS\Facades\Preference;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\User;
use ApolloCMS\ApolloCMS;
use ApolloCMS\Support\Str;

class JavascriptComposer
{
    const VIEWS = ['apollocms::partials.scripts'];

    public function compose(View $view)
    {
        $user = User::current();

        ApolloCMS::provideToScript([
            'version' => ApolloCMS::version(),
            'laravelVersion' => app()->version(),
            'csrfToken' => csrf_token(),
            'cpUrl' => cp_route('index'),
            'cpRoot' => str_start(config('apollocms.cp.route'), '/'),
            'urlPath' => Str::after(request()->getRequestUri(), config('apollocms.cp.route').'/'),
            'resourceUrl' => ApolloCMS::cpAssetUrl(),
            'locales' => config('apollocms.system.locales'),
            'flash' => ApolloCMS::flash(),
            'ajaxTimeout' => config('apollocms.system.ajax_timeout'),
            'googleDocsViewer' => config('apollocms.assets.google_docs_viewer'),
            'focalPointEditorEnabled' => config('apollocms.assets.focal_point_editor'),
            'user' => $this->user($user),
            'paginationSize' => config('apollocms.cp.pagination_size'),
            'translationLocale' => app('translator')->locale(),
            'translations' => app('translator')->toJson(),
            'sites' => $this->sites(),
            'selectedSite' => Site::selected()->handle(),
            'ampEnabled' => config('apollocms.amp.enabled'),
            'preloadableFieldtypes' => FieldtypeRepository::preloadable()->keys(),
            'livePreview' => config('apollocms.live_preview'),
            'locale' => config('app.locale'),
            'permissions' => $this->permissions($user),
        ]);
    }

    protected function sites()
    {
        return Site::all()->map(function ($site) {
            return [
                'name' => $site->name(),
                'handle' => $site->handle(),
            ];
        })->values();
    }

    protected function permissions($user)
    {
        $permissions = $user ? $user->permissions() : [];

        return base64_encode(json_encode($permissions));
    }

    protected function user($user)
    {
        if (! $user) {
            return [];
        }

        return array_merge($user->toAugmentedArray(), [
            'preferences' => Preference::all(),
            'permissions' => $user->permissions()->all(),
        ]);
    }
}
