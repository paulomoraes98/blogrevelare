<?php

namespace ApolloCMS\Http\View\Composers;

use Illuminate\View\View;
use ApolloCMS\Facades\CP\Nav;

class NavComposer
{
    const VIEWS = [
        'apollocms::partials.nav-main',
        'apollocms::partials.nav-mobile',
    ];

    protected static $nav;

    public function compose(View $view)
    {
        $view->with('nav', self::$nav = self::$nav ?? Nav::build());
    }
}
