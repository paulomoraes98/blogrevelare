<?php

namespace ApolloCMS\Http\View\Composers;

use Illuminate\View\View;
use ApolloCMS\Facades\Fieldset;
use ApolloCMS\Fields\FieldTransformer;
use ApolloCMS\ApolloCMS;

class FieldComposer
{
    const VIEWS = [
        'apollocms::*.blueprints.edit',
        'apollocms::fieldsets.edit',
    ];

    protected $fieldsetFields;

    public function compose(View $view)
    {
        ApolloCMS::provideToScript([
            'fieldsets' => $this->fieldsets(),
            'fieldsetFields' => FieldTransformer::fieldsetFields(),
        ]);
    }

    private function fieldsets()
    {
        return Fieldset::all()->mapWithKeys(function ($fieldset) {
            return [$fieldset->handle() => [
                'handle' => $fieldset->handle(),
                'title' => $fieldset->title(),
            ]];
        });
    }
}
