<?php

namespace ApolloCMS\Http\Controllers\CP\Updater;

use Facades\ApolloCMS\Marketplace\Marketplace;
use Facades\ApolloCMS\Updater\UpdatesOverview;
use Illuminate\Http\Request;
use ApolloCMS\Facades\Addon;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\ApolloCMS;

class UpdaterController extends CpController
{
    /**
     * Updates overview.
     */
    public function index()
    {
        $this->authorize('view updates');

        $addons = $this->getUpdatableAddons();

        if ($addons->isEmpty()) {
            return redirect()->route('apollocms.cp.updater.product', ApolloCMS::CORE_SLUG);
        }

        return view('apollocms::updater.index', [
            'apollocms' => Marketplace::apollocms()->changelog(),
            'addons' => Addon::all()->filter->existsOnMarketplace(),
            'unlistedAddons' => Addon::all()->reject->existsOnMarketplace(),
        ]);
    }

    /**
     * Updates count.
     *
     * @param Request $request
     */
    public function count(Request $request)
    {
        $this->authorize('view updates');

        return UpdatesOverview::count($request->input('clearCache', false));
    }

    /**
     * Get updatable addons.
     *
     * @return \Illuminate\Support\Collection
     */
    private function getUpdatableAddons()
    {
        return Addon::all()->filter->marketplaceSlug();
    }
}
