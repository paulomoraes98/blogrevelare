<?php

namespace ApolloCMS\Http\Controllers\CP\Updater;

use Facades\ApolloCMS\Console\Processes\Composer;
use Facades\ApolloCMS\Marketplace\Marketplace;
use Illuminate\Http\Request;
use ApolloCMS\Facades\Addon;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\ApolloCMS;
use ApolloCMS\Updater\Updater;

class UpdateProductController extends CpController
{
    /**
     * Show product updates overview.
     *
     * @param string $slug
     */
    public function show($slug)
    {
        $this->authorize('view updates');

        if (! $product = Marketplace::product($slug)) {
            return $this->pageNotFound();
        }

        return view('apollocms::updater.show', [
            'slug' => $slug,
            'package' => $product->package(),
            'name' => $product->name(),
        ]);
    }

    /**
     * Product changelog.
     *
     * @param string $slug
     */
    public function changelog($slug)
    {
        $this->authorize('view updates');

        if (! $product = Marketplace::product($slug)) {
            return $this->pageNotFound();
        }

        $changelog = $product->changelog();

        return [
            'changelog' => $changelog->get(),
            'currentVersion' => $changelog->currentVersion(),
            'lastInstallLog' => Composer::lastCompletedCachedOutput($product->package())['output'],
        ];
    }

    /**
     * Install explicit version.
     *
     * @param string $product
     * @param Request $request
     */
    public function install($product, Request $request)
    {
        $this->authorize('perform updates');

        $package = $product === ApolloCMS::CORE_SLUG ? ApolloCMS::PACKAGE : $this->getAddon($product)->package();

        return Updater::package($package)->install($request->version);
    }

    /**
     * Get updatable addon from product slug.
     *
     * @param string $product
     * @return \Illuminate\Support\Collection
     */
    private function getAddon($product)
    {
        return Addon::all()->first(function ($addon) use ($product) {
            return $addon->slug() === $product;
        });
    }
}
