<?php

namespace ApolloCMS\Http\Controllers\CP;

use ApolloCMS\Http\Middleware\RequireApolloCMSPro;

class GraphQLController extends CpController
{
    public function __construct()
    {
        $this->middleware(RequireApolloCMSPro::class);
    }

    public function index()
    {
        return redirect()->action([self::class, 'graphiql']);
    }

    public function graphiql()
    {
        return view('apollocms::graphql.graphiql', [
            'url' => '/'.config('graphql.prefix'),
        ]);
    }
}
