<?php

namespace ApolloCMS\Http\Controllers\CP\Fields;

use Illuminate\Http\Request;
use ApolloCMS\Facades;
use ApolloCMS\Fields\Fieldset;
use ApolloCMS\Fields\FieldTransformer;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\Support\Arr;

class FieldsetController extends CpController
{
    public function __construct()
    {
        $this->middleware(\Illuminate\Auth\Middleware\Authorize::class.':configure fields');
    }

    public function index(Request $request)
    {
        $fieldsets = Facades\Fieldset::all()->map(function ($fieldset) {
            return [
                'id' => $fieldset->handle(),
                'handle' => $fieldset->handle(),
                'title' => $fieldset->title(),
                'fields' => $fieldset->fields()->all()->count(),
                'edit_url' => $fieldset->editUrl(),
                'delete_url' => $fieldset->deleteUrl(),
            ];
        })->values();

        if ($request->wantsJson()) {
            return $fieldsets;
        }

        return view('apollocms::fieldsets.index', compact('fieldsets'));
    }

    public function edit($fieldset)
    {
        $fieldset = Facades\Fieldset::find($fieldset);

        $vue = [
            'title' => $fieldset->title(),
            'handle' => $fieldset->handle(),
            'fields' => collect(Arr::get($fieldset->contents(), 'fields'))->map(function ($field, $i) {
                return array_merge(FieldTransformer::toVue($field), ['_id' => $i]);
            })->all(),
        ];

        return view('apollocms::fieldsets.edit', [
            'fieldset' => $fieldset,
            'fieldsetVueObject' => $vue,
        ]);
    }

    public function update(Request $request, $fieldset)
    {
        $fieldset = Facades\Fieldset::find($fieldset);

        $request->validate([
            'title' => 'required',
            'fields' => 'array',
        ]);

        $fieldset->setContents([
            'title' => $request->title,
            'fields' => collect($request->fields)->map(function ($field) {
                return FieldTransformer::fromVue($field);
            })->all(),
        ])->save();

        return response('', 204);
    }

    public function create()
    {
        return view('apollocms::fieldsets.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'handle' => 'required|alpha_dash',
        ]);

        if (Facades\Fieldset::find($request->handle)) {
            return back()->withInput()->with('error', __('A fieldset with that name already exists.'));
        }

        $fieldset = (new Fieldset)
            ->setHandle($request->handle)
            ->setContents([
                'title' => $request->title,
                'fields' => [],
            ])->save();

        session()->flash('success', __('Fieldset created'));

        return ['redirect' => $fieldset->editUrl()];
    }

    public function destroy($fieldset)
    {
        $fieldset = Facades\Fieldset::find($fieldset);

        $this->authorize('delete', $fieldset);

        $fieldset->delete();

        return response('');
    }

    /**
     * Quickly create a new barebones fieldset from within the fieldtype.
     *
     * @return array
     */
    public function quickStore(Request $request)
    {
        $title = $request->title;

        if (Facades\Fieldset::exists($handle = snake_case($title))) {
            return ['success' => true];
        }

        $fieldset = (new Fieldset)->setHandle($handle)->setContents([
            'title' => $request->title,
            'fields' => [],
        ])->save();

        return ['success' => true];
    }
}
