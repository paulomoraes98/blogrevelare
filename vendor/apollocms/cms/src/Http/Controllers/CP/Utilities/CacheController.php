<?php

namespace ApolloCMS\Http\Controllers\CP\Utilities;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use League\Glide\Server;
use ApolloCMS\Facades\Apoche;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\StaticCaching\Cacher as StaticCacher;
use ApolloCMS\Support\Str;

class CacheController extends CpController
{
    public function index()
    {
        return view('apollocms::utilities.cache', [
            'apoche' => $this->getApocheStats(),
            'cache' => $this->getApplicationCacheStats(),
            'static' => $this->getStaticCacheStats(),
            'images' => $this->getImageCacheStats(),
        ]);
    }

    protected function getApocheStats()
    {
        $time = Apoche::buildTime();
        $built = Apoche::buildDate();

        return [
            'records' => Apoche::fileCount(),
            'size' => Str::fileSizeForHumans(Apoche::fileSize()),
            'time' => $time ? Str::timeForHumans($time) : __('Refresh'),
            'rebuilt' => $built ? $built->diffForHumans() : __('Refresh'),
        ];
    }

    protected function getApplicationCacheStats()
    {
        $driver = config('cache.default');
        $driver = ($driver === 'apollocms') ? 'file (apollocms)' : $driver;

        return compact('driver');
    }

    protected function getImageCacheStats()
    {
        $files = collect(app(Server::class)->getCache()->listContents('', true))
            ->filter(function ($file) {
                return $file['type'] === 'file';
            });

        return [
            'count' => $files->count(),
            'size' => Str::fileSizeForHumans($files->reduce(function ($size, $file) {
                return $size + $file['size'];
            }, 0)),
        ];
    }

    protected function getStaticCacheStats()
    {
        $strategy = config('apollocms.static_caching.strategy');

        return [
            'enabled' => (bool) $strategy,
            'strategy' => $strategy ?? __('Disabled'),
            'count' => app(StaticCacher::class)->getUrls()->count(),
        ];
    }

    public function clear(Request $request, $cache)
    {
        $method = 'clear'.ucfirst($cache).'Cache';

        return $this->$method();
    }

    protected function clearAllCache()
    {
        $this->clearApocheCache();
        $this->clearStaticCache();
        $this->clearApplicationCache();
        $this->clearImageCache();

        return back()->withSuccess(__('All caches cleared.'));
    }

    protected function clearApocheCache()
    {
        Apoche::refresh();

        return back()->withSuccess(__('Apoche cleared.'));
    }

    protected function clearStaticCache()
    {
        app(StaticCacher::class)->flush();

        return back()->withSuccess(__('Static page cache cleared.'));
    }

    protected function clearApplicationCache()
    {
        Artisan::call('cache:clear');

        // TODO: Apoche doesn't appear to be clearing?
        // Maybe related to https://github.com/apollocms/three-cms/issues/149

        return back()->withSuccess(__('Application cache cleared.'));
    }

    protected function clearImageCache()
    {
        Artisan::call('apollocms:glide:clear');

        return back()->withSuccess(__('Image cache cleared.'));
    }
}
