<?php

namespace ApolloCMS\Http\Controllers\CP\Utilities;

use Illuminate\Http\Request;
use ApolloCMS\Facades\Utility;
use ApolloCMS\Http\Controllers\CP\CpController;

class UtilitiesController extends CpController
{
    public function index()
    {
        return view('apollocms::utilities.index', [
            'utilities' => Utility::authorized()->sortBy->title(),
        ]);
    }

    public function show(Request $request)
    {
        $utility = Utility::find($this->getUtilityHandle($request));

        if ($view = $utility->view()) {
            return view($view, $utility->viewData($request));
        }

        throw new \Exception("Utility [{$utility->handle()}] has not been provided with an action or view.");
    }

    private function getUtilityHandle($request)
    {
        preg_match('/\/utilities\/([^\/]+)/', $request->url(), $matches);

        return $matches[1];
    }
}
