<?php

namespace ApolloCMS\Http\Controllers\CP\Utilities;

use ApolloCMS\Facades\Git;
use ApolloCMS\Http\Controllers\CP\CpController;

class GitController extends CpController
{
    public function index()
    {
        return view('apollocms::utilities.git', [
            'statuses' => Git::statuses(),
        ]);
    }

    public function commit()
    {
        Git::commit();

        return back()->withSuccess(__('Content committed'));
    }
}
