<?php

namespace ApolloCMS\Http\Controllers\CP\Structures;

use Illuminate\Http\Request;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\Structure;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\Structures\TreeBuilder;
use ApolloCMS\Support\Arr;

class StructurePagesController extends CpController
{
    public function index(Request $request, $handle)
    {
        $structure = Structure::find($handle);
        $site = $request->site ?? Site::selected()->handle();

        $pages = (new TreeBuilder)->buildForController([
            'structure' => $handle,
            'include_home' => true,
            'site' => $site,
        ]);

        return ['pages' => $pages];
    }

    public function store(Request $request, $structure)
    {
        $tree = $this->toTree($request->pages);

        Structure::find($structure)
            ->in($request->site)
            ->tree($tree)
            ->save();
    }

    protected function toTree($items)
    {
        return collect($items)->map(function ($item) {
            return Arr::removeNullValues([
                'entry' => $ref = $item['id'] ?? null,
                'title' => $ref ? null : ($item['title'] ?? null),
                'url' => $ref ? null : ($item['url'] ?? null),
                'children' => $this->toTree($item['children']),
            ]);
        })->all();
    }
}
