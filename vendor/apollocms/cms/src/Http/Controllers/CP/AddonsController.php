<?php

namespace ApolloCMS\Http\Controllers\CP;

use Facades\ApolloCMS\Extend\AddonInstaller;
use Illuminate\Http\Request;
use ApolloCMS\Facades\Addon;

class AddonsController extends CpController
{
    public function __construct()
    {
        $this->middleware(\Illuminate\Auth\Middleware\Authorize::class.':configure addons');
    }

    public function index()
    {
        return view('apollocms::addons.index', [
            'title' => __('Addons'),
            'addonCount' => Addon::all()->count(),
        ]);
    }

    public function install(Request $request)
    {
        return AddonInstaller::install($request->addon);
    }

    public function uninstall(Request $request)
    {
        return AddonInstaller::uninstall($request->addon);
    }
}
