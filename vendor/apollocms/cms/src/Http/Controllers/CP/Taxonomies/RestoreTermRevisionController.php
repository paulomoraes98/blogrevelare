<?php

namespace ApolloCMS\Http\Controllers\CP\Taxonomies;

use Illuminate\Http\Request;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\Revisions\WorkingCopy;

class RestoreTermRevisionController extends CpController
{
    public function __invoke(Request $request, $collection, $entry)
    {
        if (! $target = $entry->revision($request->revision)) {
            dd('no such revision', $request->revision);
            // todo: handle invalid revision reference
        }

        if ($entry->published()) {
            WorkingCopy::fromRevision($target)->date(now())->save();
        } else {
            $entry->makeFromRevision($target)->published(false)->save();
        }

        session()->flash('success', __('Revision restored'));
    }
}
