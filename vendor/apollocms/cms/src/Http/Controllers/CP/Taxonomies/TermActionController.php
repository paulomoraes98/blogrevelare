<?php

namespace ApolloCMS\Http\Controllers\CP\Taxonomies;

use ApolloCMS\Facades\Term;
use ApolloCMS\Http\Controllers\CP\ActionController;

class TermActionController extends ActionController
{
    protected function getSelectedItems($items, $context)
    {
        return $items->map(function ($item) {
            return Term::find($item);
        })->filter();
    }
}
