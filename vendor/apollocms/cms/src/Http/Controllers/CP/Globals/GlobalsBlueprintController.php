<?php

namespace ApolloCMS\Http\Controllers\CP\Globals;

use Illuminate\Http\Request;
use ApolloCMS\Facades\GlobalSet;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\Http\Controllers\CP\Fields\ManagesBlueprints;

class GlobalsBlueprintController extends CpController
{
    use ManagesBlueprints;

    public function __construct()
    {
        $this->middleware(\Illuminate\Auth\Middleware\Authorize::class.':configure fields');
    }

    public function edit($set)
    {
        if (! $set = GlobalSet::find($set)) {
            return $this->pageNotFound();
        }

        $blueprint = $this->blueprint($set);

        return view('apollocms::globals.blueprints.edit', [
            'set' => $set,
            'blueprint' => $blueprint,
            'blueprintVueObject' => $this->toVueObject($blueprint),
        ]);
    }

    public function update(Request $request, $set)
    {
        if (! $set = GlobalSet::find($set)) {
            return $this->pageNotFound();
        }

        $request->validate(['sections' => 'array']);

        $this->updateBlueprint($request, $this->blueprint($set));
    }

    private function blueprint($set)
    {
        return tap($set->blueprint() ?? $set->inDefaultSite()->blueprint())
            ->setHandle($set->handle())
            ->setNamespace('globals');
    }
}
