<?php

namespace ApolloCMS\Http\Controllers\CP\Auth;

use Illuminate\Support\Facades\Password;
use ApolloCMS\Auth\Passwords\PasswordReset;
use ApolloCMS\Http\Controllers\ResetPasswordController as Controller;
use ApolloCMS\Http\Middleware\CP\RedirectIfAuthorized;

class ResetPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware(RedirectIfAuthorized::class);
    }

    public function broker()
    {
        $broker = config('apollocms.users.passwords.'.PasswordReset::BROKER_RESETS);

        if (is_array($broker)) {
            $broker = $broker['cp'];
        }

        return Password::broker($broker);
    }

    protected function resetFormAction()
    {
        return route('apollocms.cp.password.reset.action');
    }
}
