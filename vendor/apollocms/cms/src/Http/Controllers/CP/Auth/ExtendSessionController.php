<?php

namespace ApolloCMS\Http\Controllers\CP\Auth;

use ApolloCMS\Http\Controllers\CP\CpController;

class ExtendSessionController extends CpController
{
    public function __invoke()
    {
        return 'OK';
    }
}
