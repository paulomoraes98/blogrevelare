<?php

namespace ApolloCMS\Http\Controllers\CP\Auth;

use ApolloCMS\Http\Controllers\CP\CpController;

class CsrfTokenController extends CpController
{
    public function __invoke()
    {
        return csrf_token();
    }
}
