<?php

namespace ApolloCMS\Http\Controllers\CP\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use ApolloCMS\Auth\Passwords\PasswordReset;
use ApolloCMS\Http\Controllers\ForgotPasswordController as Controller;
use ApolloCMS\Http\Middleware\CP\RedirectIfAuthorized;

class ForgotPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware(RedirectIfAuthorized::class);
    }

    public function sendResetLinkEmail(Request $request)
    {
        PasswordReset::resetFormRoute('apollocms.cp.password.reset');
        PasswordReset::redirectAfterReset(route('apollocms.cp.index'));

        return parent::sendResetLinkEmail($request);
    }

    public function broker()
    {
        $broker = config('apollocms.users.passwords.'.PasswordReset::BROKER_RESETS);

        if (is_array($broker)) {
            $broker = $broker['cp'];
        }

        return Password::broker($broker);
    }
}
