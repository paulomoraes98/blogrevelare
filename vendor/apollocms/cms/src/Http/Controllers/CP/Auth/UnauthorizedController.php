<?php

namespace ApolloCMS\Http\Controllers\CP\Auth;

class UnauthorizedController
{
    public function __invoke()
    {
        return view('apollocms::auth.unauthorized');
    }
}
