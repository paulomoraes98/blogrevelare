<?php

namespace ApolloCMS\Http\Controllers\CP;

use Illuminate\Auth\Access\AuthorizationException as LaravelAuthException;
use Illuminate\Http\Request;
use ApolloCMS\Exceptions\AuthorizationException;
use ApolloCMS\Facades\File;
use ApolloCMS\Facades\Folder;
use ApolloCMS\Facades\YAML;
use ApolloCMS\Http\Controllers\Controller;
use ApolloCMS\ApolloCMS;
use ApolloCMS\Support\Str;

/**
 * The base control panel controller.
 */
class CpController extends Controller
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new CpController.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get all the template names from the current theme.
     *
     * @return array
     */
    public function templates()
    {
        $templates = [];

        foreach (Folder::disk('resources')->getFilesByTypeRecursively('templates', 'html') as $path) {
            $parts = explode('/', $path);
            array_shift($parts);
            $templates[] = Str::removeRight(implode('/', $parts), '.html');
        }

        return $templates;
    }

    public function themes()
    {
        $themes = [];

        foreach (Folder::disk('themes')->getFolders('/') as $folder) {
            $name = $folder;

            // Get the name if one exists in a meta file
            if (File::disk('themes')->exists($folder.'/meta.yaml')) {
                $meta = YAML::parse(File::disk('themes')->get($folder.'/meta.yaml'));
                $name = array_get($meta, 'name', $folder);
            }

            $themes[] = compact('folder', 'name');
        }

        return $themes;
    }

    /**
     * 404.
     */
    public function pageNotFound()
    {
        return response()->view('apollocms::errors.404', [], 404);
    }

    public function authorize($ability, $args = [], $message = null)
    {
        $message = $message ?? __('This action is unauthorized.');

        try {
            return parent::authorize($ability, $args);
        } catch (LaravelAuthException $e) {
            throw new AuthorizationException($message);
        }
    }

    public function authorizeIf($condition, $ability, $args = [], $message = null)
    {
        if ($condition) {
            return $this->authorize($ability, $args, $message);
        }
    }

    public function authorizePro()
    {
        if (! ApolloCMS::pro()) {
            throw new AuthorizationException(__('ApolloCMS Pro is required.'));
        }
    }

    public function authorizeProIf($condition)
    {
        if ($condition) {
            return $this->authorizePro();
        }
    }
}
