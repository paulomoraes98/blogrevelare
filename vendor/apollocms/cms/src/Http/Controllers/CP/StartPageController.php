<?php

namespace ApolloCMS\Http\Controllers\CP;

use ApolloCMS\Facades\Preference;

class StartPageController extends CpController
{
    public function __invoke()
    {
        session()->reflash();

        $url = config('apollocms.cp.route').'/'.Preference::get('start_page', config('apollocms.cp.start_page'));

        return redirect($url);
    }
}
