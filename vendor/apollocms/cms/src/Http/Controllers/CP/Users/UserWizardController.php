<?php

namespace ApolloCMS\Http\Controllers\CP\Users;

use Illuminate\Http\Request;
use ApolloCMS\Facades\User;
use ApolloCMS\Http\Controllers\CP\CpController;

class UserWizardController extends CpController
{
    public function __invoke(Request $request)
    {
        $user = User::findByEmail($request->email);

        return ['exists' => (bool) $user];
    }
}
