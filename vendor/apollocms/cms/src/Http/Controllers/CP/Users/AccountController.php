<?php

namespace ApolloCMS\Http\Controllers\CP\Users;

use Illuminate\Http\Request;
use ApolloCMS\Auth\User;
use ApolloCMS\Http\Controllers\CP\CpController;

class AccountController extends CpController
{
    public function __invoke(Request $request)
    {
        return redirect(User::fromUser($request->user())->editUrl());
    }
}
