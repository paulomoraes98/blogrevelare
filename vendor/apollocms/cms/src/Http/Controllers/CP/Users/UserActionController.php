<?php

namespace ApolloCMS\Http\Controllers\CP\Users;

use ApolloCMS\Facades\User;
use ApolloCMS\Http\Controllers\CP\ActionController;

class UserActionController extends ActionController
{
    protected function getSelectedItems($items, $context)
    {
        return $items->map(function ($item) {
            return User::find($item);
        });
    }
}
