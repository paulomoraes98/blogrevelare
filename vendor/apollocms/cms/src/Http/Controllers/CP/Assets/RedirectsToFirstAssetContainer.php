<?php

namespace ApolloCMS\Http\Controllers\CP\Assets;

use ApolloCMS\Facades\AssetContainer;
use ApolloCMS\Facades\User;

trait RedirectsToFirstAssetContainer
{
    public function redirectToFirstContainer()
    {
        $containers = AssetContainer::all()->filter(function ($container) {
            return User::current()->can('view', $container);
        });

        if ($containers->isEmpty()) {
            return;
        }

        abort(redirect($containers->first()->showUrl()));
    }
}
