<?php

namespace ApolloCMS\Http\Controllers\CP\Assets;

use ApolloCMS\Assets\AssetFolder;
use ApolloCMS\Http\Controllers\CP\ActionController as Controller;

class FolderActionController extends Controller
{
    protected static $key = 'asset-folders';

    protected function getSelectedItems($items, $context)
    {
        return $items->map(function ($path) use ($context) {
            return AssetFolder::find("{$context['container']}::{$path}");
        });
    }
}
