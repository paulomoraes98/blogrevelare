<?php

namespace ApolloCMS\Http\Controllers\CP\Assets;

use Illuminate\Http\Request;
use ApolloCMS\Fieldtypes\Assets\Assets as AssetsFieldtype;
use ApolloCMS\Http\Controllers\CP\CpController;

class FieldtypeController extends CpController
{
    public function index(Request $request)
    {
        return (new AssetsFieldtype)
            ->getItemData($request->input('assets', []));
    }
}
