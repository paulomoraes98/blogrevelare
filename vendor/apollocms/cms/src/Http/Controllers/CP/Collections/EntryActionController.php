<?php

namespace ApolloCMS\Http\Controllers\CP\Collections;

use ApolloCMS\Facades\Entry;
use ApolloCMS\Http\Controllers\CP\ActionController;

class EntryActionController extends ActionController
{
    protected function getSelectedItems($items, $context)
    {
        return $items->map(function ($item) {
            return Entry::find($item);
        });
    }
}
