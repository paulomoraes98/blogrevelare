<?php

namespace ApolloCMS\Http\Controllers\CP;

use Illuminate\Http\Request;
use ApolloCMS\Facades\File;
use Symfony\Component\VarExporter\VarExporter;

class AddonEditionsController extends CpController
{
    public function __construct()
    {
        $this->middleware(\Illuminate\Auth\Middleware\Authorize::class.':configure addons');
    }

    public function __invoke(Request $request)
    {
        $request->validate([
            'addon' => 'required',
            'edition' => 'required',
        ]);

        $config = config('apollocms.editions');
        $config['addons'][$request->addon] = $request->edition;

        $str = '<?php return '.VarExporter::export($config).';';

        File::put(config_path('apollocms/editions.php'), $str);
    }
}
