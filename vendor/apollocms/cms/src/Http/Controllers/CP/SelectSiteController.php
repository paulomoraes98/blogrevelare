<?php

namespace ApolloCMS\Http\Controllers\CP;

class SelectSiteController extends CpController
{
    public function select($handle)
    {
        session()->put('apollocms.cp.selected-site', $handle);

        return back()->with('success', 'Site selected.');
    }
}
