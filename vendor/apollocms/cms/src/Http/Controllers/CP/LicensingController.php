<?php

namespace ApolloCMS\Http\Controllers\CP;

use ApolloCMS\Licensing\LicenseManager as Licenses;

class LicensingController extends CpController
{
    public function show(Licenses $licenses)
    {
        return view('apollocms::licensing', [
            'requestError' => $licenses->requestFailed(),
            'site' => $licenses->site(),
            'apollocms' => $licenses->apollocms(),
            'addons' => $licenses->addons()->filter->existsOnMarketplace(),
            'unlistedAddons' => $licenses->addons()->reject->existsOnMarketplace(),
        ]);
    }

    public function refresh(Licenses $licenses)
    {
        $licenses->refresh();

        return redirect()
            ->cpRoute('utilities.licensing')
            ->with('success', __('Data updated'));
    }
}
