<?php

namespace ApolloCMS\Http\Controllers\CP\Fieldtypes;

use Facades\ApolloCMS\Fields\FieldtypeRepository as Fieldtype;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use ApolloCMS\Fields\Field;
use ApolloCMS\Http\Controllers\CP\CpController;
use ApolloCMS\Http\Requests\FilteredRequest;

class RelationshipFieldtypeController extends CpController
{
    public function index(FilteredRequest $request)
    {
        $fieldtype = $this->fieldtype($request);

        $items = $fieldtype->getIndexItems($request);

        if ($items instanceof Collection) {
            $items = $fieldtype->filterExcludedItems($items, $request->exclusions ?? []);
        }

        return $fieldtype->getResourceCollection($request, $items);
    }

    public function data(Request $request)
    {
        $fieldtype = $this->fieldtype($request);

        $items = $fieldtype
            ->getItemData($request->selections)
            ->values();

        return ['data' => $items];
    }

    public function filters(Request $request)
    {
        return $this->fieldtype($request)->getSelectionFilters();
    }

    protected function fieldtype($request)
    {
        $config = json_decode(utf8_encode(base64_decode($request->config)), true);

        return Fieldtype::find($config['type'])->setField(
            new Field('relationship', $config)
        );
    }
}
