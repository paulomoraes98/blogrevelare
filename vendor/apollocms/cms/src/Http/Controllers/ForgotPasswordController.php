<?php

namespace ApolloCMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use ApolloCMS\Auth\Passwords\PasswordReset;
use ApolloCMS\Auth\SendsPasswordResetEmails;
use ApolloCMS\Facades\URL;
use ApolloCMS\Http\Middleware\RedirectIfAuthenticated;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails {
        sendResetLinkEmail as public traitSendResetLinkEmail;
    }

    public function __construct()
    {
        $this->middleware(RedirectIfAuthenticated::class);
    }

    public function showLinkRequestForm()
    {
        return view('apollocms::auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        if ($url = $request->_reset_url) {
            PasswordReset::resetFormUrl(URL::makeAbsolute($url));
        }

        return $this->traitSendResetLinkEmail($request);
    }

    public function broker()
    {
        $broker = config('apollocms.users.passwords.'.PasswordReset::BROKER_RESETS);

        if (is_array($broker)) {
            $broker = $broker['web'];
        }

        return Password::broker($broker);
    }
}
