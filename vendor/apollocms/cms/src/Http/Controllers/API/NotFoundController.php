<?php

namespace ApolloCMS\Http\Controllers\API;

use ApolloCMS\Exceptions\NotFoundHttpException;

class NotFoundController
{
    public function __invoke()
    {
        throw new NotFoundHttpException;
    }
}
