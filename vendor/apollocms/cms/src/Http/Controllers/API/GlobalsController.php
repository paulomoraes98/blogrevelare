<?php

namespace ApolloCMS\Http\Controllers\API;

use ApolloCMS\Facades\GlobalSet;
use ApolloCMS\Http\Resources\API\GlobalSetResource;

class GlobalsController extends ApiController
{
    protected $resourceConfigKey = 'globals';
    protected $routeResourceKey = 'global';

    public function index()
    {
        $this->abortIfDisabled();

        return app(GlobalSetResource::class)::collection(
            $this->filterAllowedResources(GlobalSet::all()->map->in($this->queryParam('site')))
        );
    }

    public function show($globalSet)
    {
        $this->abortIfDisabled();

        return app(GlobalSetResource::class)::make($globalSet);
    }
}
