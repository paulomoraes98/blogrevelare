<?php

namespace ApolloCMS\Http\Controllers\API;

use ApolloCMS\Facades\Form;
use ApolloCMS\Http\Resources\API\FormResource;

class FormsController extends ApiController
{
    protected $resourceConfigKey = 'forms';
    protected $routeResourceKey = 'form';

    public function index()
    {
        $this->abortIfDisabled();

        return app(FormResource::class)::collection(
            $this->filterAllowedResources(Form::all())
        );
    }

    public function show($form)
    {
        $this->abortIfDisabled();

        return app(FormResource::class)::make($form);
    }
}
