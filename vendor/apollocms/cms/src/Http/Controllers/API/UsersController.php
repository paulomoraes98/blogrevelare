<?php

namespace ApolloCMS\Http\Controllers\API;

use ApolloCMS\Exceptions\NotFoundHttpException;
use ApolloCMS\Facades\User;
use ApolloCMS\Http\Resources\API\UserResource;

class UsersController extends ApiController
{
    protected $resourceConfigKey = 'users';

    public function index()
    {
        $this->abortIfDisabled();

        return app(UserResource::class)::collection(
            $this->filterSortAndPaginate(User::query())
        );
    }

    public function show($id)
    {
        $this->abortIfDisabled();

        return app(UserResource::class)::make($this->getUser($id));
    }

    private function getUser($id)
    {
        $user = User::find($id);

        throw_unless($user, new NotFoundHttpException("User [$id] not found."));

        return $user;
    }
}
