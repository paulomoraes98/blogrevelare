<?php

namespace ApolloCMS\Http\Controllers;

use Illuminate\Support\Facades\Password;
use ApolloCMS\Auth\Passwords\PasswordReset;

class ActivateAccountController extends ResetPasswordController
{
    protected function resetFormAction()
    {
        return route('apollocms.account.activate.action');
    }

    protected function resetFormTitle()
    {
        return __('Activate Account');
    }

    public function broker()
    {
        $broker = config('apollocms.users.passwords.'.PasswordReset::BROKER_ACTIVATIONS);

        return Password::broker($broker);
    }
}
