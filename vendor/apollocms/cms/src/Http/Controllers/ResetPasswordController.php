<?php

namespace ApolloCMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use ApolloCMS\Auth\Passwords\PasswordReset;
use ApolloCMS\Auth\ResetsPasswords;
use ApolloCMS\Contracts\Auth\User;
use ApolloCMS\Http\Middleware\RedirectIfAuthenticated;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        $this->middleware(RedirectIfAuthenticated::class);
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('apollocms::auth.passwords.reset')->with([
            'token' => $token,
            'email' => $request->email,
            'action' => $this->resetFormAction(),
            'title' => $this->resetFormTitle(),
        ]);
    }

    protected function resetFormAction()
    {
        return route('apollocms.password.reset.action');
    }

    protected function resetFormTitle()
    {
        return __('Reset Password');
    }

    public function redirectPath()
    {
        return request('redirect') ?? route('apollocms.site');
    }

    protected function setUserPassword($user, $password)
    {
        // The ApolloCMS user class has a password method that will hash a given plain
        // text password. If we're using the "apollocms" user provider, we'll get a
        // ApolloCMS user. Otherwise (i.e. using the "eloquent" provider), we'd
        // just a User model, which requires the password to be pre-hashed.
        if ($user instanceof User) {
            $user->password($password);
        } else {
            $user->password = Hash::make($password);
        }
    }

    public function broker()
    {
        $broker = config('apollocms.users.passwords.'.PasswordReset::BROKER_RESETS);

        if (is_array($broker)) {
            $broker = $broker['web'];
        }

        return Password::broker($broker);
    }
}
