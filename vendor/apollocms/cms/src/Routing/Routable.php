<?php

namespace ApolloCMS\Routing;

use ApolloCMS\Contracts\Routing\UrlBuilder;
use ApolloCMS\Facades\URL;
use ApolloCMS\Support\Str;

trait Routable
{
    protected $slug;

    abstract public function route();

    abstract public function routeData();

    public function slug($slug = null)
    {
        return $this->fluentlyGetOrSet('slug')->setter(function ($slug) {
            return Str::slug($slug);
        })->args(func_get_args());
    }

    public function uri()
    {
        if (! $route = $this->route()) {
            return null;
        }

        return app(UrlBuilder::class)->content($this)->build($route);
    }

    public function url()
    {
        if ($this->isRedirect()) {
            return $this->redirectUrl();
        }

        return URL::makeRelative($this->absoluteUrl());
    }

    public function absoluteUrl()
    {
        if ($this->isRedirect()) {
            return $this->redirectUrl();
        }

        $url = vsprintf('%s/%s', [
            rtrim($this->site()->absoluteUrl(), '/'),
            ltrim($this->uri(), '/'),
        ]);

        return $url === '/' ? $url : rtrim($url, '/');
    }

    public function isRedirect()
    {
        return ($url = $this->redirectUrl())
            && $url !== 404;
    }

    public function redirectUrl()
    {
        if ($redirect = $this->value('redirect')) {
            return (new \ApolloCMS\Routing\ResolveRedirect)($redirect, $this);
        }
    }

    public function ampUrl()
    {
        if ($this->isRedirect()) {
            return null;
        }

        return ! $this->ampable() ? null : vsprintf('%s/%s/%s', [
            rtrim($this->site()->absoluteUrl(), '/'),
            config('apollocms.amp.route'),
            ltrim($this->uri(), '/'),
        ]);
    }
}
