<?php

namespace ApolloCMS\Widgets;

use Facades\ApolloCMS\Updater\UpdatesOverview;
use ApolloCMS\Facades\User;

class Updater extends Widget
{
    /**
     * The HTML that should be shown in the widget.
     *
     * @return \Illuminate\View\View
     */
    public function html()
    {
        if (! User::current()->can('view updates')) {
            return;
        }

        $count = UpdatesOverview::count();
        $hasApolloCMSUpdate = UpdatesOverview::hasApolloCMSUpdate();
        $updatableAddons = UpdatesOverview::updatableAddons();

        return view('apollocms::widgets.updater', compact('count', 'hasApolloCMSUpdate', 'updatableAddons'));
    }
}
