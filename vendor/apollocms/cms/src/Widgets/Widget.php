<?php

namespace ApolloCMS\Widgets;

use ApolloCMS\Extend\HasAliases;
use ApolloCMS\Extend\HasHandle;
use ApolloCMS\Extend\HasTitle;
use ApolloCMS\Extend\RegistersItself;
use ApolloCMS\Support\Str;

abstract class Widget
{
    use RegistersItself, HasTitle, HasHandle, HasAliases {
        handle as protected traitHandle;
    }

    protected static $binding = 'widgets';

    protected $config;

    /**
     * Get config for use within widget.
     *
     * @param string|null $key
     * @param mixed $default
     * @return string|\Illuminate\Support\Collection
     */
    public function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return collect($this->config);
        }

        return $this->config[$key] ?? $default;
    }

    /**
     * Set config when loading widget.
     *
     * @param array $config
     * @return array
     */
    public function setConfig($config)
    {
        $this->config = $config ?? [];
    }

    /**
     * Get container handle.
     *
     * @return string
     */
    public static function handle()
    {
        return Str::removeRight(static::traitHandle(), '_widget');
    }
}
