<?php

namespace ApolloCMS\Widgets;

use Exception;
use Facade\Ignition\Support\StringComparator;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;
use ApolloCMS\ApolloCMS;

class WidgetNotFoundException extends Exception implements ProvidesSolution
{
    protected $widget;

    public function __construct($widget)
    {
        parent::__construct("Widget [{$widget}] not found");

        $this->widget = $widget;
    }

    public function getSolution(): Solution
    {
        $description = ($suggestedWidget = $this->getSuggestedWidget())
            ? "Did you mean `$suggestedWidget`?"
            : 'Are you sure the widget exists?';

        return BaseSolution::create("The {$this->widget} widget was not found.")
            ->setSolutionDescription($description)
            ->setDocumentationLinks([
                'Read the widgets guide' => ApolloCMS::docsUrl('widgets'),
            ]);
    }

    protected function getSuggestedWidget()
    {
        return StringComparator::findClosestMatch(
            app('apollocms.widgets')->keys()->all(),
            $this->widget
        );
    }
}
