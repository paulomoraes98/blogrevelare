<?php

namespace ApolloCMS\Widgets;

class GettingStarted extends Widget
{
    /**
     * The HTML that should be shown in the widget.
     *
     * @return \Illuminate\View\View
     */
    public function html()
    {
        return view('apollocms::widgets.getting-started');
    }
}
