<?php

namespace ApolloCMS\View\Debugbar;

use ApolloCMS\Routing\Route;
use ApolloCMS\View\Events\ViewRendered;

class AddRequestMessage
{
    /**
     * Handle the event.
     *
     * @param  ViewRendered  $event
     * @return void
     */
    public function handle(ViewRendered $event)
    {
        if (! debugbar()->isEnabled()) {
            return;
        }

        if (! $item = $event->view->cascadeContent()) {
            return;
        }

        $message = "{$this->label($item)} loaded by URL Request";

        debugbar()->addMessage($message, 'apollocms');
    }

    protected function label($item)
    {
        if ($item instanceof Route) {
            return 'Route '.$item->url();
        }

        return class_basename($item).' '.$item->id();
    }
}
