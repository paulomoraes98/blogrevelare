<?php

namespace ApolloCMS\View\Events;

use ApolloCMS\Events\Event;
use ApolloCMS\View\View;

class ViewRendered extends Event
{
    /**
     * @var View
     */
    public $view;

    /**
     * @param View $view
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }
}
