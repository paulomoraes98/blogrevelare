<?php

namespace ApolloCMS\Marketplace;

use ApolloCMS\ApolloCMS;
use ApolloCMS\Updater\CoreChangelog;

class Core
{
    public function name()
    {
        return 'ApolloCMS';
    }

    public function package()
    {
        return ApolloCMS::PACKAGE;
    }

    public function changelog()
    {
        return new CoreChangelog;
    }
}
