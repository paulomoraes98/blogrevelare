<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Imaging\Manager;

/**
 * @method static string|\ApolloCMS\Contracts\Imaging\ImageManipulator manipulate($item = null, $params = null)
 * @method static \ApolloCMS\Contracts\Imaging\ImageManipulator manipulator()
 * @method static array manipulationPresets()
 * @method static array userManipulationPresets()
 * @method static array cpManipulationPresets()
 *
 * @see \ApolloCMS\Imaging\Manager
 */
class Image extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
}
