<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Forms\FormRepository;

/**
 * @method static \ApolloCMS\Contracts\Forms\Form find($handle)
 * @method static \Illuminate\Support\Collection all()
 * @method static \ApolloCMS\Contracts\Forms\Form make($handle = null)
 *
 * @see \ApolloCMS\Contracts\Forms\FormRepository
 */
class Form extends Facade
{
    protected static function getFacadeAccessor()
    {
        return FormRepository::class;
    }
}
