<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Filesystem\Manager;

/**
 * @method static \ApolloCMS\Filesystem\Filesystem disk($name = null)
 *
 * @see \ApolloCMS\Filesystem\Manager
 */
class File extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
}
