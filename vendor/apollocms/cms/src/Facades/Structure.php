<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Structures\StructureRepository;

/**
 * @method static \Illuminate\Support\Collection all()
 * @method static null|\ApolloCMS\Contracts\Structures\Structure find($id)
 * @method static null|\ApolloCMS\Contracts\Structures\Structure findByHandle($handle)
 * @method static void save(Structure $structure);
 * @method static null|\ApolloCMS\Contracts\Structures\Structure make(string $handle = null)
 *
 * @see \ApolloCMS\Contracts\Structures\StructureRepository
 */
class Structure extends Facade
{
    protected static function getFacadeAccessor()
    {
        return StructureRepository::class;
    }
}
