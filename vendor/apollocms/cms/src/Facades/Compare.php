<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Support\Comparator;

/**
 * @method static int values($one, $two)
 * @method static int strings(string $one, string $two)
 * @method static int numbers($one, $two)
 *
 * @see \ApolloCMS\Support\Comparator
 */
class Compare extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Comparator::class;
    }
}
