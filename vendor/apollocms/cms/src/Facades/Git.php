<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \ApolloCMS\Git\Git statuses($handle)
 * @method static \ApolloCMS\Git\Git commit($message = null)
 * @method static \ApolloCMS\Git\Git push($message = null)
 * @method static \ApolloCMS\Git\Git gitUserName()
 * @method static \ApolloCMS\Git\Git gitUserEmail()
 *
 * @see \ApolloCMS\Git\Git
 */
class Git extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \ApolloCMS\Git\Git::class;
    }
}
