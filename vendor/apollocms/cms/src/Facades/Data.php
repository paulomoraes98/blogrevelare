<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Data\DataRepository;

/**
 * @method static find($reference)
 * @method static findByUri($uri, $site = null)
 * @method static splitReference($reference)
 *
 * @see \ApolloCMS\Data\DataRepository
 */
class Data extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DataRepository::class;
    }
}
