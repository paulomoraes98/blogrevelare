<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Assets\AssetRepository;

/**
 * @method static all()
 * @method static whereContainer(string $container)
 * @method static whereFolder(string $folder, string $container)
 * @method static find(string $asset)
 * @method static findByUrl(string $url)
 * @method static findById(string $id)
 * @method static findByPath(string $path)
 * @method static make()
 * @method static query()
 * @method static save($asset)
 *
 * @see \ApolloCMS\Contracts\Assets\AssetRepository
 */
class Asset extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AssetRepository::class;
    }
}
