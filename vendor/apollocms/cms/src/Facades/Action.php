<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Actions\ActionRepository;

/**
 * @method static mixed get($action)
 * @method static mixed all()
 * @method static mixed for($item, $context = [])
 * @method static mixed forBulk($items, $context = [])
 *
 * @see \ApolloCMS\Actions\ActionRepository
 */
class Action extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ActionRepository::class;
    }
}
