<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Auth\UserGroupRepository;

/**
 * @method static \Illuminate\Support\Collection all()
 * @method static null|\ApolloCMS\Contracts\Auth\UserGroup find($id)
 *
 * @see \ApolloCMS\Contracts\Auth\UserGroupRepository
 */
class UserGroup extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UserGroupRepository::class;
    }
}
