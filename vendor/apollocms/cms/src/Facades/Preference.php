<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Preferences\Preferences;

/**
 * @method static array all()
 * @method static mixed get($key, $fallback = null)
 *
 * @see \ApolloCMS\Preferences\Preferences
 */
class Preference extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Preferences::class;
    }
}
