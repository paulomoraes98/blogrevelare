<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Entries\CollectionRepository;

/**
 * @method static \Illuminate\Support\Collection all()
 * @method static null|\ApolloCMS\Entries\Collection find($id)
 * @method static null|\ApolloCMS\Entries\Collection findByHandle($handle)
 * @method static null|\ApolloCMS\Entries\Collection findByMount($mount)
 * @method static \ApolloCMS\Entries\Collection make(string $handle = null)
 * @method static \Illuminate\Support\Collection handles()
 * @method static bool handleExists(string $handle)
 * @method static \Illuminate\Support\Collection whereStructured()
 *
 * @see \Illuminate\Support\Collection
 * @see \ApolloCMS\Entries\Collection
 */
class Collection extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CollectionRepository::class;
    }
}
