<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Markdown\Manager;

/**
 * @method static \ApolloCMS\Markdown\Parser makeParser(array $config = [])
 * @method static mixed|\ApolloCMS\Markdown\Parser parser(string $name)
 * @method static bool hasParser(string $name)
 * @method static void extend(string $name, \Closure $closure)
 *
 * @see \ApolloCMS\Markdown\Manager
 */
class Markdown extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
}
