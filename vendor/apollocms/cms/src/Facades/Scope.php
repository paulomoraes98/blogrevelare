<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Query\Scopes\ScopeRepository;

/**
 * @method static mixed all()
 * @method static mixed find($key, $context = [])
 * @method static mixed filters($key, $context = [])
 *
 * @see \ApolloCMS\Query\Scopes\ScopeRepository
 */
class Scope extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ScopeRepository::class;
    }
}
