<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Auth\RoleRepository;

/**
 * @method static \Illuminate\Support\Collection all()
 * @method static null|\ApolloCMS\Contracts\Auth\Role find(string $id)
 * @method static bool exists(string $id)
 * @method static \ApolloCMS\Contracts\Auth\Role make(string $handle = null)
 *
 * @see \ApolloCMS\Contracts\Auth\RoleRepository
 */
class Role extends Facade
{
    protected static function getFacadeAccessor()
    {
        return RoleRepository::class;
    }
}
