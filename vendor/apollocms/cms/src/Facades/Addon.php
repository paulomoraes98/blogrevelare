<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Extend\AddonRepository;

/**
 * @method static \ApolloCMS\Extend\Addon make($addon)
 * @method static \Illuminate\Support\Collection all()
 * @method static \ApolloCMS\Extend\Addon get($id)
 *
 * @see \ApolloCMS\Extend\AddonRepository
 */
class Addon extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AddonRepository::class;
    }
}
