<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Fields\BlueprintRepository;

/**
 * @method static self setDirectory(string $directory)
 * @method static self setFallbackDirectory(string $directory)
 * @method static null|\ApolloCMS\Fields\Blueprint find($handle)
 * @method static \Illuminate\Support\Collection all()
 * @method static void save(Blueprint $blueprint)
 * @method static void delete(Blueprint $blueprint)
 * @method static \ApolloCMS\Fields\Blueprint make($handle = null)
 * @method static \ApolloCMS\Fields\Blueprint makeFromFields($fields)
 * @method static \ApolloCMS\Fields\Blueprint makeFromSections($sections)
 *
 * @see \ApolloCMS\Fields\Blueprint
 */
class Blueprint extends Facade
{
    protected static function getFacadeAccessor()
    {
        return BlueprintRepository::class;
    }
}
