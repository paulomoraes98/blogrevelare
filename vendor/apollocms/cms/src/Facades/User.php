<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Auth\UserRepository;

/**
 * @method static \ApolloCMS\Contracts\Auth\User make()
 * @method static \ApolloCMS\Auth\UserCollection all()
 * @method static null|\ApolloCMS\Contracts\Auth\User find($id)
 * @method static null|\ApolloCMS\Contracts\Auth\User findByEmail(string $email)
 * @method static null|\ApolloCMS\Contracts\Auth\User findByOAuthId(string $provider, string $id)
 * @method static null|\ApolloCMS\Contracts\Auth\User current()
 * @method static null|\ApolloCMS\Contracts\Auth\User fromUser($user)
 * @method static void save(User $user);
 * @method static void delete(User $user);
 * @method static \ApolloCMS\Fields\Blueprint blueprint();
 *
 * @see \ApolloCMS\Contracts\Auth\UserRepository
 */
class User extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UserRepository::class;
    }
}
