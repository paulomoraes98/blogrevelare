<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Structures\NavigationRepository;

/**
 * @method static \Illuminate\Support\Collection all()
 * @method static null|\ApolloCMS\Contracts\Structures\Nav find($id)
 * @method static null|\ApolloCMS\Contracts\Structures\Nav findByHandle($handle)
 * @method static void save(Nav $nav)
 * @method static \ApolloCMS\Contracts\Structures\Nav make(string $handle = null)
 *
 * @see \ApolloCMS\Contracts\Structures\NavigationRepository
 */
class Nav extends Facade
{
    protected static function getFacadeAccessor()
    {
        return NavigationRepository::class;
    }
}
