<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed parser()
 * @method static mixed usingParser(Parser $parser, Closure $callback)
 * @method static mixed parse($str, $variables = [])
 * @method static string parseLoop($content, $data, $supplement = true, $context = [])
 *
 * @see \ApolloCMS\View\Lunar\Lunar
 */
class Lunar extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \ApolloCMS\View\Lunar\Lunar::class;
    }
}
