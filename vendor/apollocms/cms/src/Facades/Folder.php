<?php

namespace ApolloCMS\Facades;

/**
 * @method static \ApolloCMS\Filesystem\Filesystem disk($name = null)
 *
 * @see \ApolloCMS\Filesystem\Manager
 */
class Folder extends File
{
    //
}
