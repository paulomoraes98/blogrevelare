<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed|\ApolloCMS\Blink\Blink store($name = 'default')
 *
 * @see ApolloCMS\Support\Blink
 */
class Blink extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \ApolloCMS\Support\Blink::class;
    }
}
