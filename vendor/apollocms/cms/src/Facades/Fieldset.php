<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Fields\FieldsetRepository;

/**
 * @method static self setDirectory($directory)
 * @method static null|\ApolloCMS\Fields\Fieldset find(string $handle)
 * @method static bool exists(string $handle)
 * @method static \ApolloCMS\Fields\Fieldset make($handle = null)
 * @method static \Illuminate\Support\Collection all()
 * @method static void save(Fieldset $fieldset)
 * @method static void delete(Fieldset $fieldset)
 *
 * @see \ApolloCMS\Fields\FieldsetRepository
 */
class Fieldset extends Facade
{
    protected static function getFacadeAccessor()
    {
        return FieldsetRepository::class;
    }
}
