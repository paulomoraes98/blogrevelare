<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Taxonomies\Term as TermContract;
use ApolloCMS\Contracts\Taxonomies\TermRepository;
use ApolloCMS\Apoche\Query\TermQueryBuilder;
use ApolloCMS\Taxonomies\TermCollection;

/**
 * @method static TermCollection all()
 * @method static TermCollection whereTaxonomy(string $handle)
 * @method static TermCollection whereInTaxonomy(array $handles)
 * @method static TermContract find($id)
 * @method static TermContract findByUri(string $uri, string $site = null)
 * @method static TermContract findBySlug(string $slug, string $taxonomy)
 * @method static save($term)
 * @method static delete($term)
 * @method static TermQueryBuilder query()
 * @method static TermContract make(string $slug = null)
 * @method static int entriesCount(Term $term)
 *
 * @see \ApolloCMS\Contracts\Taxonomies\TermRepository
 */
class Term extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TermRepository::class;
    }
}
