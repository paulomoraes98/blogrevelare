<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed indexes()
 * @method static mixed index($index = null)
 * @method static mixed in($index = null)
 * @method static mixed clearIndex($index = null)
 * @method static mixed indexExists($index = null)
 * @method static void extend($driver, $callback)
 *
 * @see \ApolloCMS\Search\Search
 */
class Search extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \ApolloCMS\Search\Search::class;
    }
}
