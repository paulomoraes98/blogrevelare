<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\CP\Utilities\UtilityRepository;

/**
 * @method static mixed|null make($handle)
 * @method static self push(Utility $utility)
 * @method static mixed all()
 * @method static mixed authorized()
 * @method static mixed find($handle)
 * @method static void routes()
 *
 * @see \ApolloCMS\CP\Utilities\UtilityRepository
 */
class Utility extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UtilityRepository::class;
    }
}
