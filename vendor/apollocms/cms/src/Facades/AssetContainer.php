<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Assets\AssetContainerRepository;

/**
 * @method static \lluminate\Support\Collection all()
 * @method static null|\ApolloCMS\Contracts\Assets\AssetContainer find($id)
 * @method static null|\ApolloCMS\Contracts\Assets\AssetContainer findByHandle(string $handle)
 * @method static \ApolloCMS\Contracts\Assets\AssetContainer make(string $handle = null)
 *
 * @see \ApolloCMS\Assets\AssetRepository
 */
class AssetContainer extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AssetContainerRepository::class;
    }
}
