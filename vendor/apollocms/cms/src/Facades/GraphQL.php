<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\GraphQL\Manager;

class GraphQL extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
}
