<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Entries\EntryRepository;

/**
 * @method static \ApolloCMS\Entries\EntryCollection all()
 * @method static \ApolloCMS\Entries\EntryCollection whereCollection(string $handle)
 * @method static \ApolloCMS\Entries\EntryCollection whereInCollection(array $handles)
 * @method static null|\ApolloCMS\Contracts\Entries\Entry find($id)
 * @method static null|\ApolloCMS\Contracts\Entries\Entry findByUri(string $uri)
 * @method static null|\ApolloCMS\Contracts\Entries\Entry findBySlug(string $slug, string $collection)
 * @method static \ApolloCMS\Contracts\Entries\Entry make()
 * @method static \ApolloCMS\Contracts\Entries\QueryBuilder query()
 * @method static void save($entry)
 * @method static void delete($entry)
 * @method static array createRules($collection, $site)
 * @method static array updateRules($collection, $entry)
 *
 * @see \ApolloCMS\Apoche\Repositories\EntryRepository
 * @see \ApolloCMS\Apoche\Query\EntryQueryBuilder
 * @see \ApolloCMS\Entries\EntryCollection
 * @see \ApolloCMS\Entries\Entry
 */
class Entry extends Facade
{
    protected static function getFacadeAccessor()
    {
        return EntryRepository::class;
    }
}
