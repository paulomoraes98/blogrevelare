<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\OAuth\Manager;

/**
 * @method static bool enabled()
 * @method static mixed|\ApolloCMS\OAuth\Provider Providerprovider($provider)
 * @method static mixed providers()
 *
 * @see \ApolloCMS\OAuth\Provider
 */
class OAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
}
