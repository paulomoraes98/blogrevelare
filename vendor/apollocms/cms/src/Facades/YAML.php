<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static self file($file)
 * @method static array parse($str = null)
 * @method static string dump($data, $content = null)
 * @method static string dumpFrontMatter($data, $content = null)
 *
 * @see \ApolloCMS\Yaml\Yaml
 */
class YAML extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \ApolloCMS\Yaml\Yaml::class;
    }
}
