<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Globals\GlobalRepository;

/**
 * @method static \ApolloCMS\Globals\GlobalCollection all()
 * @method static null|\ApolloCMS\Globals\GlobalCollection find($id)
 * @method static null|\ApolloCMS\Globals\GlobalCollection findByHandle($handle)
 * @method static void save($global);
 *
 * @see \ApolloCMS\Globals\GlobalCollection
 */
class GlobalSet extends Facade
{
    protected static function getFacadeAccessor()
    {
        return GlobalRepository::class;
    }
}
