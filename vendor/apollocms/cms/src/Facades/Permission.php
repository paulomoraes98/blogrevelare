<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Auth\Permissions;

/**
 * @method static \ApolloCMS\Auth\Permission make(string $value)
 * @method static \ApolloCMS\Auth\Permission register($permission, $callback = null)
 * @method static mixed all()
 * @method static mixed get($key)
 * @method static mixed tree()
 * @method static mixed|null group($name, $label, $permissions = null)
 *
 * @see \ApolloCMS\Auth\Permissions
 */
class Permission extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Permissions::class;
    }
}
