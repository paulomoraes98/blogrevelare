<?php

namespace ApolloCMS\Facades;

use Illuminate\Support\Facades\Facade;
use ApolloCMS\Contracts\Revisions\RevisionRepository;

/**
 * @method static mixed whereKey($key)
 * @method static \ApolloCMS\Revisions\Revision findWorkingCopyByKey($key)
 * @method static void save(Revision $revision)
 * @method static void delete(Revision $revision)
 * @method static \ApolloCMS\Contracts\Revisions\Revision make()
 *
 * @see \ApolloCMS\Revisions\RevisionRepository
 * @see \ApolloCMS\Revisions\Revision
 */
class Revision extends Facade
{
    protected static function getFacadeAccessor()
    {
        return RevisionRepository::class;
    }
}
