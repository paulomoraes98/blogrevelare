<?php

namespace ApolloCMS\Updater;

use ApolloCMS\ApolloCMS;

class CoreChangelog extends Changelog
{
    public function item()
    {
        return ApolloCMS::PACKAGE;
    }

    public function currentVersion()
    {
        return ApolloCMS::version();
    }
}
