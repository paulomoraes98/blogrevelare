<?php

namespace ApolloCMS\Updater;

use Carbon\Carbon;
use Facades\ApolloCMS\Marketplace\Marketplace;
use Illuminate\Support\Facades\Cache;
use ApolloCMS\Facades\Addon;

class UpdatesOverview
{
    const CACHE_FOR_MINUTES = 60;

    protected $count;
    protected $apollocms;
    protected $addons;

    /**
     * Get updates count.
     *
     * @param bool $clearCache
     * @return int
     */
    public function count($clearCache = false)
    {
        return $this->getCached('updates-overview.count', $clearCache);
    }

    /**
     * Check if apollocms update is available.
     *
     * @param bool $clearCache
     * @return bool
     */
    public function hasApolloCMSUpdate($clearCache = false)
    {
        return $this->getCached('updates-overview.apollocms', $clearCache);
    }

    /**
     * List updatable addons.
     *
     * @param bool $clearCache
     * @return array
     */
    public function updatableAddons($clearCache = false)
    {
        return $this->getCached('updates-overview.addons', $clearCache);
    }

    /**
     * Get value from cache.
     *
     * @param string $key
     * @param bool $clearCache
     * @return mixed
     */
    public function getCached($key, $clearCache = false)
    {
        if (! Cache::has($key) || $clearCache) {
            $this->checkAndCache();
        }

        return Cache::get($key);
    }

    /**
     * Check for updates and cache results.
     *
     * @return $this
     */
    protected function checkAndCache()
    {
        return $this
            ->resetState()
            ->checkForApolloCMSUpdates()
            ->checkForAddonUpdates()
            ->cache();
    }

    /**
     * Reset state.
     */
    protected function resetState()
    {
        $this->count = 0;
        $this->apollocms = false;
        $this->addons = [];

        return $this;
    }

    /**
     * Check for apollocms updates and increment count.
     *
     * @return $this
     */
    protected function checkForApolloCMSUpdates()
    {
        if (Marketplace::apollocms()->changelog()->latest()->type === 'upgrade') {
            $this->apollocms = true;
            $this->count++;
        }

        return $this;
    }

    /**
     * Check for addon updates and increment count.
     *
     * @return $this
     */
    protected function checkForAddonUpdates()
    {
        Addon::all()
            ->reject->isLatestVersion()
            ->each(function ($addon) {
                $this->addons[$addon->marketplaceSlug()] = $addon->name();
                $this->count++;
            });

        return $this;
    }

    /**
     * Cache data.
     *
     * @return $this
     */
    protected function cache()
    {
        $expiry = Carbon::now()->addMinutes(self::CACHE_FOR_MINUTES);

        Cache::put('updates-overview.count', $this->count, $expiry);
        Cache::put('updates-overview.apollocms', $this->apollocms, $expiry);
        Cache::put('updates-overview.addons', $this->addons, $expiry);

        return $this;
    }
}
