<?php

namespace ApolloCMS\API\Cachers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use ApolloCMS\API\Cacher;
use ApolloCMS\Events\Event;

class NullCacher implements Cacher
{
    public function get(Request $request)
    {
        //
    }

    public function put(Request $request, JsonResponse $response)
    {
        //
    }

    public function handleInvalidationEvent(Event $event)
    {
        //
    }
}
