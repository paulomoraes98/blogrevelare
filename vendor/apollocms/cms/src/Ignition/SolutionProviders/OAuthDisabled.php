<?php

namespace ApolloCMS\Ignition\SolutionProviders;

use Facade\IgnitionContracts\HasSolutionsForThrowable;
use ApolloCMS\Ignition\Solutions\EnableOAuth;
use Throwable;

class OAuthDisabled implements HasSolutionsForThrowable
{
    public function canSolve(Throwable $throwable): bool
    {
        return $throwable->getMessage() === 'Route [apollocms.oauth.login] not defined.';
    }

    public function getSolutions(Throwable $throwable): array
    {
        return [new EnableOAuth];
    }
}
