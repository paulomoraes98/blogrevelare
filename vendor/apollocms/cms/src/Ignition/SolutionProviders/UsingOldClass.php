<?php

namespace ApolloCMS\Ignition\SolutionProviders;

use Facade\IgnitionContracts\HasSolutionsForThrowable;
use ApolloCMS\Ignition\Solutions\UpdateClassReference;
use ApolloCMS\ApolloCMS;
use ApolloCMS\Support\Arr;
use Throwable;

class UsingOldClass implements HasSolutionsForThrowable
{
    protected $class;

    public function canSolve(Throwable $throwable): bool
    {
        if (! $this->oldClass = $this->getClassFromThrowable($throwable)) {
            return false;
        }

        return Arr::has($this->classes(), $this->oldClass);
    }

    public function getSolutions(Throwable $throwable): array
    {
        $class = $this->classes()[$this->oldClass];

        return [new UpdateClassReference($this->oldClass, $class['class'], $class['docs'] ?? [])];
    }

    protected function getClassFromThrowable($throwable)
    {
        if (! preg_match('/Class \'(.*)\' not found/', $throwable->getMessage(), $matches)) {
            return null;
        }

        return $matches[1];
    }

    protected function classes()
    {
        return [
            'ApolloCMS\Extend\Fieldtype' => [
                'class' => \ApolloCMS\Fields\Fieldtype::class,
                'docs' => ['Fieldtypes Documentation' => ApolloCMS::docsUrl('extending/fieldtypes')],
            ],
            'ApolloCMS\Extend\Modifier' => [
                'class' => \ApolloCMS\Modifiers\Modifier::class,
                'docs' => ['Modifiers Documentation' => ApolloCMS::docsUrl('extending/modifiers')],
            ],
            'ApolloCMS\Extend\Tags' => [
                'class' => \ApolloCMS\Tags\Tags::class,
                'docs' => ['Tags Documentation' => ApolloCMS::docsUrl('extending/tags')],
            ],
            'ApolloCMS\Extend\Widget' => [
                'class' => \ApolloCMS\Widgets\Widget::class,
                'docs' => ['Widgets Documentation' => ApolloCMS::docsUrl('extending/widgets')],
            ],
        ];
    }
}
