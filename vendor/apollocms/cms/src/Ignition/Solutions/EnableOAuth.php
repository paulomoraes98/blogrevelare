<?php

namespace ApolloCMS\Ignition\Solutions;

use Facade\IgnitionContracts\Solution;
use ApolloCMS\ApolloCMS;

class EnableOAuth implements Solution
{
    public function getSolutionTitle(): string
    {
        return 'OAuth is disabled';
    }

    public function getSolutionDescription(): string
    {
        return 'Enable it by setting `APOLLOCMS_OAUTH_ENABLED=true` in your `.env` file.';
    }

    public function getDocumentationLinks(): array
    {
        return [
            'OAuth Guide' => ApolloCMS::docsUrl('/oauth'),
        ];
    }
}
