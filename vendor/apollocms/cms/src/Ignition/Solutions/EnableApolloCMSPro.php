<?php

namespace ApolloCMS\Ignition\Solutions;

use Facade\IgnitionContracts\RunnableSolution;
use ApolloCMS\ApolloCMS;

class EnableApolloCMSPro implements RunnableSolution
{
    public function getSolutionTitle(): string
    {
        return 'ApolloCMS Pro is required';
    }

    public function getSolutionDescription(): string
    {
        return 'Enable it by setting `\'pro\' => true` in `config/apollocms/editions.php`.';
    }

    public function getDocumentationLinks(): array
    {
        return [
            'Licensing documentation' => ApolloCMS::docsUrl('licensing'),
        ];
    }

    public function getSolutionActionDescription(): string
    {
        return 'ApolloCMS can attempt to enable it for you.';
    }

    public function getRunButtonText(): string
    {
        return 'Enable ApolloCMS Pro';
    }

    public function run(array $parameters = [])
    {
        ApolloCMS::enablePro();
    }

    public function getRunParameters(): array
    {
        return [];
    }
}
