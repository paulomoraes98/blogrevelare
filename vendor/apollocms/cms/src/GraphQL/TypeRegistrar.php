<?php

namespace ApolloCMS\GraphQL;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\GraphQL\Types\AssetContainerType;
use ApolloCMS\GraphQL\Types\AssetInterface;
use ApolloCMS\GraphQL\Types\CollectionStructureType;
use ApolloCMS\GraphQL\Types\CollectionType;
use ApolloCMS\GraphQL\Types\DateRangeType;
use ApolloCMS\GraphQL\Types\EntryInterface;
use ApolloCMS\GraphQL\Types\GlobalSetInterface;
use ApolloCMS\GraphQL\Types\JsonArgument;
use ApolloCMS\GraphQL\Types\LabeledValueType;
use ApolloCMS\GraphQL\Types\NavType;
use ApolloCMS\GraphQL\Types\PageInterface;
use ApolloCMS\GraphQL\Types\RoleType;
use ApolloCMS\GraphQL\Types\SiteType;
use ApolloCMS\GraphQL\Types\TableRowType;
use ApolloCMS\GraphQL\Types\TaxonomyType;
use ApolloCMS\GraphQL\Types\TermInterface;
use ApolloCMS\GraphQL\Types\TreeBranchType;
use ApolloCMS\GraphQL\Types\UserGroupType;
use ApolloCMS\GraphQL\Types\UserType;

class TypeRegistrar
{
    private $registered = false;

    public function register()
    {
        if ($this->registered) {
            return;
        }

        GraphQL::addType(JsonArgument::class);
        GraphQL::addType(DateRangeType::class);
        GraphQL::addType(SiteType::class);
        GraphQL::addType(LabeledValueType::class);
        GraphQL::addType(CollectionType::class);
        GraphQL::addType(CollectionStructureType::class);
        GraphQL::addType(TaxonomyType::class);
        GraphQL::addType(AssetContainerType::class);
        GraphQL::addType(NavType::class);
        GraphQL::addType(TreeBranchType::class);
        GraphQL::addType(UserType::class);
        GraphQL::addType(UserGroupType::class);
        GraphQL::addType(RoleType::class);
        GraphQL::addType(TableRowType::class);
        PageInterface::addTypes();
        EntryInterface::addTypes();
        TermInterface::addTypes();
        AssetInterface::addTypes();
        GlobalSetInterface::addTypes();

        $this->registered = true;
    }
}
