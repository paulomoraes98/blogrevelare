<?php

namespace ApolloCMS\GraphQL\ResponseCache;

use Illuminate\Http\Request;
use ApolloCMS\Contracts\GraphQL\ResponseCache;
use ApolloCMS\Events\Event;

class NullCache implements ResponseCache
{
    public function get(Request $request)
    {
        //
    }

    public function put(Request $request, $response)
    {
        //
    }

    public function handleInvalidationEvent(Event $event)
    {
        //
    }
}
