<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Nav;
use ApolloCMS\GraphQL\Types\NavType;

class NavQuery extends Query
{
    protected $attributes = [
        'name' => 'nav',
    ];

    public function type(): Type
    {
        return GraphQL::type(NavType::NAME);
    }

    public function args(): array
    {
        return [
            'handle' => GraphQL::string(),
        ];
    }

    public function resolve($root, $args)
    {
        return Nav::findByHandle($args['handle']);
    }
}
