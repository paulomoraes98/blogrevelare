<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Site;
use ApolloCMS\GraphQL\Types\SiteType;

class SitesQuery extends Query
{
    protected $attributes = [
        'name' => 'sites',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(SiteType::NAME));
    }

    public function resolve($root, $args)
    {
        return Site::all();
    }
}
