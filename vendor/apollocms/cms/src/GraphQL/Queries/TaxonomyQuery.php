<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Taxonomy;
use ApolloCMS\GraphQL\Types\TaxonomyType;

class TaxonomyQuery extends Query
{
    protected $attributes = [
        'name' => 'taxonomy',
    ];

    public function type(): Type
    {
        return GraphQL::type(TaxonomyType::NAME);
    }

    public function args(): array
    {
        return [
            'handle' => GraphQL::string(),
        ];
    }

    public function resolve($root, $args)
    {
        return Taxonomy::findByHandle($args['handle']);
    }
}
