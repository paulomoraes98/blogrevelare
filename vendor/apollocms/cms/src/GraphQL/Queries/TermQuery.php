<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Term;
use ApolloCMS\GraphQL\Types\TermInterface;

class TermQuery extends Query
{
    protected $attributes = [
        'name' => 'term',
    ];

    public function type(): Type
    {
        return GraphQL::type(TermInterface::NAME);
    }

    public function args(): array
    {
        return [
            'id' => GraphQL::string(),
        ];
    }

    public function resolve($root, $args)
    {
        $query = Term::query();

        if ($id = $args['id']) {
            $query->where('id', $id);
        }

        return $query->limit(1)->get()->first();
    }
}
