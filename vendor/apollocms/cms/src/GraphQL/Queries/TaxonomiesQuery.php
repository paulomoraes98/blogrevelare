<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Taxonomy;
use ApolloCMS\GraphQL\Types\TaxonomyType;

class TaxonomiesQuery extends Query
{
    protected $attributes = [
        'name' => 'taxonomies',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(TaxonomyType::NAME));
    }

    public function resolve($root, $args)
    {
        return Taxonomy::all();
    }
}
