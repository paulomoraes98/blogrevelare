<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Nav;
use ApolloCMS\GraphQL\Types\NavType;

class NavsQuery extends Query
{
    protected $attributes = [
        'name' => 'navs',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(NavType::NAME));
    }

    public function resolve($root, $args)
    {
        return Nav::all();
    }
}
