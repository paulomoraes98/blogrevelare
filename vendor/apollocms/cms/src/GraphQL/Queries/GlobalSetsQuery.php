<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\GlobalSet;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Facades\Site;
use ApolloCMS\GraphQL\Types\GlobalSetInterface;

class GlobalSetsQuery extends Query
{
    protected $attributes = [
        'name' => 'globalSets',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(GlobalSetInterface::NAME));
    }

    public function resolve($root, $args)
    {
        $site = Site::default()->handle();

        return GlobalSet::all()->map->in($site);
    }
}
