<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\GraphQL\Types\CollectionType;

class CollectionQuery extends Query
{
    protected $attributes = [
        'name' => 'collection',
    ];

    public function type(): Type
    {
        return GraphQL::type(CollectionType::NAME);
    }

    public function args(): array
    {
        return [
            'handle' => GraphQL::string(),
        ];
    }

    public function resolve($root, $args)
    {
        return Collection::findByHandle($args['handle']);
    }
}
