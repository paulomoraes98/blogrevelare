<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\AssetContainer;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\GraphQL\Types\AssetContainerType;

class AssetContainersQuery extends Query
{
    protected $attributes = [
        'name' => 'assetContainers',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(AssetContainerType::NAME));
    }

    public function resolve($root, $args)
    {
        return AssetContainer::all();
    }
}
