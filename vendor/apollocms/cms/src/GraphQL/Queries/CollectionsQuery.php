<?php

namespace ApolloCMS\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\GraphQL\Types\CollectionType;

class CollectionsQuery extends Query
{
    protected $attributes = [
        'name' => 'collections',
    ];

    public function type(): Type
    {
        return GraphQL::listOf(GraphQL::type(CollectionType::NAME));
    }

    public function resolve($root, $args)
    {
        return Collection::all();
    }
}
