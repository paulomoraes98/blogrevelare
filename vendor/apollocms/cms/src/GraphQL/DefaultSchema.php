<?php

namespace ApolloCMS\GraphQL;

use Rebing\GraphQL\Support\Contracts\ConfigConvertible;
use ApolloCMS\Facades\GraphQL;
use ApolloCMS\GraphQL\Middleware\CacheResponse;
use ApolloCMS\GraphQL\Queries\AssetContainerQuery;
use ApolloCMS\GraphQL\Queries\AssetContainersQuery;
use ApolloCMS\GraphQL\Queries\AssetQuery;
use ApolloCMS\GraphQL\Queries\AssetsQuery;
use ApolloCMS\GraphQL\Queries\CollectionQuery;
use ApolloCMS\GraphQL\Queries\CollectionsQuery;
use ApolloCMS\GraphQL\Queries\EntriesQuery;
use ApolloCMS\GraphQL\Queries\EntryQuery;
use ApolloCMS\GraphQL\Queries\GlobalSetQuery;
use ApolloCMS\GraphQL\Queries\GlobalSetsQuery;
use ApolloCMS\GraphQL\Queries\NavQuery;
use ApolloCMS\GraphQL\Queries\NavsQuery;
use ApolloCMS\GraphQL\Queries\PingQuery;
use ApolloCMS\GraphQL\Queries\SitesQuery;
use ApolloCMS\GraphQL\Queries\TaxonomiesQuery;
use ApolloCMS\GraphQL\Queries\TaxonomyQuery;
use ApolloCMS\GraphQL\Queries\TermQuery;
use ApolloCMS\GraphQL\Queries\TermsQuery;
use ApolloCMS\GraphQL\Queries\UserQuery;
use ApolloCMS\GraphQL\Queries\UsersQuery;

class DefaultSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return $this->getConfig();
    }

    public static function config()
    {
        return app(self::class)->getConfig();
    }

    public function getConfig()
    {
        return [
            'query' => $this->getQueries(),
            'mutation' => [],
            'middleware' => $this->getMiddleware(),
            'method' => ['get', 'post'],
        ];
    }

    private function getQueries()
    {
        $queries = collect([PingQuery::class]);

        collect([
            'collections' => [CollectionsQuery::class, CollectionQuery::class, EntriesQuery::class, EntryQuery::class],
            'assets' => [AssetContainersQuery::class, AssetContainerQuery::class, AssetsQuery::class, AssetQuery::class],
            'taxonomies' => [TaxonomiesQuery::class, TaxonomyQuery::class, TermsQuery::class, TermQuery::class],
            'globals' => [GlobalSetsQuery::class, GlobalSetQuery::class],
            'navs' => [NavsQuery::class, NavQuery::class],
            'sites' => [SitesQuery::class],
            'users' => [UsersQuery::class, UserQuery::class],
        ])->each(function ($qs, $key) use (&$queries) {
            $queries = $queries->merge(config('apollocms.graphql.resources.'.$key) ? $qs : []);
        });

        return $queries
            ->merge(config('apollocms.graphql.queries', []))
            ->merge(GraphQL::getExtraQueries())
            ->all();
    }

    private function getMiddleware()
    {
        return array_merge(
            [CacheResponse::class],
            config('apollocms.graphql.middleware', []),
            GraphQL::getExtraMiddleware()
        );
    }
}
