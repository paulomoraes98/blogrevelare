<?php

namespace ApolloCMS\GraphQL;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as LaravelProvider;
use ApolloCMS\Contracts\GraphQL\ResponseCache;
use ApolloCMS\GraphQL\ResponseCache\DefaultCache;
use ApolloCMS\GraphQL\ResponseCache\NullCache;
use ApolloCMS\Http\Middleware\API\SwapExceptionHandler;
use ApolloCMS\Http\Middleware\RequireApolloCMSPro;

class ServiceProvider extends LaravelProvider
{
    public function register()
    {
        $this->app->bind(ResponseCache::class, function ($app) {
            return config('apollocms.graphql.cache') === false
                ? new NullCache
                : new DefaultCache;
        });

        $this->app->booting(function () {
            if ($this->hasPublishedConfig()) {
                return;
            }

            if (! config('apollocms.graphql.enabled')) {
                config(['graphql.routes' => false]);
            }

            $this->addMiddleware();
            $this->disableGraphiql();
            $this->setDefaultSchema();
        });
    }

    public function boot()
    {
        Event::subscribe(Subscriber::class);
    }

    private function hasPublishedConfig()
    {
        return $this->app['files']->exists(config_path('graphql.php'));
    }

    private function addMiddleware()
    {
        config(['graphql.middleware' => [
            SwapExceptionHandler::class,
            RequireApolloCMSPro::class,
        ]]);
    }

    private function disableGraphiql()
    {
        config(['graphql.graphiql.display' => false]);
    }

    private function setDefaultSchema()
    {
        config(['graphql.schemas.default' => DefaultSchema::class]);
    }
}
