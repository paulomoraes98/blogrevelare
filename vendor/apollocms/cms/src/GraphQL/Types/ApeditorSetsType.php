<?php

namespace ApolloCMS\GraphQL\Types;

use ApolloCMS\Facades\GraphQL;

class ApeditorSetsType extends ReplicatorSetsType
{
    public function resolveType($value)
    {
        return $value['type'] === 'text'
            ? GraphQL::type(ApeditorTextType::NAME)
            : parent::resolveType($value);
    }
}
