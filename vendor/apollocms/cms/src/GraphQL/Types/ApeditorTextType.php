<?php

namespace ApolloCMS\GraphQL\Types;

use ApolloCMS\Facades\GraphQL;

class ApeditorTextType extends \Rebing\GraphQL\Support\Type
{
    const NAME = 'ApeditorText';

    protected $attributes = [
        'name' => self::NAME,
    ];

    protected $fieldtype;

    public function __construct($fieldtype)
    {
        $this->fieldtype = $fieldtype;
    }

    public function fields(): array
    {
        return [
            'type' => [
                'type' => GraphQL::nonNull(GraphQL::string()),
            ],
            'text' => [
                'type' => GraphQL::string(),
            ],
        ];
    }
}
