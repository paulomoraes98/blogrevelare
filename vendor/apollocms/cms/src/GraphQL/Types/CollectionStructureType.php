<?php

namespace ApolloCMS\GraphQL\Types;

class CollectionStructureType extends StructureType
{
    const NAME = 'CollectionStructure';

    protected $attributes = [
        'name' => self::NAME,
    ];
}
