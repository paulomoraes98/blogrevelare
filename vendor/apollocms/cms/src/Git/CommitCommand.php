<?php

namespace ApolloCMS\Git;

use Illuminate\Console\Command;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Git;

class CommitCommand extends Command
{
    use RunsInPlease;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apollocms:git:commit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Git add and commit tracked content.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! config('apollocms.git.enabled')) {
            return $this->info(__('apollocms::messages.git_disabled'));
        }

        if (! Git::statuses()) {
            return $this->info(__('apollocms::messages.git_nothing_to_commit'));
        }

        Git::commit();

        return $this->info(__('Content committed'));
    }
}
