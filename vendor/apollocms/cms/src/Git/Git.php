<?php

namespace ApolloCMS\Git;

use Illuminate\Filesystem\Filesystem;
use ApolloCMS\Console\Processes\Git as GitProcess;
use ApolloCMS\Facades\Lunar;
use ApolloCMS\Facades\Path;
use ApolloCMS\Facades\User;
use ApolloCMS\Support\Str;

class Git
{
    /**
     * Instantiate git tracked content manager.
     */
    public function __construct()
    {
        if (! config('apollocms.git.enabled')) {
            throw new \Exception(__('apollocms::messages.git_disabled'));
        }
    }

    /**
     * Listen to custom addon event.
     *
     * @param string $event
     */
    public function listen($event)
    {
        \Illuminate\Support\Facades\Event::listen($event, Subscriber::class.'@commit');
    }

    /**
     * Get statuses of tracked content paths.
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function statuses()
    {
        $statuses = $this
            ->groupTrackedContentPathsByRepo()
            ->map(function ($paths, $gitRoot) {
                return GitProcess::create($gitRoot)
                    ->colorized(true) // TODO: Why is it not colorizing?
                    ->status($paths);
            })
            ->map(function ($status) {
                return (object) $this->statusWithFileCounts($status);
            })
            ->filter
            ->totalCount;

        return $statuses->isNotEmpty() ? $statuses : null;
    }

    /**
     * Git add and commit all tracked content, using configured commands.
     */
    public function commit($message = null)
    {
        $this->groupTrackedContentPathsByRepo()->each(function ($paths, $gitRoot) use ($message) {
            $this->runConfiguredCommands($gitRoot, $paths, $message ?? __('Content saved'));
        });
    }

    /**
     * Dispatch commit job to queue.
     */
    public function dispatchCommit($message = null)
    {
        if ($delay = config('apollocms.git.dispatch_delay')) {
            $delayInMinutes = now()->addMinutes($delay);
            $message = null;
        }

        CommitJob::dispatch($message)
            ->onConnection(config('apollocms.git.queue_connection'))
            ->delay($delayInMinutes ?? null);
    }

    /**
     * Get git user name.
     *
     * @return string
     */
    public function gitUserName()
    {
        $default = config('apollocms.git.user.name');

        if (! config('apollocms.git.use_authenticated')) {
            return $default;
        }

        $currentUser = User::current();

        return $currentUser ? $currentUser->name() : $default;
    }

    /**
     * Get git user email.
     *
     * @return string
     */
    public function gitUserEmail()
    {
        $default = config('apollocms.git.user.email');

        if (! config('apollocms.git.use_authenticated')) {
            return $default;
        }

        $currentUser = User::current();

        return $currentUser ? $currentUser->email() : $default;
    }

    /**
     * Group tracked content paths by repo.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function groupTrackedContentPathsByRepo()
    {
        return collect(config('apollocms.git.paths'))
            ->map(function ($path) {
                return $this->ensureAbsolutePath($path);
            })
            ->filter(function ($path) {
                return app(Filesystem::class)->exists($path);
            })
            ->filter(function ($path) {
                return GitProcess::create($path)->status();
            })
            ->groupBy(function ($path) {
                return GitProcess::create($path)->root();
            });
    }

    /**
     * Merge status string with calculated file count stats.
     *
     * @param string $status
     * @return array
     */
    protected function statusWithFileCounts($status)
    {
        $lines = collect(explode("\n", $status))->filter();

        $totalCount = $lines->count();

        $addedCount = $lines->filter(function ($line) {
            return Str::startsWith($line, ['A ', ' A', '??']);
        })->count();

        $modifiedCount = $lines->filter(function ($line) {
            return Str::startsWith($line, ['M ', ' M']);
        })->count();

        $deletedCount = $lines->filter(function ($line) {
            return Str::startsWith($line, ['D ', ' D']);
        })->count();

        return compact('status', 'totalCount', 'addedCount', 'modifiedCount', 'deletedCount');
    }

    /**
     * Ensure absolute path.
     *
     * @param string $path
     * @return string
     */
    protected function ensureAbsolutePath($path)
    {
        $absolute = Path::isAbsolute(Path::tidy($path))
            ? $path
            : base_path($path);

        return Path::resolve($absolute);
    }

    /**
     * Run configured commands.
     *
     * @param mixed $gitRoot
     * @param mixed $paths
     * @param mixed $message
     */
    protected function runConfiguredCommands($gitRoot, $paths, $message)
    {
        $this->getParsedCommands($paths, $message)->each(function ($command) use ($gitRoot) {
            GitProcess::create($gitRoot)->run($command);
        });

        if (config('apollocms.git.push')) {
            $this->push($gitRoot);
        }
    }

    /**
     * Get parsed commands.
     *
     * @param mixed $paths
     * @param mixed $message
     */
    protected function getParsedCommands($paths, $message)
    {
        $context = $this->getCommandContext($paths, $message);

        return collect(config('apollocms.git.commands'))->map(function ($command) use ($context) {
            return Lunar::parse($command, $context);
        });
    }

    /**
     * Get command context.
     *
     * @param array $paths
     * @param string $message
     * @return array
     */
    protected function getCommandContext($paths, $message)
    {
        return [
            'paths' => collect($paths)->implode(' '),
            'message' => $message,
            'name' => $this->gitUserName(),
            'email' => $this->gitUserEmail(),
        ];
    }

    /**
     * Git push tracked content for a specific repo.
     */
    protected function push($gitRoot)
    {
        GitProcess::create($gitRoot)->push();
    }
}
