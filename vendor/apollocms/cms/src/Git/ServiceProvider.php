<?php

namespace ApolloCMS\Git;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use ApolloCMS\ApolloCMS;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot()
    {
        if (! ApolloCMS::pro()) {
            return;
        }

        if ($this->app->runningInConsole()) {
            $this->commands([
                CommitCommand::class,
            ]);
        }

        Event::subscribe(Subscriber::class);
    }
}
