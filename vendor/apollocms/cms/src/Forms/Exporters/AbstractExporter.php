<?php

namespace ApolloCMS\Forms\Exporters;

use ApolloCMS\Contracts\Forms\Exporter;

abstract class AbstractExporter implements Exporter
{
    /**
     * @var ApolloCMS\Contracts\Forms\Form
     */
    private $form;

    /**
     * Get or set the form.
     *
     * @param  ApolloCMS\Contracts\Forms\Form|null $form
     * @return ApolloCMS\Contracts\Forms\Form
     */
    public function form($form = null)
    {
        if (is_null($form)) {
            return $this->form;
        }

        $this->form = $form;
    }

    /**
     * Get the content type.
     *
     * @return string
     */
    public function contentType()
    {
        return 'text/plain';
    }
}
