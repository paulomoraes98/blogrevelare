<?php

namespace ApolloCMS\Forms;

use ApolloCMS\Data\AbstractAugmented;
use ApolloCMS\ApolloCMS;

class AugmentedForm extends AbstractAugmented
{
    public function keys()
    {
        $keys = ['handle', 'title', 'fields', 'api_url'];

        if (! ApolloCMS::isApiRoute()) {
            $keys[] = 'honeypot';
        }

        return $keys;
    }
}
