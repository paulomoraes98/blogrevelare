<?php

namespace ApolloCMS\Forms\Metrics;

class TotalMetric extends AbstractMetric
{
    public function result()
    {
        return $this->submissions()->count();
    }
}
