<?php

namespace ApolloCMS\Globals;

use ApolloCMS\Contracts\Data\Augmentable;
use ApolloCMS\Contracts\Data\Augmented;
use ApolloCMS\Contracts\Data\Localization;
use ApolloCMS\Contracts\Globals\Variables as Contract;
use ApolloCMS\Data\ContainsData;
use ApolloCMS\Data\ExistsAsFile;
use ApolloCMS\Data\HasAugmentedInstance;
use ApolloCMS\Data\HasOrigin;
use ApolloCMS\Events\GlobalVariablesBlueprintFound;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\Apoche;
use ApolloCMS\Support\Traits\FluentlyGetsAndSets;

class Variables implements Contract, Localization, Augmentable
{
    use ExistsAsFile, ContainsData, HasAugmentedInstance, HasOrigin, FluentlyGetsAndSets;

    protected $set;
    protected $locale;

    public function __construct()
    {
        $this->data = collect();
        $this->supplements = collect();
    }

    public function globalSet($set = null)
    {
        return $this->fluentlyGetOrSet('set')->args(func_get_args());
    }

    public function locale($locale = null)
    {
        return $this->fluentlyGetOrSet('locale')->args(func_get_args());
    }

    public function id()
    {
        return $this->globalSet()->id();
    }

    public function handle()
    {
        return $this->globalSet()->handle();
    }

    public function title()
    {
        return $this->globalSet()->title();
    }

    public function path()
    {
        return vsprintf('%s/%s%s.%s', [
            rtrim(Apoche::store('globals')->directory(), '/'),
            Site::hasMultiple() ? $this->locale().'/' : '',
            $this->handle(),
            'yaml',
        ]);
    }

    public function editUrl()
    {
        return $this->cpUrl('globals.variables.edit');
    }

    public function updateUrl()
    {
        return $this->cpUrl('globals.variables.update');
    }

    protected function cpUrl($route)
    {
        $params = [$this->handle()];

        if (Site::hasMultiple()) {
            $params['site'] = $this->locale();
        }

        return cp_route($route, $params);
    }

    public function save()
    {
        $this
            ->globalSet()
            ->addLocalization($this)
            ->save();

        return $this;
    }

    public function site()
    {
        return Site::get($this->locale());
    }

    public function sites()
    {
        return $this->globalSet()->sites();
    }

    public function blueprint()
    {
        $blueprint = $this->globalSet()->blueprint() ?? $this->fallbackBlueprint();

        GlobalVariablesBlueprintFound::dispatch($blueprint, $this);

        return $blueprint;
    }

    protected function fallbackBlueprint()
    {
        $fields = collect($this->values())
            ->except(['id', 'title', 'blueprint'])
            ->map(function ($field, $handle) {
                return [
                    'handle' => $handle,
                    'field' => ['type' => 'text'],
                ];
            });

        return (new \ApolloCMS\Fields\Blueprint)->setContents([
            'sections' => [
                'main' => [
                    'fields' => array_values($fields->all()),
                ],
            ],
        ]);
    }

    public function fileData()
    {
        return array_merge([
            'origin' => $this->hasOrigin() ? $this->origin()->locale() : null,
        ], $this->data()->all());
    }

    public function reference()
    {
        return "globals::{$this->id()}";
    }

    protected function getOriginByString($origin)
    {
        return $this->globalSet()->in($origin);
    }

    public function newAugmentedInstance(): Augmented
    {
        return new AugmentedVariables($this);
    }
}
