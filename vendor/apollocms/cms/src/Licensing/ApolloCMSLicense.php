<?php

namespace ApolloCMS\Licensing;

use ApolloCMS\ApolloCMS;

class ApolloCMSLicense extends License
{
    public function pro()
    {
        return ApolloCMS::pro();
    }

    public function version()
    {
        return ApolloCMS::version();
    }
}
