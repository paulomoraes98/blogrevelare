<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;

class CollectionRoutes extends Fieldtype
{
    protected $selectable = false;
}
