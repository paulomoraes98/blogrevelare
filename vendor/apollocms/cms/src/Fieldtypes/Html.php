<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;

class Html extends Fieldtype
{
    protected static $title = 'HTML';
    protected $icon = 'code';

    protected $configFields = [
        'html' => [
            'display' => 'HTML',
            'type' => 'code',
            'mode' => 'htmlmixed',
        ],
    ];
}
