<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Fields\Fieldtype;

class Yaml extends Fieldtype
{
    // Turn the YAML back into a string
    public function preProcess($data)
    {
        if (is_array($data)) {
            return count($data) > 0 ? \ApolloCMS\Facades\Yaml::dump($data) : '';
        }

        return $data;
    }

    public function process($data)
    {
        if (substr_count($data, "\n") > 0 || substr_count($data, ': ') > 0) {
            $data = \ApolloCMS\Facades\Yaml::parse($data);
        }

        if (empty($data)) {
            $data = null;
        }

        return $data;
    }

    public function toGqlType()
    {
        return [
            'type' => GraphQL::string(),
            'resolve' => function ($entry, $args, $context, $info) {
                if ($value = $entry->resolveRawGqlValue($info->fieldName)) {
                    return \ApolloCMS\Facades\YAML::dump($value);
                }
            },
        ];
    }
}
