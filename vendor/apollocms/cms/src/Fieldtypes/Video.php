<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;

class Video extends Fieldtype
{
    protected $categories = ['media'];
}
