<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Fields\Fieldtype;

class Toggle extends Fieldtype
{
    protected $defaultValue = false;

    public function preProcess($data)
    {
        return (bool) $data;
    }

    public function process($data)
    {
        return (bool) $data;
    }

    public function augment($data)
    {
        return (bool) $data;
    }

    public function toGqlType()
    {
        return GraphQL::boolean();
    }
}
