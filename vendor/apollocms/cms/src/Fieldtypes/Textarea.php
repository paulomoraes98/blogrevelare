<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;
use ApolloCMS\Query\Scopes\Filters\Fields\Textarea as TextareaFilter;

class Textarea extends Fieldtype
{
    protected function configFieldItems(): array
    {
        return [
            'placeholder' => [
                'display' => __('Placeholder'),
                'instructions' => __('apollocms::fieldtypes.text.config.placeholder'),
                'type' => 'text',
                'width' => 50,
            ],
            'character_limit' => [
                'display' => __('Character Limit'),
                'instructions' => __('apollocms::fieldtypes.text.config.character_limit'),
                'type' => 'text',
            ],
            'lunar' => [
                'display' => 'Lunar',
                'instructions' => __('apollocms::fieldtypes.any.config.lunar'),
                'type' => 'toggle',
                'width' => 50,
            ],
        ];
    }

    public function filter()
    {
        return new TextareaFilter($this);
    }
}
