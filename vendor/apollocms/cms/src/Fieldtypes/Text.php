<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;

class Text extends Fieldtype
{
    protected function configFieldItems(): array
    {
        return [
            'placeholder' => [
                'display' => __('Placeholder'),
                'instructions' => __('apollocms::fieldtypes.text.config.placeholder'),
                'type' => 'text',
                'width' => 50,
            ],
            'input_type' => [
                'display' => __('Input Type'),
                'instructions' => __('apollocms::fieldtypes.text.config.input_type'),
                'type' => 'select',
                'default' => 'text',
                'width' => 50,
                'options' => [
                    'color',
                    'date',
                    'email',
                    'hidden',
                    'month',
                    'number',
                    'password',
                    'tel',
                    'text',
                    'time',
                    'url',
                    'week',
                ],
            ],
            'character_limit' => [
                'display' => __('Character Limit'),
                'instructions' => __('apollocms::fieldtypes.text.config.character_limit'),
                'type' => 'integer',
                'width' => 50,
            ],
            'prepend' => [
                'display' => __('Prepend'),
                'instructions' => __('apollocms::fieldtypes.text.config.prepend'),
                'type' => 'text',
                'width' => 50,
            ],
            'append' => [
                'display' => __('Append'),
                'instructions' => __('apollocms::fieldtypes.text.config.append'),
                'type' => 'text',
                'width' => 50,
            ],
            'lunar' => [
                'display' => 'Lunar',
                'instructions' => __('apollocms::fieldtypes.any.config.lunar'),
                'type' => 'toggle',
                'width' => 50,
            ],
        ];
    }

    public function process($data)
    {
        if ($this->config('input_type') === 'number') {
            return (int) $data;
        }

        return $data;
    }

    public function preProcessIndex($value)
    {
        if ($value) {
            return $this->config('prepend').$value.$this->config('append');
        }
    }
}
