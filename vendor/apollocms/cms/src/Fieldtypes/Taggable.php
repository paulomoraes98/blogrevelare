<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Fields\Fieldtype;

class Taggable extends Fieldtype
{
    protected $component = 'tags';
    protected $icon = 'tags';

    protected function configFieldItems(): array
    {
        return [
            'placeholder' => [
                'display' => __('Placeholder'),
                'instructions' => __('apollocms::fieldtypes.select.config.placeholder'),
                'type' => 'text',
                'default' => '',
                'width' => 50,
            ],
        ];
    }

    public function preProcess($data)
    {
        return ($data) ? $data : [];
    }

    public function toGqlType()
    {
        return GraphQL::listOf(GraphQL::string());
    }
}
