<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Fields\Fieldtype;

class Section extends Fieldtype
{
    protected $localizable = false;
    protected $validatable = false;
    protected $defaultable = false;
}
