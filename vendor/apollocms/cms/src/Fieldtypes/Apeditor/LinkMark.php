<?php

namespace ApolloCMS\Fieldtypes\Apeditor;

use ProseMirrorToHtml\Marks\Link;
use ApolloCMS\Facades\Data;
use ApolloCMS\Support\Str;

class LinkMark extends Link
{
    public function tag()
    {
        $tag = parent::tag();

        $tag[0]['attrs']['href'] = $this->convertHref($tag[0]['attrs']['href']);

        return $tag;
    }

    private function convertHref($href)
    {
        if (! Str::startsWith($href, 'apollocms://')) {
            return $href;
        }

        $ref = Str::after($href, 'apollocms://');

        if (! $item = Data::find($ref)) {
            return '';
        }

        return $item->url();
    }
}
