<?php

namespace ApolloCMS\Fieldtypes\Apeditor;

use ApolloCMS\Fields\Fieldtype;

class Buttons extends Fieldtype
{
    public static $handle = 'apeditor_buttons_setting';
    protected $selectable = false;
}
