<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Fields\Fieldtype;
use ApolloCMS\Fields\LabeledValue;
use ApolloCMS\GraphQL\Types\LabeledValueType;

class ButtonGroup extends Fieldtype
{
    protected function configFieldItems(): array
    {
        return [
            'options' => [
                'display' => __('Options'),
                'instructions' => __('apollocms::fieldtypes.radio.config.options'),
                'type' => 'array',
                'value_header' => __('Label'),
            ],
        ];
    }

    public function augment($value)
    {
        $label = is_null($value) ? null : array_get($this->config('options'), $value, $value);

        return new LabeledValue($value, $label);
    }

    public function toGqlType()
    {
        return [
            'type' => GraphQL::type(LabeledValueType::NAME),
            'resolve' => function ($item, $args, $context, $info) {
                $resolved = $item->resolveGqlValue($info->fieldName);

                return $resolved->value() ? $resolved : null;
            },
        ];
    }
}
