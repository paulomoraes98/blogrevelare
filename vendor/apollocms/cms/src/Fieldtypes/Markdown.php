<?php

namespace ApolloCMS\Fieldtypes;

use ApolloCMS\Facades\GraphQL;
use ApolloCMS\Fields\Fieldtype;
use ApolloCMS\Query\Scopes\Filters\Fields\Markdown as MarkdownFilter;
use ApolloCMS\Support\Html;

class Markdown extends Fieldtype
{
    protected function configFieldItems(): array
    {
        return [
            'container' => [
                'display' => __('Container'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.container'),
                'type' => 'asset_container',
                'max_items' => 1,
                'width' => 50,
            ],
            'folder' => [
                'display' => __('Folder'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.folder'),
                'type' => 'asset_folder',
                'max_items' => 1,
                'width' => 50,
            ],
            'restrict' => [
                'display' => __('Restrict'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.restrict'),
                'type' => 'toggle',
                'width' => 50,
            ],
            'automatic_line_breaks' => [
                'display' => __('Automatic Line Breaks'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.automatic_line_breaks'),
                'type' => 'toggle',
                'default' => true,
                'width' => 50,
            ],
            'automatic_links' => [
                'display' => __('Automatic Links'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.automatic_links'),
                'type' => 'toggle',
                'default' => false,
                'width' => 50,
            ],
            'escape_markup' => [
                'display' => __('Escape Markup'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.escape_markup'),
                'type' => 'toggle',
                'default' => false,
                'width' => 50,
            ],
            'smartypants' => [
                'display' => __('Smartypants'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.smartypants'),
                'type' => 'toggle',
                'default' => false,
                'width' => 50,
            ],
            'parser' => [
                'display' => __('Parser'),
                'instructions' => __('apollocms::fieldtypes.markdown.config.parser'),
                'type' => 'text',
                'width' => 50,
            ],
            'lunar' => [
                'display' => 'Lunar',
                'instructions' => __('apollocms::fieldtypes.any.config.lunar'),
                'type' => 'toggle',
                'width' => 50,
            ],
        ];
    }

    public function filter()
    {
        return new MarkdownFilter($this);
    }

    public function augment($value)
    {
        if (is_null($value)) {
            return;
        }

        $markdown = \ApolloCMS\Facades\Markdown::parser(
            $this->config('parser', 'default')
        );

        if ($this->config('automatic_line_breaks')) {
            $markdown = $markdown->withAutoLineBreaks();
        }

        if ($this->config('escape_markup')) {
            $markdown = $markdown->withMarkupEscaping();
        }

        if ($this->config('automatic_links')) {
            $markdown = $markdown->withAutoLinks();
        }

        if ($this->config('smartypants')) {
            $markdown = $markdown->withSmartPunctuation();
        }

        $html = $markdown->parse((string) $value);

        return $html;
    }

    public function preProcessIndex($value)
    {
        return $value ? Html::markdown($value) : $value;
    }

    public function toGqlType()
    {
        return [
            'type' => GraphQL::string(),
            'args' => [
                'format' => [
                    'type' => GraphQL::string(),
                    'description' => 'How the value should be formatted. Either "markdown" or "html". Defaults to "html".',
                    'defaultValue' => 'html',
                ],
            ],
            'resolve' => function ($entry, $args, $context, $info) {
                return $args['format'] == 'html'
                    ? $entry->resolveGqlValue($info->fieldName)
                    : $entry->resolveRawGqlValue($info->fieldName);
            },
        ];
    }
}
