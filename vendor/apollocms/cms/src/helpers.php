<?php

use Barryvdh\Debugbar\LaravelDebugbar;
use ApolloCMS\Facades\Path;
use ApolloCMS\ApolloCMS;

function cp_route($route, $params = [])
{
    return ApolloCMS::cpRoute($route, $params);
}

function apollocms_path($path = null)
{
    return Path::tidy(__DIR__.'/../'.$path);
}

if (! function_exists('debugbar')) {
    function debugbar()
    {
        return app()->bound(LaravelDebugbar::class) ? app(LaravelDebugbar::class) : optional();
    }
}
