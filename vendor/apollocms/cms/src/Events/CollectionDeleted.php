<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class CollectionDeleted extends Event implements ProvidesCommitMessage
{
    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function commitMessage()
    {
        return __('Collection deleted', [], config('apollocms.git.locale'));
    }
}
