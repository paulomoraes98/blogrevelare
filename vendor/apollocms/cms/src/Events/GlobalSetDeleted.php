<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class GlobalSetDeleted extends Event implements ProvidesCommitMessage
{
    public $globals;

    public function __construct($globals)
    {
        $this->globals = $globals;
    }

    public function commitMessage()
    {
        return __('Global Set deleted', [], config('apollocms.git.locale'));
    }
}
