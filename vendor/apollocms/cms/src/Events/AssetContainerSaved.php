<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class AssetContainerSaved extends Event implements ProvidesCommitMessage
{
    public $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function commitMessage()
    {
        return __('Asset container saved', [], config('apollocms.git.locale'));
    }
}
