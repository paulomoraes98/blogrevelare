<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class AssetContainerDeleted extends Event implements ProvidesCommitMessage
{
    public $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function commitMessage()
    {
        return __('Asset container deleted', [], config('apollocms.git.locale'));
    }
}
