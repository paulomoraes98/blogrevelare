<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class FormDeleted extends Event implements ProvidesCommitMessage
{
    public $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    public function commitMessage()
    {
        return __('Form deleted', [], config('apollocms.git.locale'));
    }
}
