<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class UserSaved extends Event implements ProvidesCommitMessage
{
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function commitMessage()
    {
        return __('User saved', [], config('apollocms.git.locale'));
    }
}
