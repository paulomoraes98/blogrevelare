<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class AssetFolderDeleted extends Event implements ProvidesCommitMessage
{
    public $folder;

    public function __construct($folder)
    {
        $this->folder = $folder;
    }

    public function commitMessage()
    {
        return __('Asset folder deleted', [], config('apollocms.git.locale'));
    }
}
