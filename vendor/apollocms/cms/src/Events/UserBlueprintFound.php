<?php

namespace ApolloCMS\Events;

class UserBlueprintFound extends Event
{
    public $blueprint;

    public function __construct($blueprint)
    {
        $this->blueprint = $blueprint;
    }
}
