<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class UserGroupSaved extends Event implements ProvidesCommitMessage
{
    public $group;

    public function __construct($group)
    {
        $this->group = $group;
    }

    public function commitMessage()
    {
        return __('User group saved', [], config('apollocms.git.locale'));
    }
}
