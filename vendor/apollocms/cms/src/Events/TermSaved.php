<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class TermSaved extends Event implements ProvidesCommitMessage
{
    public $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function commitMessage()
    {
        return __('Term saved', [], config('apollocms.git.locale'));
    }
}
