<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class SubmissionDeleted extends Event implements ProvidesCommitMessage
{
    public $submission;

    public function __construct($submission)
    {
        $this->submission = $submission;
    }

    public function commitMessage()
    {
        return __('Submission deleted', [], config('apollocms.git.locale'));
    }
}
