<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class TermDeleted extends Event implements ProvidesCommitMessage
{
    public $term;

    public function __construct($term)
    {
        $this->term = $term;
    }

    public function commitMessage()
    {
        return __('Term deleted', [], config('apollocms.git.locale'));
    }
}
