<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class NavTreeDeleted extends Event implements ProvidesCommitMessage
{
    public $tree;

    public function __construct($tree)
    {
        $this->tree = $tree;
    }

    public function commitMessage()
    {
        return __('Navigation tree deleted', [], config('apollocms.git.locale'));
    }
}
