<?php

namespace ApolloCMS\Events;

class CollectionCreated extends Event
{
    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }
}
