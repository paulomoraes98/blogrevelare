<?php

namespace ApolloCMS\Events;

class EntryCreated extends Event
{
    public $entry;

    public function __construct($entry)
    {
        $this->entry = $entry;
    }
}
