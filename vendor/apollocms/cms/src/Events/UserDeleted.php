<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class UserDeleted extends Event implements ProvidesCommitMessage
{
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function commitMessage()
    {
        return __('User deleted', [], config('apollocms.git.locale'));
    }
}
