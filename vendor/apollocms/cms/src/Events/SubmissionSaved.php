<?php

namespace ApolloCMS\Events;

use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class SubmissionSaved extends Event implements ProvidesCommitMessage
{
    public $submission;

    public function __construct($submission)
    {
        $this->submission = $submission;
    }

    public function commitMessage()
    {
        return __('Submission saved', [], config('apollocms.git.locale'));
    }
}
