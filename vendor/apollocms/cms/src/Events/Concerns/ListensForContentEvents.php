<?php

namespace ApolloCMS\Events\Concerns;

trait ListensForContentEvents
{
    /**
     * Content changed events.
     *
     * @var array
     */
    protected $events = [
        \ApolloCMS\Events\AssetContainerDeleted::class,
        \ApolloCMS\Events\AssetContainerSaved::class,
        \ApolloCMS\Events\AssetDeleted::class,
        \ApolloCMS\Events\AssetFolderDeleted::class,
        \ApolloCMS\Events\AssetFolderSaved::class,
        \ApolloCMS\Events\AssetSaved::class,
        \ApolloCMS\Events\BlueprintDeleted::class,
        \ApolloCMS\Events\BlueprintSaved::class,
        \ApolloCMS\Events\CollectionDeleted::class,
        \ApolloCMS\Events\CollectionSaved::class,
        \ApolloCMS\Events\CollectionTreeSaved::class,
        \ApolloCMS\Events\CollectionTreeDeleted::class,
        \ApolloCMS\Events\EntryDeleted::class,
        \ApolloCMS\Events\EntrySaved::class,
        \ApolloCMS\Events\FieldsetDeleted::class,
        \ApolloCMS\Events\FieldsetSaved::class,
        \ApolloCMS\Events\FormDeleted::class,
        \ApolloCMS\Events\FormSaved::class,
        \ApolloCMS\Events\GlobalSetDeleted::class,
        \ApolloCMS\Events\GlobalSetSaved::class,
        \ApolloCMS\Events\NavDeleted::class,
        \ApolloCMS\Events\NavSaved::class,
        \ApolloCMS\Events\NavTreeSaved::class,
        \ApolloCMS\Events\NavTreeDeleted::class,
        \ApolloCMS\Events\RoleDeleted::class,
        \ApolloCMS\Events\RoleSaved::class,
        \ApolloCMS\Events\SubmissionDeleted::class,
        \ApolloCMS\Events\SubmissionSaved::class,
        \ApolloCMS\Events\TaxonomyDeleted::class,
        \ApolloCMS\Events\TaxonomySaved::class,
        \ApolloCMS\Events\TermDeleted::class,
        \ApolloCMS\Events\TermSaved::class,
        \ApolloCMS\Events\UserDeleted::class,
        \ApolloCMS\Events\UserGroupDeleted::class,
        \ApolloCMS\Events\UserGroupSaved::class,
        \ApolloCMS\Events\UserSaved::class,
    ];
}
