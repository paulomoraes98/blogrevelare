<?php

namespace ApolloCMS\Events;

use Illuminate\Foundation\Events\Dispatchable;

abstract class Event
{
    use Dispatchable;
}
