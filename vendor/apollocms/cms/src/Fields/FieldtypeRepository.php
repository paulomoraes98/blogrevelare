<?php

namespace ApolloCMS\Fields;

class FieldtypeRepository
{
    public function preloadable()
    {
        return $this->classes()->filter(function ($class) {
            return $class::preloadable();
        });
    }

    public function find($handle)
    {
        if (! ($fieldtypes = $this->classes())->has($handle)) {
            throw new \ApolloCMS\Exceptions\FieldtypeNotFoundException($handle);
        }

        return app($fieldtypes->get($handle));
    }

    public function classes()
    {
        return app('apollocms.fieldtypes');
    }

    public function handles()
    {
        return $this->classes()->map(function ($class) {
            return $class::handle();
        });
    }
}
