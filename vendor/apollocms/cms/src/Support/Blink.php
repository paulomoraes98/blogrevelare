<?php

namespace ApolloCMS\Support;

use ApolloCMS\Blink\Blink as ApolloCMSBlink;

class Blink
{
    protected $stores = [];

    public function store($name = 'default')
    {
        return $this->stores[$name] = $this->stores[$name] ?? new ApolloCMSBlink;
    }

    public function __call($method, $args)
    {
        return $this->store()->$method(...$args);
    }
}
