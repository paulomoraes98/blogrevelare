<?php

namespace ApolloCMS\Support\Traits;

use ApolloCMS\Support\FluentGetterSetter;

trait FluentlyGetsAndSets
{
    /**
     * Fluently get or set property using the FluentGetterSetter helper class.
     *
     * @param string $property
     * @return FluentGetterSetter
     */
    public function fluentlyGetOrSet($property)
    {
        return new FluentGetterSetter($this, $property);
    }
}
