<?php

namespace ApolloCMS\Imaging;

use League\Glide\ServerFactory;
use ApolloCMS\Facades\Config;
use ApolloCMS\Facades\Image;
use ApolloCMS\Imaging\ResponseFactory as LaravelResponseFactory;

class GlideServer
{
    /**
     * Create glide server.
     *
     * @return \League\Glide\Server
     */
    public function create()
    {
        return ServerFactory::create([
            'source'   => base_path(), // this gets overriden on the fly by the image generator
            'cache'    => $this->cachePath(),
            'response' => new LaravelResponseFactory(app('request')),
            'driver'   => Config::get('apollocms.assets.image_manipulation.driver'),
            'cache_with_file_extensions' => true,
            'presets' => Image::manipulationPresets(),
        ]);
    }

    /**
     * Get glide cache path.
     *
     * @return string
     */
    public function cachePath()
    {
        return Config::get('apollocms.assets.image_manipulation.cache')
            ? Config::get('apollocms.assets.image_manipulation.cache_path')
            : storage_path('apollocms/glide');
    }
}
