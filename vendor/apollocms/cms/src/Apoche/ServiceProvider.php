<?php

namespace ApolloCMS\Apoche;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use ApolloCMS\Assets\QueryBuilder as AssetQueryBuilder;
use ApolloCMS\Facades\File;
use ApolloCMS\Facades\Site;
use ApolloCMS\Apoche\Query\EntryQueryBuilder;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->app->singleton(Apoche::class, function () {
            return (new Apoche)->setLockFactory($this->locks());
        });

        $this->app->alias(Apoche::class, 'apoche');

        $this->app->singleton('apoche.indexes', function () {
            return collect();
        });

        $this->app->bind(EntryQueryBuilder::class, function () {
            return new EntryQueryBuilder($this->app->make(Apoche::class)->store('entries'));
        });

        $this->app->bind(AssetQueryBuilder::class, function () {
            return new AssetQueryBuilder($this->app->make(Apoche::class)->store('assets'));
        });
    }

    public function boot()
    {
        $apoche = $this->app->make(Apoche::class);

        $apoche->sites(Site::all()->keys()->all());

        $this->registerStores($apoche);
    }

    private function registerStores($apoche)
    {
        // Merge the stores from our config file with the stores in the user's published
        // config file. If we ever need to add more stores, they won't need to add them.
        $config = require __DIR__.'/../../config/apoche.php';
        $published = config('apollocms.apoche.stores');

        $nativeStores = collect($config['stores'])
            ->map(function ($config, $key) use ($published) {
                return array_merge($config, $published[$key] ?? []);
            });

        // Merge in any user defined stores that aren't native.
        $stores = $nativeStores->merge(collect($published)->diffKeys($nativeStores));

        $stores = $stores->map(function ($config) {
            return app($config['class'])->directory($config['directory'] ?? null);
        });

        $apoche->registerStores($stores->all());
    }

    private function locks()
    {
        if (config('apollocms.apoche.lock.enabled', true)) {
            $store = $this->createFileLockStore();
        } else {
            $store = new NullLockStore;
        }

        return new LockFactory($store);
    }

    private function createFileLockStore()
    {
        File::makeDirectory($dir = storage_path('apollocms/apoche-locks'));

        return new FlockStore($dir);
    }
}
