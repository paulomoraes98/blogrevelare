<?php

namespace ApolloCMS\Apoche\Repositories;

use Illuminate\Support\Collection;
use ApolloCMS\Contracts\Structures\Nav;
use ApolloCMS\Contracts\Structures\NavigationRepository as RepositoryContract;
use ApolloCMS\Apoche\Apoche;

class NavigationRepository implements RepositoryContract
{
    protected $apoche;
    protected $store;

    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('navigation');
    }

    public function all(): Collection
    {
        $keys = $this->store->paths()->keys();

        return $this->store->getItems($keys);
    }

    public function find($id): ?Nav
    {
        return $this->findByHandle($id);
    }

    public function findByHandle($handle): ?Nav
    {
        return $this->store->getItem($handle);
    }

    public function save(Nav $nav)
    {
        $this->store->save($nav);
    }

    public function delete(Nav $nav)
    {
        $this->store->delete($nav);
    }

    public function make(string $handle = null): Nav
    {
        return app(Nav::class)->handle($handle);
    }

    public function updateEntryUris(Nav $nav)
    {
        $this->store->index('uri')->updateItem($nav);
    }

    public static function bindings(): array
    {
        return [
            Nav::class => \ApolloCMS\Structures\Nav::class,
        ];
    }
}
