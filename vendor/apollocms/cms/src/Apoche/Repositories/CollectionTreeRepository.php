<?php

namespace ApolloCMS\Apoche\Repositories;

use ApolloCMS\Apoche\Apoche;

class CollectionTreeRepository extends NavTreeRepository
{
    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('collection-trees');
    }
}
