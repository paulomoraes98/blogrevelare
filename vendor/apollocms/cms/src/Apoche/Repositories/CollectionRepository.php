<?php

namespace ApolloCMS\Apoche\Repositories;

use Illuminate\Support\Collection as IlluminateCollection;
use ApolloCMS\Contracts\Entries\Collection;
use ApolloCMS\Contracts\Entries\CollectionRepository as RepositoryContract;
use ApolloCMS\Facades\Blink;
use ApolloCMS\Apoche\Apoche;

class CollectionRepository implements RepositoryContract
{
    protected $apoche;
    protected $store;

    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('collections');
    }

    public function all(): IlluminateCollection
    {
        $keys = $this->store->paths()->keys();

        return $this->store->getItems($keys);
    }

    public function find($id): ?Collection
    {
        return $this->findByHandle($id);
    }

    public function findByHandle($handle): ?Collection
    {
        return $this->store->getItem($handle);
    }

    public function findByMount($mount): ?Collection
    {
        if (! $mount->reference()) {
            return null;
        }

        return $this->all()->first(function ($collection) use ($mount) {
            return optional($collection->mount())->id() === $mount->id();
        });
    }

    public function make(string $handle = null): Collection
    {
        return app(Collection::class)->handle($handle);
    }

    public function handles(): IlluminateCollection
    {
        return Blink::once('collection-handles', function () {
            return $this->all()->map->handle();
        });
    }

    public function handleExists(string $handle): bool
    {
        return $this->handles()->contains($handle);
    }

    public function save(Collection $collection)
    {
        $this->store->save($collection);

        if ($collection->orderable()) {
            $this->apoche->store('entries')->store($collection->handle())->index('order')->update();
        }
    }

    public function delete(Collection $collection)
    {
        $this->store->delete($collection);
    }

    public function updateEntryUris(Collection $collection, $ids = null)
    {
        $this->store->updateEntryUris($collection, $ids);
    }

    public function updateEntryOrder(Collection $collection, $ids = null)
    {
        $this->store->updateEntryOrder($collection, $ids);
    }

    public function whereStructured(): IlluminateCollection
    {
        return $this->all()->filter->hasStructure();
    }

    public static function bindings(): array
    {
        return [
            Collection::class => \ApolloCMS\Entries\Collection::class,
        ];
    }
}
