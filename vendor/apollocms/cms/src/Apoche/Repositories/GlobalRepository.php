<?php

namespace ApolloCMS\Apoche\Repositories;

use ApolloCMS\Contracts\Globals\GlobalRepository as RepositoryContract;
use ApolloCMS\Contracts\Globals\GlobalSet;
use ApolloCMS\Globals\GlobalCollection;
use ApolloCMS\Apoche\Apoche;

class GlobalRepository implements RepositoryContract
{
    protected $apoche;
    protected $store;

    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('globals');
    }

    public function make($handle = null)
    {
        return app(GlobalSet::class)->handle($handle);
    }

    public function all(): GlobalCollection
    {
        $keys = $this->store->paths()->keys();

        return GlobalCollection::make($this->store->getItems($keys));
    }

    public function find($id): ?GlobalSet
    {
        return $this->store->getItem($id);
    }

    public function findByHandle($handle): ?GlobalSet
    {
        $key = $this->store->index('handle')->items()->flip()->get($handle);

        return $this->find($key);
    }

    public function save($global)
    {
        $this->store->save($global);
    }

    public function delete($global)
    {
        $this->store->delete($global);
    }

    public static function bindings(): array
    {
        return [
            GlobalSet::class => \ApolloCMS\Globals\GlobalSet::class,
        ];
    }
}
