<?php

namespace ApolloCMS\Apoche\Repositories;

use ApolloCMS\Contracts\Structures\NavTreeRepository as Contract;
use ApolloCMS\Contracts\Structures\Tree;
use ApolloCMS\Apoche\Apoche;

class NavTreeRepository implements Contract
{
    protected $apoche;
    protected $store;

    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('nav-trees');
    }

    public function find(string $handle, string $site): ?Tree
    {
        return $this->store->getItem("$handle::$site");
    }

    public function save(Tree $tree)
    {
        $this->store->save($tree);

        return true;
    }

    public function delete(Tree $tree)
    {
        $this->store->delete($tree);

        return true;
    }

    public static function bindings()
    {
        return [];
    }
}
