<?php

namespace ApolloCMS\Apoche\Repositories;

use ApolloCMS\Auth\File\RoleRepository;
use ApolloCMS\Auth\File\User as FileUser;
use ApolloCMS\Auth\File\UserGroupRepository;
use ApolloCMS\Auth\UserCollection;
use ApolloCMS\Auth\UserRepository as BaseRepository;
use ApolloCMS\Contracts\Auth\User;
use ApolloCMS\Apoche\Query\UserQueryBuilder;
use ApolloCMS\Apoche\Apoche;

class UserRepository extends BaseRepository
{
    protected $apoche;
    protected $store;
    protected $config;
    protected $roleRepository = RoleRepository::class;
    protected $userGroupRepository = UserGroupRepository::class;

    public function __construct(Apoche $apoche, array $config = [])
    {
        $this->apoche = $apoche;
        $this->store = $apoche->store('users');
        $this->config = $config;
    }

    public function make(): User
    {
        return new FileUser;
    }

    public function all(): UserCollection
    {
        return $this->query()->get();
    }

    public function find($id): ?User
    {
        return $this->store->getItem($id);
    }

    public function findByEmail(string $email): ?User
    {
        return $this->query()->where('email', $email)->first();
    }

    public function query()
    {
        return new UserQueryBuilder($this->store);
    }

    public function save(User $user)
    {
        if (! $user->id()) {
            $user->id($this->apoche->generateId());
        }

        $this->store->save($user);
    }

    public function delete(User $user)
    {
        $this->store->delete($user);
    }

    public function fromUser($user): ?User
    {
        if ($user instanceof User) {
            return $user;
        }

        return null;
    }
}
