<?php

namespace ApolloCMS\Apoche\Indexes\Users;

use ApolloCMS\Apoche\Indexes\Value;
use ApolloCMS\Support\Str;

class Group extends Value
{
    public function getItemValue($item)
    {
        $group = Str::after($this->name(), 'groups/');

        return $item->isInGroup($group);
    }
}
