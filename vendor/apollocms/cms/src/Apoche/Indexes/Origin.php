<?php

namespace ApolloCMS\Apoche\Indexes;

class Origin extends Value
{
    public function getItemValue($item)
    {
        if (! $item->origin()) {
            return null;
        }

        return $item->origin()->id();
    }
}
