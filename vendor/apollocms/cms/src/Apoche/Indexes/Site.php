<?php

namespace ApolloCMS\Apoche\Indexes;

class Site extends Value
{
    public function getItemValue($item)
    {
        return $item->locale();
    }
}
