<?php

namespace ApolloCMS\Apoche\Stores;

/**
 * Exists only to ease migration from structures to navigation in beta 20.
 *
 * @deprecated
 **/
class StructuresStore extends NavigationStore
{
    //
}
