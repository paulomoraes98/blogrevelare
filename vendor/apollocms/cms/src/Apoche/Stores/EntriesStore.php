<?php

namespace ApolloCMS\Apoche\Stores;

use ApolloCMS\Facades\Collection;

class EntriesStore extends AggregateStore
{
    protected $childStore = CollectionEntriesStore::class;

    public function key()
    {
        return 'entries';
    }

    public function discoverStores()
    {
        return Collection::handles()->map(function ($handle) {
            return $this->store($handle);
        });
    }
}
