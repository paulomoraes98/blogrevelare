<?php

namespace ApolloCMS\Contracts\Data;

interface Localization
{
    public function locale($locale = null);
}
