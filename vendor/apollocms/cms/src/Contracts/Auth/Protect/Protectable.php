<?php

namespace ApolloCMS\Contracts\Auth\Protect;

interface Protectable
{
    public function getProtectionScheme();
}
