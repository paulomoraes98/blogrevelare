<?php

namespace ApolloCMS\Contracts\Assets;

use ApolloCMS\Contracts\Data\DataService;

interface AssetService extends DataService
{
    /**
     * Get assets from a folder.
     *
     * @param string      $folder
     * @param string|null $locale
     * @return \ApolloCMS\Assets\AssetCollection
     */
    public function getAssets($folder, $locale = null);

    /**
     * @param string|null $locale
     * @return \ApolloCMS\Contracts\Assets\AssetContainer[]
     */
    public function getContainers($locale = null);

    /**
     * @param string      $uuid
     * @param string|null $locale
     * @return \ApolloCMS\Contracts\Assets\AssetContainer
     */
    public function getContainer($uuid, $locale = null);
}
