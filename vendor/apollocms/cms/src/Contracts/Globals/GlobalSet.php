<?php

namespace ApolloCMS\Contracts\Globals;

use ApolloCMS\Contracts\Data\Localizable;

interface GlobalSet extends Localizable
{
}
