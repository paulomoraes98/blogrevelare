<?php

namespace ApolloCMS\Contracts\Imaging;

interface UrlBuilder
{
    /**
     * Build the URL.
     *
     * @param \ApolloCMS\Contracts\Assets\Asset|string $item
     * @param array                                   $params
     * @param string|null                             $filename
     * @return string
     */
    public function build($item, $params, $filename = null);
}
