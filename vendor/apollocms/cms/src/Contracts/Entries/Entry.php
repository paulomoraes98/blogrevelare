<?php

namespace ApolloCMS\Contracts\Entries;

use ApolloCMS\Contracts\Data\Localizable;

interface Entry extends Localizable
{
}
