<?php

namespace ApolloCMS\Taxonomies;

use ApolloCMS\Contracts\Taxonomies\Taxonomy as TaxonomyContract;
use ApolloCMS\Events\Apoche\RepositoryItemInserted;
use ApolloCMS\Events\Apoche\RepositoryItemRemoved;
use ApolloCMS\Facades\Taxonomy;
use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Apoches\TaxonomyApoche;

class TermTracker
{
    /**
     * @var Apoche
     */
    private $apoche;
    /**
     * @var TaxonomyApoche
     */
    private $taxonomyApoche;

    /**
     * @param Apoche $apoche
     */
    public function __construct(Apoche $apoche)
    {
        $this->apoche = $apoche;
        $this->taxonomyApoche = $apoche->taxonomies;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(RepositoryItemInserted::class, self::class.'@insert');
        $events->listen(RepositoryItemRemoved::class, self::class.'@remove');
    }

    /**
     * @param RepositoryItemInserted $event
     */
    public function insert(RepositoryItemInserted $event)
    {
        if ($event->item instanceof TaxonomyContract) {
            $this->updateLocalizedTermUris($event->item);

            return;
        }

        if (! method_exists($event->item, 'isTaxonomizable') || ! $event->item->isTaxonomizable()) {
            return;
        }

        Taxonomy::all()->each(function ($taxonomy, $handle) use ($event) {
            if ($event->item->has($handle)) {
                try {
                    $this->addTerms($handle, $event->item);
                } catch (\Exception $e) {
                    \Log::debug('There was a problem adding taxonomy terms to data with ID '.$event->item->id());
                    \Log::debug($e->getMessage().PHP_EOL.$e->getTraceAsString());
                }
            }
        });
    }

    private function addTerms($taxonomy, $item)
    {
        // Don't do anything if there aren't any terms.
        if (! $values = $item->get($taxonomy)) {
            return;
        }

        $this->taxonomyApoche->syncAssociations(
            $item->id(),
            $taxonomy,
            (array) $item->get($taxonomy)
        );
    }

    private function updateLocalizedTermUris($taxonomy)
    {
        $terms = [];

        $this->taxonomyApoche->clearLocalizedUris();

        // Collect all the terms that have had their slugs localized.
        foreach ($taxonomy->get('slugs', []) as $locale => $slugs) {
            foreach ((array) $slugs as $slug => $localizedSlug) {
                $terms[] = $slug;
            }
        }

        // Now that we have the terms (not concerned with the locales
        // now) we can let the Apoche sort out the respective URIs.
        foreach (collect($terms)->unique() as $term) {
            $this->taxonomyApoche->addUris($taxonomy->basename(), $term);
        }
    }

    /**
     * @param RepositoryItemRemoved $event
     */
    public function remove(RepositoryItemRemoved $event)
    {
        $this->taxonomyApoche->removeData($event->id);
    }
}
