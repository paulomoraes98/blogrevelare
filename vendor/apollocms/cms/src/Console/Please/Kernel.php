<?php

namespace ApolloCMS\Console\Please;

use App\Console\Kernel as ConsoleKernel;
use ApolloCMS\Console\Please\Application as Please;
use ApolloCMS\ApolloCMS;

class Kernel extends ConsoleKernel
{
    /**
     * Get the Artisan application instance.
     *
     * @return \Illuminate\Console\Application
     */
    protected function getArtisan()
    {
        return tap(
            new Please($this->app, $this->events, ApolloCMS::version())
        )->setName('ApolloCMS');
    }
}
