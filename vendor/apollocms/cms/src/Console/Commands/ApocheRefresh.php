<?php

namespace ApolloCMS\Console\Commands;

use Illuminate\Console\Command;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Apoche;
use Wilderborn\Partyline\Facade as Partyline;

class ApocheRefresh extends Command
{
    use RunsInPlease;

    protected $signature = 'apollocms:apoche:refresh';
    protected $description = 'Clear and rebuild the "Apoche" cache';

    public function handle()
    {
        Partyline::bind($this);

        $this->line('Please wait. This may take a while if you have a lot of content.');

        Apoche::refresh();

        $this->info('You have trimmed and polished the Apoche. It is handsome, warm, and ready.');
    }
}
