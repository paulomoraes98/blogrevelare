<?php

namespace ApolloCMS\Console\Commands;

use Illuminate\Console\Command;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Apoche;

class ApocheClear extends Command
{
    use RunsInPlease;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apollocms:apoche:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the "Apoche" cache';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        Apoche::clear();

        $this->info('You have trimmed the Apoche. It looks dashing.');
    }
}
