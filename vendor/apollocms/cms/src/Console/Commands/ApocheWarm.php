<?php

namespace ApolloCMS\Console\Commands;

use Illuminate\Console\Command;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Apoche;
use Wilderborn\Partyline\Facade as Partyline;

class ApocheWarm extends Command
{
    use RunsInPlease;

    protected $signature = 'apollocms:apoche:warm';
    protected $description = 'Build the "Apoche" cache';

    public function handle()
    {
        Partyline::bind($this);

        $this->line('Please wait. This may take a while if you have a lot of content.');

        Apoche::warm();

        $this->info('You have poured oil over the Apoche and polished it until it shines. It is warm and ready.');
    }
}
