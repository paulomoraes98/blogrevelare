<?php

namespace ApolloCMS\Console\Commands;

use Illuminate\Console\Command;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Apoche;
use ApolloCMS\Apoche\Stores\AggregateStore;

class ApocheDoctor extends Command
{
    use RunsInPlease;

    protected $signature = 'apollocms:apoche:doctor';
    protected $description = 'Diagnose any problems with the Apoche.';

    public function handle()
    {
        $missing = Apoche::stores()->flatMap(function ($store) {
            return $store instanceof AggregateStore ? $store->discoverStores() : [$store];
        })->mapWithKeys(function ($store) {
            return [$store->key() => $this->getUnconfiguredIndexes($store)];
        })->filter(function ($item) {
            return ! $item->isEmpty();
        });

        if ($missing->isEmpty()) {
            $this->info('[✓] No unconfigured indexes.');
            $this->line('Indexes are created on demand through regular site usage.');
            $this->line('You could consider trying again after browsing your site.');
        }

        $missing->each(function ($item, $key) {
            $this->line("<fg=red>[✗]</> Unconfigured indexes in <comment>{$key}</comment>");
            $item->each(function ($item) {
                $this->line('- '.$item);
            });
            $this->line('');
        });
    }

    protected function getUnconfiguredIndexes($store)
    {
        $allIndexes = $store->resolveIndexes(true);
        $configuredIndexes = $store->resolveIndexes(false);

        return $allIndexes->reject(function ($index) use ($configuredIndexes) {
            return $configuredIndexes->has($index->name());
        })->map->name()->values();
    }
}
