<?php

namespace ApolloCMS\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Application;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\Addon;
use ApolloCMS\ApolloCMS;

class SupportDetails extends Command
{
    use RunsInPlease;

    protected $signature = 'apollocms:support:details';
    protected $description = 'Outputs details helpful for support requests.';

    public function handle()
    {
        $this->line(sprintf('<info>ApolloCMS</info> %s %s', ApolloCMS::version(), ApolloCMS::pro() ? 'Pro' : 'Solo'));
        $this->line('<info>Laravel</info> '.Application::VERSION);
        $this->line('<info>PHP</info> '.phpversion());
        $this->addons();
    }

    private function addons()
    {
        $addons = Addon::all();

        if ($addons->isEmpty()) {
            return $this->line('No addons installed');
        }

        foreach ($addons as $addon) {
            $this->line(sprintf('<info>%s</info> %s', $addon->package(), $addon->version()));
        }
    }
}
