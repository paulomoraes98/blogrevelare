<?php

namespace ApolloCMS\Console\Commands;

use Exception;
use Facades\ApolloCMS\UpdateScripts\Manager as UpdateScriptManager;
use Illuminate\Console\Command;
use ApolloCMS\Console\Composer\Json as ComposerJson;
use ApolloCMS\Console\RunsInPlease;
use ApolloCMS\Facades\File;
use ApolloCMS\ApolloCMS;

class Install extends Command
{
    use RunsInPlease;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apollocms:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install ApolloCMS';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addons()
             ->createFiles()
             ->publish()
             ->runCallbacks()
             ->clearViews()
             ->clearCache()
             ->runUpdateScripts();
    }

    protected function addons()
    {
        $this->call('apollocms:addons:discover');

        return $this;
    }

    protected function createFiles()
    {
        $gitkeeps = [
            config('apollocms.apoche.stores.asset-containers.directory'),
            config('apollocms.apoche.stores.collections.directory'),
            config('apollocms.apoche.stores.globals.directory'),
            config('apollocms.apoche.stores.taxonomies.directory'),
            config('apollocms.apoche.stores.navigation.directory'),
            config('apollocms.users.repositories.file.paths.users'),
        ];

        $gitignores = [
            storage_path('apollocms'),
        ];

        foreach (array_filter($gitkeeps) as $dir) {
            if (! File::exists($gitkeep = $dir.'/.gitkeep')) {
                File::put($gitkeep, '');
                $this->info("Created the <comment>[$dir]</comment> directory.");
            }
        }

        foreach (array_filter($gitignores) as $dir) {
            if (! File::exists($gitignore = $dir.'/.gitignore')) {
                File::put($gitignore, "*\n!.gitignore");
                $this->info("Created the <comment>[$dir]</comment> directory.");
            }
        }

        return $this;
    }

    protected function publish()
    {
        $this->call('vendor:publish', ['--tag' => 'apollocms']);
        $this->call('vendor:publish', ['--tag' => 'apollocms-cp', '--force' => true]);

        return $this;
    }

    protected function clearViews()
    {
        $this->call('view:clear');

        return $this;
    }

    protected function clearCache()
    {
        $this->call('cache:clear');

        return $this;
    }

    protected function runCallbacks()
    {
        ApolloCMS::runAfterInstalledCallbacks($this);

        return $this;
    }

    protected function runUpdateScripts()
    {
        if (ComposerJson::isMissingPreUpdateCmd()) {
            return $this->addPreUpdateCmdAndRunFirstTime();
        }

        UpdateScriptManager::runAll($this);

        return $this;
    }

    protected function addPreUpdateCmdAndRunFirstTime()
    {
        try {
            ComposerJson::addPreUpdateCmd();
        } catch (Exception $exception) {
            return $this->outputMissingPreUpdateCmd();
        }

        UpdateScriptManager::runUpdatesForSpecificPackageVersion(ApolloCMS::PACKAGE, '3.0.0', $this);

        return $this;
    }

    protected function outputMissingPreUpdateCmd()
    {
        $this->error('We notice you are missing a composer hook!');
        $this->error('Please ensure the following is registered in the `scripts` section of your composer.json file,');
        $this->error('And re-run [php please updates:run 3.0] when complete.');

        $this->line(<<<'EOT'
"scripts": {
    "pre-update-cmd": [
        "ApolloCMS\\Console\\Composer\\Scripts::preUpdateCmd"
    ],
    ...
}
EOT
        );

        return $this;
    }
}
