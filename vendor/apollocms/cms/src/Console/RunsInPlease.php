<?php

namespace ApolloCMS\Console;

use ApolloCMS\Support\Str;

trait RunsInPlease
{
    /**
     * Is command running in please?
     *
     * @var bool
     */
    public $runningInPlease = false;

    /**
     * Set running in please.
     */
    public function setRunningInPlease()
    {
        $this->runningInPlease = true;
    }

    /**
     * Remove apollocms grouping from signature for apollocms please command.
     */
    public function removeApolloCMSGrouping()
    {
        // Commands that extend GeneratorCommand expect a `$name` property instead of `$signature`!
        if (! isset($this->signature)) {
            $this->signature = $this->name;
        }

        $this->signature = str_replace('apollocms:', '', $this->signature);

        $this->configureUsingFluentDefinition();
        $this->specifyParameters();
    }

    /**
     * If `hiddenInPlease` property is set, override hidden status when running please.
     */
    public function setHiddenInPlease()
    {
        if (isset($this->hiddenInPlease)) {
            $this->setHidden($this->hiddenInPlease);
        }
    }

    protected function resolveCommand($command)
    {
        if ($this->runningInPlease && Str::startsWith($command, 'apollocms:')) {
            $command = Str::after($command, 'apollocms:');
        }

        return parent::resolveCommand($command);
    }
}
