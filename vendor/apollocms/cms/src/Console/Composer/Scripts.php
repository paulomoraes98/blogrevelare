<?php

namespace ApolloCMS\Console\Composer;

use Composer\Script\Event;

class Scripts
{
    /**
     * Run ApolloCMS pre-update-cmd hook.
     *
     * @param Event $event
     */
    public static function preUpdateCmd(Event $event)
    {
        Lock::backup();
    }
}
