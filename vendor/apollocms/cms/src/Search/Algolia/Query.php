<?php

namespace ApolloCMS\Search\Algolia;

use ApolloCMS\Search\QueryBuilder;

class Query extends QueryBuilder
{
    public function getSearchResults($query)
    {
        return $this->index->searchUsingApi($query);
    }
}
