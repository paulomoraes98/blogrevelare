<?php

namespace ApolloCMS\Search\Comb;

use ApolloCMS\Search\QueryBuilder;

class Query extends QueryBuilder
{
    public function getSearchResults($query)
    {
        return $this->index->lookup($this->query);
    }
}
