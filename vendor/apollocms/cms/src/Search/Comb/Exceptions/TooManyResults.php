<?php

namespace ApolloCMS\Search\Comb\Exceptions;

/**
 * Thrown when there are more results than the limit.
 */
class TooManyResults extends Exception
{
}
