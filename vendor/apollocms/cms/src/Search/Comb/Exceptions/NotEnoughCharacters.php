<?php

namespace ApolloCMS\Search\Comb\Exceptions;

/**
 * Thrown when not enough characters have been entered.
 */
class NotEnoughCharacters extends Exception
{
}
