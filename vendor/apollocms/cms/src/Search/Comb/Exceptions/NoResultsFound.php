<?php

namespace ApolloCMS\Search\Comb\Exceptions;

/**
 * Thrown when no results are found.
 */
class NoResultsFound extends Exception
{
}
