<?php

namespace ApolloCMS\Search\Comb\Exceptions;

/**
 * Basic Comb exception from which others extend.
 */
class Exception extends \Exception
{
}
