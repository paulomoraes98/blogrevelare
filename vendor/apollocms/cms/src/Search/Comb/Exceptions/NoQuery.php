<?php

namespace ApolloCMS\Search\Comb\Exceptions;

/**
 * Thrown when no query has been asked.
 */
class NoQuery extends Exception
{
}
