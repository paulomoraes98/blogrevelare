<?php

namespace ApolloCMS\Providers;

use Illuminate\Support\ServiceProvider;
use ApolloCMS\Facades\Markdown;
use ApolloCMS\Markdown\Manager;
use ApolloCMS\Markdown\Parser;

class MarkdownServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Manager::class, function () {
            return new Manager(new Parser);
        });
    }

    public function boot()
    {
        Markdown::extend('default', function ($parser) {
            return $parser->withApolloCMSDefaults();
        });
    }
}
