<?php

namespace ApolloCMS\Providers;

use Facades\ApolloCMS\Auth\CorePermissions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use ApolloCMS\Auth\Passwords\PasswordBrokerManager;
use ApolloCMS\Auth\PermissionCache;
use ApolloCMS\Auth\Permissions;
use ApolloCMS\Auth\Protect\ProtectorManager;
use ApolloCMS\Auth\UserProvider;
use ApolloCMS\Auth\UserRepositoryManager;
use ApolloCMS\Contracts\Auth\RoleRepository;
use ApolloCMS\Contracts\Auth\UserGroupRepository;
use ApolloCMS\Contracts\Auth\UserRepository;
use ApolloCMS\Facades\User;
use ApolloCMS\Policies;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        \ApolloCMS\Contracts\Structures\Nav::class => Policies\NavPolicy::class,
        \ApolloCMS\Contracts\Entries\Collection::class => Policies\CollectionPolicy::class,
        \ApolloCMS\Contracts\Entries\Entry::class => Policies\EntryPolicy::class,
        \ApolloCMS\Contracts\Taxonomies\Taxonomy::class => Policies\TaxonomyPolicy::class,
        \ApolloCMS\Contracts\Taxonomies\Term::class => Policies\TermPolicy::class,
        \ApolloCMS\Contracts\Globals\GlobalSet::class => Policies\GlobalSetPolicy::class,
        \ApolloCMS\Contracts\Globals\Variables::class => Policies\GlobalSetVariablesPolicy::class,
        \ApolloCMS\Contracts\Auth\User::class => Policies\UserPolicy::class,
        \ApolloCMS\Contracts\Forms\Form::class => Policies\FormPolicy::class,
        \ApolloCMS\Contracts\Forms\Submission::class => Policies\FormSubmissionPolicy::class,
        \ApolloCMS\Contracts\Assets\Asset::class => Policies\AssetPolicy::class,
        \ApolloCMS\Contracts\Assets\AssetFolder::class => Policies\AssetFolderPolicy::class,
        \ApolloCMS\Contracts\Assets\AssetContainer::class => Policies\AssetContainerPolicy::class,
    ];

    public function register()
    {
        $this->app->singleton(UserRepositoryManager::class, function ($app) {
            return new UserRepositoryManager($app);
        });

        $this->app->singleton(UserRepository::class, function ($app) {
            return $app[UserRepositoryManager::class]->repository();
        });

        $this->app->singleton(RoleRepository::class, function ($app) {
            return $app[UserRepository::class]->roleRepository();
        });

        $this->app->singleton(UserGroupRepository::class, function ($app) {
            return $app[UserRepository::class]->userGroupRepository();
        });

        $this->app->singleton(ProtectorManager::class, function ($app) {
            return new ProtectorManager($app);
        });

        $this->app->singleton(Permissions::class, function () {
            return new Permissions;
        });

        $this->app->singleton(PermissionCache::class, function ($app) {
            return new PermissionCache;
        });
    }

    public function boot()
    {
        Auth::provider('apollocms', function () {
            return new UserProvider;
        });

        Gate::before(function ($user, $ability) {
            return optional(User::fromUser($user))->isSuper() ? true : null;
        });

        Gate::after(function ($user, $ability) {
            return optional(User::fromUser($user))->hasPermission($ability) === true ? true : null;
        });

        $this->app->booted(function () {
            CorePermissions::boot();
        });

        foreach ($this->policies as $key => $policy) {
            Gate::policy($key, $policy);
        }

        $this->app->extend('auth.password', function ($broker, $app) {
            return ($app['auth']->getProvider() instanceof UserProvider)
                ? new PasswordBrokerManager($app)
                : $broker;
        });
    }
}
