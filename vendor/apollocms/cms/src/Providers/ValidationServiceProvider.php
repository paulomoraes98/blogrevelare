<?php

namespace ApolloCMS\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use ApolloCMS\Validation\UniqueEntryValue;
use ApolloCMS\Validation\UniqueTermValue;
use ApolloCMS\Validation\UniqueUserValue;

class ValidationServiceProvider extends ServiceProvider
{
    protected $rules = [
        'unique_entry_value' => UniqueEntryValue::class,
        'unique_term_value' => UniqueTermValue::class,
        'unique_user_value' => UniqueUserValue::class,
    ];

    public function boot()
    {
        foreach ($this->rules as $rule => $class) {
            Validator::extend($rule, $class);
        }
    }
}
