<?php

namespace ApolloCMS\Providers;

use Illuminate\Console\Application as Artisan;
use Illuminate\Support\ServiceProvider;
use ApolloCMS\Console\Commands;

class ConsoleServiceProvider extends ServiceProvider
{
    protected $commands = [
        Commands\ListCommand::class,
        Commands\AddonsDiscover::class,
        Commands\AssetsGeneratePresets::class,
        Commands\AssetsMeta::class,
        Commands\GlideClear::class,
        Commands\Install::class,
        Commands\MakeAction::class,
        Commands\MakeAddon::class,
        Commands\MakeFieldtype::class,
        Commands\MakeModifier::class,
        Commands\MakeScope::class,
        Commands\MakeFilter::class,
        Commands\MakeTag::class,
        Commands\MakeWidget::class,
        Commands\MakeUser::class,
        Commands\Rtfm::class,
        Commands\ApocheClear::class,
        Commands\ApocheRefresh::class,
        Commands\ApocheWarm::class,
        Commands\ApocheDoctor::class,
        Commands\StaticClear::class,
        // Commands\MakeUserMigration::class,
        Commands\SupportDetails::class,
        Commands\AuthMigration::class,
        Commands\Multisite::class,
        Commands\SiteClear::class,
        Commands\UpdatesRun::class,
    ];

    public function boot()
    {
        Artisan::starting(function ($artisan) {
            $artisan->resolveCommands($this->commands);
        });

        $this->publishes([
            __DIR__.'/../Console/Please/please.stub' => base_path('please'),
        ], 'apollocms');
    }
}
