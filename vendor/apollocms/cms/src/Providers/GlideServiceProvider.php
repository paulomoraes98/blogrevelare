<?php

namespace ApolloCMS\Providers;

use Facades\ApolloCMS\Imaging\GlideServer;
use Illuminate\Support\ServiceProvider;
use League\Glide\Server;
use ApolloCMS\Contracts\Imaging\ImageManipulator;
use ApolloCMS\Contracts\Imaging\UrlBuilder;
use ApolloCMS\Facades\Config;
use ApolloCMS\Facades\Image;
use ApolloCMS\Imaging\GlideImageManipulator;
use ApolloCMS\Imaging\GlideUrlBuilder;
use ApolloCMS\Imaging\ImageGenerator;
use ApolloCMS\Imaging\PresetGenerator;
use ApolloCMS\Imaging\StaticUrlBuilder;
use ApolloCMS\Support\Str;

class GlideServiceProvider extends ServiceProvider
{
    public $defer = true;

    public function register()
    {
        $this->app->bind(UrlBuilder::class, function () {
            return $this->getBuilder();
        });

        $this->app->bind(ImageManipulator::class, function () {
            return new GlideImageManipulator(
                $this->app->make(UrlBuilder::class)
            );
        });

        $this->app->singleton(Server::class, function () {
            return GlideServer::create();
        });

        $this->app->bind(PresetGenerator::class, function ($app) {
            return new PresetGenerator(
                $app->make(ImageGenerator::class),
                Image::manipulationPresets()
            );
        });
    }

    private function getBuilder()
    {
        $route = Config::get('apollocms.assets.image_manipulation.route');

        if (Config::get('apollocms.assets.image_manipulation.cache')) {
            return new StaticUrlBuilder($this->app->make(ImageGenerator::class), [
                'route' => Str::start($route, '/'),
            ]);
        }

        return new GlideUrlBuilder([
            'key' => (Config::get('apollocms.assets.image_manipulation.secure')) ? Config::getAppKey() : null,
            'route' => $route,
        ]);
    }

    public function provides()
    {
        return [ImageManipulator::class, Server::class, PresetGenerator::class];
    }
}
