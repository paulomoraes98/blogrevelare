<?php

namespace ApolloCMS\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use ApolloCMS\Facades\Preference;
use ApolloCMS\Sites\Sites;
use ApolloCMS\ApolloCMS;

class AppServiceProvider extends ServiceProvider
{
    protected $root = __DIR__.'/../..';

    protected $configFiles = [
        'amp', 'api', 'assets', 'cp', 'editions', 'forms', 'git', 'graphql', 'live_preview', 'oauth', 'protect', 'revisions',
        'routes', 'search', 'static_caching', 'sites', 'apoche', 'system', 'users',
    ];

    public function boot()
    {
        $this->app->booted(function () {
            ApolloCMS::runBootedCallbacks();
            $this->loadRoutesFrom("{$this->root}/routes/routes.php");
        });

        $this->registerMiddlewareGroup();

        $this->app[\Illuminate\Contracts\Http\Kernel::class]
            ->pushMiddleware(\ApolloCMS\Http\Middleware\PoweredByHeader::class)
            ->pushMiddleware(\ApolloCMS\Http\Middleware\CheckComposerJsonScripts::class)
            ->pushMiddleware(\ApolloCMS\Http\Middleware\CheckMultisite::class);

        $this->loadViewsFrom("{$this->root}/resources/views", 'apollocms');

        collect($this->configFiles)->each(function ($config) {
            $this->publishes(["{$this->root}/config/$config.php" => config_path("apollocms/$config.php")], 'apollocms');
        });

        $this->publishes([
            "{$this->root}/resources/users" => resource_path('users'),
        ], 'apollocms');

        $this->publishes([
            "{$this->root}/resources/dist" => public_path('vendor/apollocms/cp'),
        ], 'apollocms-cp');

        $this->loadTranslationsFrom("{$this->root}/resources/lang", 'apollocms');
        $this->loadJsonTranslationsFrom("{$this->root}/resources/lang");

        $this->publishes([
            "{$this->root}/resources/lang" => resource_path('lang/vendor/apollocms'),
        ], 'apollocms-translations');

        $this->loadViewsFrom("{$this->root}/resources/views/extend", 'apollocms');

        $this->publishes([
            "{$this->root}/resources/views/extend/forms" => resource_path('views/vendor/apollocms/forms'),
        ], 'apollocms-forms');

        $this->app['redirect']->macro('cpRoute', function ($route, $parameters = []) {
            return $this->to(cp_route($route, $parameters));
        });

        Carbon::setToStringFormat(config('apollocms.system.date_format'));

        Carbon::macro('inPreferredFormat', function () {
            return $this->format(
                Preference::get('date_format', config('apollocms.cp.date_format'))
            );
        });
    }

    public function register()
    {
        collect($this->configFiles)->each(function ($config) {
            $this->mergeConfigFrom("{$this->root}/config/$config.php", "apollocms.$config");
        });

        $this->app->singleton(Sites::class, function () {
            return new Sites(config('apollocms.sites'));
        });

        collect([
            \ApolloCMS\Contracts\Entries\EntryRepository::class => \ApolloCMS\Apoche\Repositories\EntryRepository::class,
            \ApolloCMS\Contracts\Taxonomies\TermRepository::class => \ApolloCMS\Apoche\Repositories\TermRepository::class,
            \ApolloCMS\Contracts\Taxonomies\TaxonomyRepository::class => \ApolloCMS\Apoche\Repositories\TaxonomyRepository::class,
            \ApolloCMS\Contracts\Entries\CollectionRepository::class => \ApolloCMS\Apoche\Repositories\CollectionRepository::class,
            \ApolloCMS\Contracts\Globals\GlobalRepository::class => \ApolloCMS\Apoche\Repositories\GlobalRepository::class,
            \ApolloCMS\Contracts\Assets\AssetContainerRepository::class => \ApolloCMS\Apoche\Repositories\AssetContainerRepository::class,
            \ApolloCMS\Contracts\Structures\StructureRepository::class => \ApolloCMS\Structures\StructureRepository::class,
            \ApolloCMS\Contracts\Structures\CollectionTreeRepository::class => \ApolloCMS\Apoche\Repositories\CollectionTreeRepository::class,
            \ApolloCMS\Contracts\Structures\NavTreeRepository::class => \ApolloCMS\Apoche\Repositories\NavTreeRepository::class,
            \ApolloCMS\Contracts\Structures\NavigationRepository::class => \ApolloCMS\Apoche\Repositories\NavigationRepository::class,
            \ApolloCMS\Contracts\Assets\AssetRepository::class => \ApolloCMS\Assets\AssetRepository::class,
            \ApolloCMS\Contracts\Forms\FormRepository::class => \ApolloCMS\Forms\FormRepository::class,
        ])->each(function ($concrete, $abstract) {
            if (! $this->app->bound($abstract)) {
                ApolloCMS::repository($abstract, $concrete);
            }
        });

        $this->app->singleton(\ApolloCMS\Contracts\Data\DataRepository::class, function ($app) {
            return (new \ApolloCMS\Data\DataRepository)
                ->setRepository('entry', \ApolloCMS\Contracts\Entries\EntryRepository::class)
                ->setRepository('term', \ApolloCMS\Contracts\Taxonomies\TermRepository::class)
                ->setRepository('collection', \ApolloCMS\Contracts\Entries\CollectionRepository::class)
                ->setRepository('taxonomy', \ApolloCMS\Contracts\Taxonomies\TaxonomyRepository::class)
                ->setRepository('global', \ApolloCMS\Contracts\Globals\GlobalRepository::class)
                ->setRepository('asset', \ApolloCMS\Contracts\Assets\AssetRepository::class)
                ->setRepository('user', \ApolloCMS\Contracts\Auth\UserRepository::class);
        });

        $this->app->bind(\ApolloCMS\Fields\BlueprintRepository::class, function () {
            return (new \ApolloCMS\Fields\BlueprintRepository)
                ->setDirectory(resource_path('blueprints'))
                ->setFallback('default', function () {
                    return \ApolloCMS\Facades\Blueprint::makeFromFields([
                        'content' => ['type' => 'markdown', 'localizable' => true],
                    ]);
                });
        });

        $this->app->bind(\ApolloCMS\Fields\FieldsetRepository::class, function () {
            return (new \ApolloCMS\Fields\FieldsetRepository)
                ->setDirectory(resource_path('fieldsets'));
        });
    }

    protected function registerMiddlewareGroup()
    {
        $this->app->make(Router::class)->middlewareGroup('apollocms.web', [
            \ApolloCMS\Http\Middleware\ApocheLock::class,
            \ApolloCMS\Http\Middleware\Localize::class,
            \ApolloCMS\Http\Middleware\AuthGuard::class,
            \ApolloCMS\StaticCaching\Middleware\Cache::class,
        ]);
    }
}
