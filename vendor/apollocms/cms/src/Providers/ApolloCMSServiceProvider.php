<?php

namespace ApolloCMS\Providers;

use Illuminate\Support\AggregateServiceProvider;

class ApolloCMSServiceProvider extends AggregateServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        IgnitionServiceProvider::class,
        ViewServiceProvider::class,
        AppServiceProvider::class,
        ConsoleServiceProvider::class,
        CollectionsServiceProvider::class,
        CacheServiceProvider::class,
        FilesystemServiceProvider::class,
        ExtensionServiceProvider::class,
        EventServiceProvider::class,
        \ApolloCMS\Apoche\ServiceProvider::class,
        AuthServiceProvider::class,
        GlideServiceProvider::class,
        MarkdownServiceProvider::class,
        \ApolloCMS\Search\ServiceProvider::class,
        \ApolloCMS\StaticCaching\ServiceProvider::class,
        \ApolloCMS\Revisions\ServiceProvider::class,
        CpServiceProvider::class,
        ValidationServiceProvider::class,
        RouteServiceProvider::class,
        BroadcastServiceProvider::class,
        \ApolloCMS\API\ServiceProvider::class,
        \ApolloCMS\Git\ServiceProvider::class,
        \ApolloCMS\GraphQL\ServiceProvider::class,
    ];
}
