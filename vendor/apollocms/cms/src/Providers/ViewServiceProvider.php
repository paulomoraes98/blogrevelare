<?php

namespace ApolloCMS\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use ApolloCMS\Facades\Site;
use ApolloCMS\View\Lunar\Engine;
use ApolloCMS\View\Lunar\Parser;
use ApolloCMS\View\Cascade;
use ApolloCMS\View\Store;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Store::class);

        $this->app->singleton(Cascade::class, function ($app) {
            return new Cascade($app['request'], Site::current());
        });

        $this->app->bind(Parser::class, function ($app) {
            return (new Parser)
                ->callback([Engine::class, 'renderTag'])
                ->cascade($app[Cascade::class]);
        });

        $this->app->singleton(Engine::class, function ($app) {
            return new Engine($app['files'], $app[Parser::class]);
        });
    }

    public function boot()
    {
        View::macro('withoutExtractions', function () {
            if ($this->engine instanceof Engine) {
                $this->engine->withoutExtractions();
            }

            return $this;
        });

        tap($this->app['view'], function ($view) {
            $resolver = function () {
                return $this->app[Engine::class];
            };
            $view->addExtension('lunar.html', 'lunar', $resolver);
            $view->addExtension('lunar.php', 'lunar', $resolver);
        });

        ini_set('pcre.backtrack_limit', config('apollocms.system.pcre_backtrack_limit', -1));
    }
}
