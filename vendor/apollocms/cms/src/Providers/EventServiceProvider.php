<?php

namespace ApolloCMS\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        \ApolloCMS\View\Events\ViewRendered::class => [
            \ApolloCMS\View\Debugbar\AddVariables::class,
            \ApolloCMS\View\Debugbar\AddRequestMessage::class,
        ],
        \Illuminate\Auth\Events\Login::class => [
            \ApolloCMS\Auth\SetLastLoginTimestamp::class,
        ],
        \ApolloCMS\Events\CollectionTreeSaved::class => [
            \ApolloCMS\Entries\UpdateStructuredEntryUris::class,
            \ApolloCMS\Entries\UpdateStructuredEntryOrder::class,
        ],
    ];

    protected $subscribe = [
        // \ApolloCMS\Taxonomies\TermTracker::class, // TODO
        \ApolloCMS\Listeners\GeneratePresetImageManipulations::class,
    ];
}
