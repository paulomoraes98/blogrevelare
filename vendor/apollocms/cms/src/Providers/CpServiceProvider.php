<?php

namespace ApolloCMS\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use ApolloCMS\CP\Utilities\CoreUtilities;
use ApolloCMS\CP\Utilities\UtilityRepository;
use ApolloCMS\Extensions\Translation\Loader;
use ApolloCMS\Extensions\Translation\Translator;
use ApolloCMS\Facades\User;
use ApolloCMS\Http\View\Composers\CustomLogoComposer;
use ApolloCMS\Http\View\Composers\FieldComposer;
use ApolloCMS\Http\View\Composers\JavascriptComposer;
use ApolloCMS\Http\View\Composers\NavComposer;
use ApolloCMS\Http\View\Composers\SessionExpiryComposer;
use ApolloCMS\Licensing\LicenseManager;
use ApolloCMS\Licensing\Outpost;

class CpServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer('apollocms::*', function ($view) {
            $view->with('user', User::current());
        });

        View::composer(FieldComposer::VIEWS, FieldComposer::class);
        View::composer(SessionExpiryComposer::VIEWS, SessionExpiryComposer::class);
        View::composer(JavascriptComposer::VIEWS, JavascriptComposer::class);
        View::composer(NavComposer::VIEWS, NavComposer::class);
        View::composer(CustomLogoComposer::VIEWS, CustomLogoComposer::class);

        CoreUtilities::boot();

        Blade::directive('cp_svg', function ($expression) {
            return "<?php echo ApolloCMS::svg({$expression}) ?>";
        });

        $this->registerMiddlewareGroups();
    }

    public function register()
    {
        $this->app->extend('translation.loader', function ($loader, $app) {
            return new Loader($loader, $app['path.lang']);
        });

        $this->app->extend('translator', function ($translator, $app) {
            $extended = new Translator($app['files'], $translator->getLoader(), $translator->getLocale());
            $extended->setFallback($translator->getFallback());

            return $extended;
        });

        $this->app->singleton(UtilityRepository::class, function () {
            return new UtilityRepository;
        });

        $this->app->singleton(LicenseManager::class, function ($app) {
            return new LicenseManager($app[Outpost::class]);
        });
    }

    protected function registerMiddlewareGroups()
    {
        $router = $this->app->make(Router::class);

        $router->middlewareGroup('apollocms.cp', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \ApolloCMS\Http\Middleware\CP\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Foundation\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \ApolloCMS\Http\Middleware\CP\ContactOutpost::class,
            \ApolloCMS\Http\Middleware\CP\AuthGuard::class,
        ]);

        $router->middlewareGroup('apollocms.cp.authenticated', [
            \ApolloCMS\Http\Middleware\CP\Authorize::class,
            \ApolloCMS\Http\Middleware\CP\Localize::class,
            \ApolloCMS\Http\Middleware\CP\CountUsers::class,
        ]);
    }
}
