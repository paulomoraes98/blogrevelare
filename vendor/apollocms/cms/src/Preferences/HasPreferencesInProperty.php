<?php

namespace ApolloCMS\Preferences;

use ApolloCMS\Support\Arr;

trait HasPreferencesInProperty
{
    use HasPreferences;

    protected $preferences = [];

    protected function getPreferences()
    {
        return $this->preferences;
    }

    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function mergePreferences($preferences)
    {
        $this->preferences = array_merge($this->getPreferences(), Arr::wrap($preferences));

        return $this;
    }
}
