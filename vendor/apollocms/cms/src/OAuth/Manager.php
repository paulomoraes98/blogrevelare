<?php

namespace ApolloCMS\OAuth;

class Manager
{
    protected static $providers = [];

    public function enabled()
    {
        return config('apollocms.oauth.enabled')
            && ! empty(config('apollocms.oauth.providers'));
    }

    public function provider($provider)
    {
        return static::$providers[$provider] = static::$providers[$provider] ?? new Provider($provider);
    }

    public function providers()
    {
        return collect(config('apollocms.oauth.providers'))
            ->map(function ($provider, $key) {
                // Allow users to specify just the name as a string, or as a name/label pair.
                // eg. ['github' => 'GitHub', 'facebook']
                $expanded = is_string($key);

                return $this
                    ->provider($expanded ? $key : $provider)
                    ->label($expanded ? $provider : null);
            });
    }
}
