<?php

namespace ApolloCMS\OAuth;

use Closure;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use ApolloCMS\Contracts\Auth\User as ApolloCMSUser;
use ApolloCMS\Facades\File;
use ApolloCMS\Facades\User;
use ApolloCMS\Support\Str;

class Provider
{
    protected $name;
    protected $label;
    protected $userCallback;
    protected $userDataCallback;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get a ApolloCMS user ID from an OAuth user ID.
     *
     * @param string $id  User ID from an OAuth provider
     * @return string|null  A ApolloCMS user ID
     */
    public function getUserId(string $id): ?string
    {
        return array_flip($this->getIds())[$id] ?? null;
    }

    public function findOrCreateUser($socialite): ApolloCMSUser
    {
        if ($user = User::findByOAuthId($this->name, $socialite->getId())) {
            return $user;
        }

        return $this->createUser($socialite);
    }

    /**
     * Create a ApolloCMS user from a Socialite user.
     *
     * @param SocialiteUser $socialite
     * @return ApolloCMSUser
     */
    public function createUser($socialite): ApolloCMSUser
    {
        $user = $this->makeUser($socialite);

        $user->save();

        $this->setUserProviderId($user, $socialite->getId());

        return $user;
    }

    public function makeUser($socialite): ApolloCMSUser
    {
        if ($this->userCallback) {
            return call_user_func($this->userCallback, $socialite);
        }

        return User::make()
            ->email($socialite->getEmail())
            ->data($this->userData($socialite));
    }

    public function userData($socialite)
    {
        if ($this->userDataCallback) {
            return call_user_func($this->userDataCallback, $socialite);
        }

        return ['name' => $socialite->getName()];
    }

    public function withUserData(Closure $callback)
    {
        $this->userDataCallback = $callback;
    }

    public function withUser(Closure $callback)
    {
        $this->userCallback = $callback;
    }

    public function loginUrl()
    {
        return route('apollocms.oauth.login', $this->name);
    }

    public function label($label = null)
    {
        if (func_num_args() === 0) {
            return $this->label ?? Str::title($this->name);
        }

        $this->label = $label;

        return $this;
    }

    protected function getIds()
    {
        if (! File::exists($path = $this->storagePath())) {
            $this->setIds([]);
        }

        return require $path;
    }

    protected function setIds($ids)
    {
        $contents = '<?php return '.var_export($ids, true).';';

        File::put($this->storagePath(), $contents);
    }

    protected function setUserProviderId($user, $id)
    {
        $ids = $this->getIds();

        $ids[$user->id()] = $id;

        $this->setIds($ids);
    }

    protected function storagePath()
    {
        return storage_path("apollocms/oauth/{$this->name}.php");
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'label' => $this->label(),
            'loginUrl' => $this->loginUrl(),
        ];
    }
}
