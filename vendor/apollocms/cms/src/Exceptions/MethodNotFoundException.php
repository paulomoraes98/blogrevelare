<?php

namespace ApolloCMS\Exceptions;

/**
 * A called method doesn't exist.
 */
class MethodNotFoundException extends \Exception
{
}
