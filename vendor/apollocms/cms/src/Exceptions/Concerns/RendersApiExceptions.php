<?php

namespace ApolloCMS\Exceptions\Concerns;

use ApolloCMS\Exceptions\NotFoundHttpException;
use ApolloCMS\Exceptions\ApolloCMSProAuthorizationException;
use ApolloCMS\Exceptions\ApolloCMSProRequiredException;

trait RendersApiExceptions
{
    protected function renderException($request, $e)
    {
        if ($e instanceof NotFoundHttpException) {
            return response()->json(['message' => $e->getMessage() ?: 'Not found.'], 404);
        }

        if ($e instanceof ApolloCMSProAuthorizationException) {
            throw new ApolloCMSProRequiredException($e->getMessage());
        }

        return parent::render($request, $e);
    }
}
