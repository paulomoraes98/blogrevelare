<?php

namespace ApolloCMS\Exceptions;

/**
 * Bad. Stops the application in its tracks.
 */
class FatalException extends \Exception
{
}
