<?php

namespace ApolloCMS\Exceptions;

use Exception;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;
use ApolloCMS\ApolloCMS;

class NotBootedException extends Exception implements ProvidesSolution
{
    public function getSolution(): Solution
    {
        return BaseSolution::create('ApolloCMS has not booted')
            ->setSolutionDescription('
                Code has been run that relies on ApolloCMS having already been booted.\
                Typically found inside a service provider. Wrap your code in a `$this->app->booted` callback.'
            )->setDocumentationLinks([
                'Read the addons guide' => ApolloCMS::docsUrl('extending/addons'),
            ]);
    }
}
