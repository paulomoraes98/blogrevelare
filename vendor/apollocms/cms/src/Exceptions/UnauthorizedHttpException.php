<?php

namespace ApolloCMS\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UnauthorizedHttpException extends HttpException
{
}
