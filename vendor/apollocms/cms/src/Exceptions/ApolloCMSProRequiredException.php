<?php

namespace ApolloCMS\Exceptions;

use Exception;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;
use ApolloCMS\Ignition\Solutions\EnableApolloCMSPro;

class ApolloCMSProRequiredException extends Exception implements ProvidesSolution
{
    public function getSolution(): Solution
    {
        return new EnableApolloCMSPro;
    }
}
