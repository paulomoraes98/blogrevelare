<?php

namespace ApolloCMS\Exceptions;

use ApolloCMS\ApolloCMS;
use ApolloCMS\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as SymfonyException;

class NotFoundHttpException extends SymfonyException
{
    public function render()
    {
        if (ApolloCMS::isCpRoute()) {
            return response()->view('apollocms::errors.404', [], 404);
        }

        if (view()->exists('errors.404')) {
            return response($this->contents(), 404);
        }
    }

    protected function contents()
    {
        return (new View)
            ->template('errors.404')
            ->layout($this->layout())
            ->with(['response_code' => 404])
            ->render();
    }

    protected function layout()
    {
        $layouts = collect([
            'errors.layout',
            'layouts.layout',
            'layout',
            'apollocms::blank',
        ]);

        return $layouts->filter()->first(function ($layout) {
            return view()->exists($layout);
        });
    }
}
