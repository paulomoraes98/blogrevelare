<?php

namespace ApolloCMS\Exceptions;

/**
 * When a file is not found.
 */
class FileNotFoundException extends \Exception
{
}
