<?php

namespace ApolloCMS\Exceptions;

/**
 * Trigger a 404.
 */
class UrlNotFoundException extends \Exception
{
}
