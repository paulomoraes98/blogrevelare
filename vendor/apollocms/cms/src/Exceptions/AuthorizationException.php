<?php

namespace ApolloCMS\Exceptions;

use Illuminate\Auth\Access\AuthorizationException as Exception;

class AuthorizationException extends Exception
{
    //
}
