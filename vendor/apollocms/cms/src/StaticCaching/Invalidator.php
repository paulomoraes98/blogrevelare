<?php

namespace ApolloCMS\StaticCaching;

interface Invalidator
{
    public function invalidate($item);
}
