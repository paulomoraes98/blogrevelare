<?php

namespace ApolloCMS\StaticCaching;

use Illuminate\Cache\Repository;
use ApolloCMS\Facades\Site;
use ApolloCMS\StaticCaching\Cachers\ApplicationCacher;
use ApolloCMS\StaticCaching\Cachers\FileCacher;
use ApolloCMS\StaticCaching\Cachers\NullCacher;
use ApolloCMS\StaticCaching\Cachers\Writer;
use ApolloCMS\Support\Manager;

class StaticCacheManager extends Manager
{
    protected function invalidImplementationMessage($name)
    {
        return "Static cache strategy [{$name}] is not defined.";
    }

    public function getDefaultDriver()
    {
        return $this->app['config']['apollocms.static_caching.strategy'] ?? 'null';
    }

    public function createNullDriver()
    {
        return new NullCacher;
    }

    public function createFileDriver(array $config)
    {
        return new FileCacher(new Writer, $this->app[Repository::class], $config);
    }

    public function createApplicationDriver(array $config)
    {
        return new ApplicationCacher($this->app[Repository::class], $config);
    }

    protected function getConfig($name)
    {
        if (! $config = $this->app['config']["apollocms.static_caching.strategies.$name"]) {
            return null;
        }

        return array_merge($config, [
            'exclude' => $this->app['config']['apollocms.static_caching.exclude'] ?? [],
            'ignore_query_strings' => $this->app['config']['apollocms.static_caching.ignore_query_strings'] ?? false,
            'base_url' => $this->app['request']->root(),
            'locale' => Site::current()->handle(),
        ]);
    }
}
