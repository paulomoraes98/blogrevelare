<?php

namespace ApolloCMS\StaticCaching;

use Illuminate\Contracts\Queue\ShouldQueue;
use ApolloCMS\Events\EntryDeleted;
use ApolloCMS\Events\EntrySaved;
use ApolloCMS\Events\GlobalSetDeleted;
use ApolloCMS\Events\GlobalSetSaved;
use ApolloCMS\Events\NavDeleted;
use ApolloCMS\Events\NavSaved;
use ApolloCMS\Events\TermDeleted;
use ApolloCMS\Events\TermSaved;

class Invalidate implements ShouldQueue
{
    protected $invalidator;

    protected $events = [
        EntrySaved::class => 'invalidateEntry',
        EntryDeleted::class => 'invalidateEntry',
        TermSaved::class => 'invalidateTerm',
        TermDeleted::class => 'invalidateTerm',
        GlobalSetSaved::class => 'invalidateGlobalSet',
        GlobalSetDeleted::class => 'invalidateGlobalSet',
        NavSaved::class => 'invalidateNav',
        NavDeleted::class => 'invalidateNav',
    ];

    public function __construct(Invalidator $invalidator)
    {
        $this->invalidator = $invalidator;
    }

    public function subscribe($dispatcher)
    {
        foreach ($this->events as $event => $method) {
            $dispatcher->listen($event, self::class.'@'.$method);
        }
    }

    public function invalidateEntry($event)
    {
        $this->invalidator->invalidate($event->entry);
    }

    public function invalidateTerm($event)
    {
        $this->invalidator->invalidate($event->term);
    }

    public function invalidateGlobalSet($event)
    {
        $this->invalidator->invalidate($event->globals);
    }

    public function invalidateNav($event)
    {
        $this->invalidator->invalidate($event->nav);
    }
}
