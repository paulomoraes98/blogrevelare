<?php

namespace ApolloCMS;

use Facades\ApolloCMS\Console\Processes\Composer;

class Version
{
    public function get()
    {
        return Composer::installedVersion(ApolloCMS::PACKAGE);
    }
}
