<?php

namespace ApolloCMS\Structures;

use Facades\ApolloCMS\Structures\CollectionTreeDiff;
use ApolloCMS\Contracts\Structures\CollectionTreeRepository;
use ApolloCMS\Events\CollectionTreeDeleted;
use ApolloCMS\Events\CollectionTreeSaved;
use ApolloCMS\Facades\Blink;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\Apoche;

class CollectionTree extends Tree
{
    public function structure()
    {
        return Blink::once('collection-tree-structure-'.$this->handle(), function () {
            return Collection::findByHandle($this->handle())->structure();
        });
    }

    public function path()
    {
        $path = Apoche::store('collection-trees')->directory();

        if (Site::hasMultiple()) {
            $path .= $this->locale().'/';
        }

        $handle = $this->collection()->handle();

        return "{$path}{$handle}.yaml";
    }

    protected function dispatchSavedEvent()
    {
        CollectionTreeSaved::dispatch($this);
    }

    protected function dispatchDeletedEvent()
    {
        CollectionTreeDeleted::dispatch($this);
    }

    public function collection()
    {
        return $this->structure()->collection();
    }

    public function diff()
    {
        return CollectionTreeDiff::analyze(
            $this->original['tree'],
            $this->tree,
            $this->structure()->expectsRoot()
        );
    }

    protected function repository()
    {
        return app(CollectionTreeRepository::class);
    }
}
