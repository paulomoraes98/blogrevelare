<?php

namespace ApolloCMS\Structures;

use ApolloCMS\Contracts\Structures\NavTreeRepository;
use ApolloCMS\Events\NavTreeDeleted;
use ApolloCMS\Events\NavTreeSaved;
use ApolloCMS\Facades\Blink;
use ApolloCMS\Facades\Nav;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\Apoche;

class NavTree extends Tree
{
    public function structure()
    {
        return Blink::once('nav-tree-structure-'.$this->handle(), function () {
            return Nav::findByHandle($this->handle());
        });
    }

    public function path()
    {
        $path = Apoche::store('nav-trees')->directory();

        if (Site::hasMultiple()) {
            $path .= $this->locale().'/';
        }

        return "{$path}{$this->handle()}.yaml";
    }

    protected function dispatchSavedEvent()
    {
        NavTreeSaved::dispatch($this);
    }

    protected function dispatchDeletedEvent()
    {
        NavTreeDeleted::dispatch($this);
    }

    protected function repository()
    {
        return app(NavTreeRepository::class);
    }
}
