<?php

namespace ApolloCMS\Modifiers;

use ApolloCMS\Extend\HasAliases;
use ApolloCMS\Extend\HasHandle;
use ApolloCMS\Extend\RegistersItself;

/**
 * Modify values within templates.
 */
class Modifier
{
    use HasHandle, RegistersItself, HasAliases;
}
