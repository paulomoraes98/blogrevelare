<?php

namespace ApolloCMS\Auth;

use Illuminate\Auth\Events\Login;
use ApolloCMS\Contracts\Auth\User as ApolloCMSUser;
use ApolloCMS\Facades\User;

class SetLastLoginTimestamp
{
    public function handle(Login $event)
    {
        if ($event->user instanceof ApolloCMSUser) {
            User::fromUser($event->user)->setLastLogin(now());
        }
    }
}
