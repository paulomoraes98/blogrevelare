<?php

namespace ApolloCMS\Auth\Protect\Protectors\Password;

use Facades\ApolloCMS\Auth\Protect\Protectors\Password\Token;
use ApolloCMS\Auth\Protect\Protectors\Protector;

class PasswordProtector extends Protector
{
    /**
     * Provide protection.
     *
     * @return void
     */
    public function protect()
    {
        if (empty(array_get($this->config, 'allowed', []))) {
            abort(403);
        }

        if ($this->isPasswordFormUrl()) {
            return;
        }

        if (! $this->hasEnteredValidPassword()) {
            $this->redirectToPasswordForm();
        }
    }

    public function hasEnteredValidPassword()
    {
        return (new Guard($this->scheme))->check(
            session("apollocms:protect:password.passwords.{$this->scheme}")
        );
    }

    protected function isPasswordFormUrl()
    {
        return $this->url === $this->getPasswordFormUrl();
    }

    protected function redirectToPasswordForm()
    {
        $url = $this->getPasswordFormUrl().'?token='.$this->generateToken();

        abort(redirect($url));
    }

    protected function getPasswordFormUrl()
    {
        return url($this->config['form_url'] ?? route('apollocms.protect.password.show'));
    }

    protected function generateToken()
    {
        $token = Token::generate();

        session()->put("apollocms:protect:password.tokens.$token", [
            'scheme' => $this->scheme,
            'url' => $this->url,
        ]);

        return $token;
    }
}
