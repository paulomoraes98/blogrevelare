<?php

namespace ApolloCMS\Auth\Protect\Protectors\Password;

use ApolloCMS\Support\Str;

class Token
{
    public function generate()
    {
        return Str::random(32);
    }
}
