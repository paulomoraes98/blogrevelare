<?php

namespace ApolloCMS\Auth\Protect\Protectors;

class Fallback extends Protector
{
    public function protect()
    {
        abort(403);
    }
}
