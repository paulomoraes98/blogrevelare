<?php

namespace ApolloCMS\Auth\Protect;

use ApolloCMS\Auth\Protect\Protectors\Authenticated;
use ApolloCMS\Auth\Protect\Protectors\Fallback;
use ApolloCMS\Auth\Protect\Protectors\IpAddress;
use ApolloCMS\Auth\Protect\Protectors\NullProtector;
use ApolloCMS\Auth\Protect\Protectors\Password\PasswordProtector;
use ApolloCMS\Support\Manager;

class ProtectorManager extends Manager
{
    protected function invalidImplementationMessage($name)
    {
        return "Protection scheme [{$name}] is not defined.";
    }

    protected function invalidDriverMessage($driver, $name)
    {
        return "Driver [{$driver}] in protection scheme [{$name}] is invalid.";
    }

    protected function getConfig($name)
    {
        return $this->app['config']["apollocms.protect.schemes.$name"];
    }

    public function getDefaultDriver()
    {
        return 'fallback';
    }

    public function createNullDriver()
    {
        return new NullProtector;
    }

    public function createFallbackDriver()
    {
        return new Fallback;
    }

    public function createAuthDriver($config, $name)
    {
        return $this->protector(new Authenticated, $name, $config);
    }

    public function createIpAddressDriver($config, $name)
    {
        return $this->protector(new IpAddress, $name, $config);
    }

    public function createPasswordDriver($config, $name)
    {
        return $this->protector(new PasswordProtector, $name, $config);
    }

    protected function protector($class, $name, $config)
    {
        return $class->setConfig($config)->setScheme($name);
    }
}
