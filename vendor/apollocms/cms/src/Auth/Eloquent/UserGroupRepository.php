<?php

namespace ApolloCMS\Auth\Eloquent;

use ApolloCMS\Auth\File\UserGroupRepository as BaseRepository;

class UserGroupRepository extends BaseRepository
{
    public function make()
    {
        return new UserGroup;
    }
}
