<?php

namespace ApolloCMS\Auth\Eloquent;

use ApolloCMS\Auth\RoleRepository as BaseRepository;
use ApolloCMS\Contracts\Auth\Role as RoleContract;

class RoleRepository extends BaseRepository
{
    public function make(string $handle = null): RoleContract
    {
        return (new Role)->handle($handle);
    }
}
