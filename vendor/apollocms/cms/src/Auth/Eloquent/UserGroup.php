<?php

namespace ApolloCMS\Auth\Eloquent;

use ApolloCMS\Auth\File\UserGroup as FileUserGroup;
use ApolloCMS\Facades\User;

class UserGroup extends FileUserGroup
{
    public function queryUsers()
    {
        return User::query()->whereIn('id', $this->getUserIds());
    }

    protected function getUserIds()
    {
        return \DB::connection(config('apollocms.users.database'))
            ->table('group_user')
            ->where('group_id', $this->id())
            ->pluck('user_id');
    }
}
