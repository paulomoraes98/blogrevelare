<?php

namespace ApolloCMS\Auth\Eloquent;

use ApolloCMS\Auth\UserCollection;
use ApolloCMS\Query\EloquentQueryBuilder;

class UserQueryBuilder extends EloquentQueryBuilder
{
    protected function transform($items, $columns = ['*'])
    {
        return UserCollection::make($items)->map(function ($model) {
            return User::fromModel($model);
        })->each->selectedQueryColumns($columns);
    }
}
