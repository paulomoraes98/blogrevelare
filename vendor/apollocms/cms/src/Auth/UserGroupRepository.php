<?php

namespace ApolloCMS\Auth;

use ApolloCMS\Contracts\Auth\UserGroup as UserGroupContract;
use ApolloCMS\Contracts\Auth\UserGroupRepository as RepositoryContract;

abstract class UserGroupRepository implements RepositoryContract
{
    public function find($id): ?UserGroupContract
    {
        return $this->all()->get($id);
    }
}
