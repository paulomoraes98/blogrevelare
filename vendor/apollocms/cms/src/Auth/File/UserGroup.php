<?php

namespace ApolloCMS\Auth\File;

use ApolloCMS\Auth\UserGroup as BaseUserGroup;
use ApolloCMS\Facades\User;

class UserGroup extends BaseUserGroup
{
    public function queryUsers()
    {
        return User::query()->where('groups/'.$this->handle(), true);
    }
}
