<?php

namespace ApolloCMS\Auth;

use ApolloCMS\Auth\Eloquent\UserRepository as EloquentRepository;
use ApolloCMS\Apoche\Repositories\UserRepository as ApocheRepository;
use ApolloCMS\Support\Manager;

class UserRepositoryManager extends Manager
{
    public function repository($repository = null)
    {
        return $this->driver($repository);
    }

    public function getDefaultDriver()
    {
        return $this->app['config']['apollocms.users.repository'];
    }

    protected function getConfig($name)
    {
        return $this->app['config']["apollocms.users.repositories.$name"] ?? null;
    }

    public function createFileDriver(array $config)
    {
        return new ApocheRepository($this->app['apoche'], $config);
    }

    public function createEloquentDriver(array $config)
    {
        $config['model'] = $this->app['config']['auth.providers.users.model'];

        return new EloquentRepository($config);
    }
}
