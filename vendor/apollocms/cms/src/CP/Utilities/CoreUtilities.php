<?php

namespace ApolloCMS\CP\Utilities;

use ApolloCMS\Facades\Utility;
use ApolloCMS\Http\Controllers\CP\LicensingController;
use ApolloCMS\Http\Controllers\CP\Utilities\CacheController;
use ApolloCMS\Http\Controllers\CP\Utilities\EmailController;
use ApolloCMS\Http\Controllers\CP\Utilities\GitController;
use ApolloCMS\Http\Controllers\CP\Utilities\PhpInfoController;
use ApolloCMS\Http\Controllers\CP\Utilities\UpdateSearchController;
use ApolloCMS\ApolloCMS;

class CoreUtilities
{
    public static function boot()
    {
        Utility::make('cache')
            ->action([CacheController::class, 'index'])
            ->title(__('Cache Manager'))
            ->icon('cache')
            ->navTitle(__('Cache'))
            ->description(__('apollocms::messages.cache_utility_description'))
            ->docsUrl(ApolloCMS::docsUrl('utilities/cache-manager'))
            ->routes(function ($router) {
                $router->post('cache/{cache}', [CacheController::class, 'clear'])->name('clear');
            })
            ->register();

        Utility::make('phpinfo')
            ->action(PhpInfoController::class)
            ->title(__('PHP Info'))
            ->icon('php')
            ->description(__('apollocms::messages.phpinfo_utility_description'))
            ->docsUrl(ApolloCMS::docsUrl('utilities/phpinfo'))
            ->register();

        Utility::make('search')
            ->view('apollocms::utilities.search')
            ->title(__('Search'))
            ->icon('search-utility')
            ->description(__('apollocms::messages.search_utility_description'))
            ->docsUrl(ApolloCMS::docsUrl('utilities/search'))
            ->routes(function ($router) {
                $router->post('/', [UpdateSearchController::class, 'update'])->name('update');
            })
            ->register();

        Utility::make('email')
            ->view('apollocms::utilities.email')
            ->title(__('Email'))
            ->icon('email-utility')
            ->description(__('apollocms::messages.email_utility_description'))
            ->docsUrl(ApolloCMS::docsUrl('utilities/email'))
            ->routes(function ($router) {
                $router->post('/', [EmailController::class, 'send']);
            })
            ->register();

        Utility::make('licensing')
            ->action([LicensingController::class, 'show'])
            ->title(__('Licensing'))
            ->icon('licensing')
            ->description(__('apollocms::messages.licensing_utility_description'))
            ->docsUrl(ApolloCMS::docsUrl('licensing'))
            ->routes(function ($router) {
                $router->get('refresh', [LicensingController::class, 'refresh'])->name('refresh');
            })
            ->register();

        if (config('apollocms.git.enabled') && ApolloCMS::pro()) {
            Utility::make('git')
                ->action([GitController::class, 'index'])
                ->title('Git')
                ->icon('git')
                ->description(__('apollocms::messages.git_utility_description'))
                ->docsUrl(ApolloCMS::docsUrl('utilities/git'))
                ->routes(function ($router) {
                    $router->post('/', [GitController::class, 'commit'])->name('commit');
                })
                ->register();
        }
    }
}
