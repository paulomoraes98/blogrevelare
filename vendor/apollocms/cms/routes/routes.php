<?php

use Illuminate\Support\Facades\Route;
use ApolloCMS\API\Middleware\Cache;
use ApolloCMS\Http\Middleware\API\SwapExceptionHandler as SwapAPIExceptionHandler;
use ApolloCMS\Http\Middleware\CP\SwapExceptionHandler as SwapCpExceptionHandler;
use ApolloCMS\Http\Middleware\RequireApolloCMSPro;

if (config('apollocms.api.enabled')) {
    Route::middleware([
        SwapApiExceptionHandler::class,
        RequireApolloCMSPro::class,
        Cache::class,
    ])->group(function () {
        Route::middleware(config('apollocms.api.middleware'))
            ->name('apollocms.api.')
            ->prefix(config('apollocms.api.route'))
            ->namespace('ApolloCMS\Http\Controllers\API')
            ->group(__DIR__.'/api.php');
    });
}

if (config('apollocms.cp.enabled')) {
    Route::middleware(SwapCpExceptionHandler::class)->group(function () {
        Route::middleware('apollocms.cp')
            ->name('apollocms.cp.')
            ->prefix(config('apollocms.cp.route'))
            ->namespace('ApolloCMS\Http\Controllers\CP')
            ->group(__DIR__.'/cp.php');
    });
}

Route::middleware(config('apollocms.routes.middleware', 'web'))
    ->namespace('ApolloCMS\Http\Controllers')
    ->group(__DIR__.'/web.php');
