# Release Notes

## 3.1.5 (2020-04-07)

### What's new
- The Apeditor link toolbar allows you to browse for entries.
- Added a `queue` option to the `assets:generate-presets` command.
- The `cache` tag supports cache tags. (Naming is hard.)
- Add status UI for text nav items.

### What's fixed
- Fix SVG dimensions when not using pixels.
- Prevent the 'read only' label and translation icons on `section` fieldtypes.
- Prevent incorrect nav output when you had a nav named the same as a collection.



## 3.1.4 (2020-04-06)

### What's new
- Ability to push queries and middleware into GraphQL.
- Add breadcrumbs to asset browser.
- Add limit param to foreach tag.

### What's fixed
- Fix squished sidebar toggle.
- Prevent unintended deletion of assets through editor.
- Fix autofocus issues in Safari and Firefox.
- Handle encoded characters in uploaded asset filenames.
- Fix Glide 404ing for images in the `public` directory.
- Fix assets being incorrect every other request in some cases.
- Use request helper instead of server variables to fix an issue with Laravel Octane.



## 3.1.3 (2020-04-02)

### What's new
- Status icons are shown in collections' tree views.
- Addons can add external stylesheets.
- Added a `honeypot` variable inside forms.

### What's fixed
- Glide routes will return 404s for non-existent images.
- Recognize tag pairs correctly for a collection alias.
- Fix utf8 handling of base64 encoded strings.
- Fix `markdown` modifier not working with the `code` fieldtype.
- Allow `symfony/var-exporter` 5.1.
- Bump y18n from 4.0.0 to 4.0.1.



## 3.1.2 (2020-03-30)

### What's improved
- Prevent the need to hit enter to add a validation rule.
- Updated German translations.

### What's fixed
- Fix taxonomies url and data handling which fixes a `nav:breadcrumbs` issue.
- Fix "move asset" action not listing all folders.
- Prevent action and glide routes being disabled by config.
- Prevent error during addon tests.



## 3.1.1 (2020-03-25)

### What's improved
- French translations.

### What's fixed
- Fix widths for certain fieldtypes within Grid tables.
- Fix update issue when a nav doesn't have a tree.
- Fix link color inside updater.
- Fix translation typo
- Fix date fieldtypes not displaying.
- Fix issue where the delete action wouldn't show, or would show twice.
- Prevent error on `/cp/auth` when logged in.
- Don't check for composer scripts during tests.



## 3.1.0 (2020-03-24)

### What's new
- Official 3.1 release. 🎉



## 3.1.0-beta.3 (2020-03-24)

### What's new
- `form:create` action and method params.

### What's fixed
- Redirect to CP after CP-based user activation.
- Allow grid tables to dynamically use the most appropriate space.
- Preprocess default values in Apeditor, Grid and Replicator preload methods.
- Bumped `laravel/framework` requirement to versions with security patches.
- Changes from 3.0.49



## 3.1.0-beta.2 (2020-03-22)

### What's new
- Added option to set a custom path to git binary.
- Added `ArrayableString` class, and apply to the `code` fieldtype.
- Added support for `date` input type on `text` fieldtype.
- Added ability to set HTML attributes on `NavItem`s.

### What's improved
- More asset performance improvements.
- Redesign the updater widget.
- Set widget heights to full for a more pleasing experience.
- Display toggle fieldtypes inline when in sidebar.
- Don't show error templates in the template fieldtype.
- When a Replicator has a single set, the add button will not show the set selector.
- Added an icon to the collection widget.

### What's fixed
- Fix custom logo when using arrays with null.
- Fix `trans_choice()` pluralization.
- Fix broadcasting error if you have your routes cached.
- Prevent delete action showing outside of core listings.
- Brought over changes from 3.0.48



## 3.1.0-beta.1 (2020-03-15)

### What's new
- You can configure ApolloCMS to use separate authentication from the rest of your app. 
- Added support for the `mimetypes` validation rule.

### What's improved
- A whole bunch of Amazon S3 performance optimization.
- The `mimes` and `image` validation rules now use the actual mime type rather than just the extension.
- SVG assets can provide their dimensions.

### What's fixed
- GraphQL will filter out draft entries from the entries query by default.
- Fix an error when there's missing asset metadata. It's now lazily loaded.
- Brought over changes from 3.0.47



## 3.1.0-alpha.4 (2020-03-08)

### What's new
- Collection and Nav Trees are now stored separately from their config.
- Added configuration to make REST API resources opt-in.
- Added a form endpoint to the REST API.
- You can disable paste and input rules on Apeditor fields.
- You can add placeholder text to `textarea` fieldtypes.

### What's fixed
- The REST API will filter out draft entries by default.
- Full measure static caching no longer logs when creating the page.
- Form fieldtypes now show data in the API rather than an empty object.
- Removed the minimum character limit for search queries.
- Added the missing jpeg file type icon.
- Update scripts and lock file class will normalize versions even more normalized.
- Brought over changes from 3.0.44-46

### What's changing
- A bunch of structure tree related things outlined in
- A `hasCachedPage` method has been added to the `ApolloCMS\StaticCaching\Cacher` interface.
- GraphQL queries are all disabled by default.
- Global search is now only triggered with a slash. (Not ctrl/alt/shift+f)
- Since REST API resources are now opt-in, everything will 404 until you update your config.
