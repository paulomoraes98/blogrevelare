<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('apollocms::partials.head')
    </head>
    <body class="outside {{ config('apollocms.cp.theme') }}-theme @yield('body_class')">
        <div id="apollocms">
            @yield('content')
        </div>
        @include('apollocms::partials.scripts')
        @yield('scripts')
    </body>
</html>
