@extends('apollocms::layout')
@section('title', __('Collections'))

@section('content')

    @unless($collections->isEmpty())

        <div class="flex items-center justify-between mb-3">
            <h1>{{ __('Collections') }}</h1>

            @can('create', 'ApolloCMS\Contracts\Entries\Collection')
                <a href="{{ cp_route('collections.create') }}" class="btn-primary">{{ __('Create Collection') }}</a>
            @endcan
        </div>

        <collection-list
            :initial-rows="{{ json_encode($collections) }}"
            :initial-columns="{{ json_encode($columns) }}"
            :endpoints="{}">
        </collection-list>

    @else

        @include('apollocms::partials.empty-state', [
            'title' => __('Collections'),
            'description' => __('apollocms::messages.collection_configure_intro'),
            'svg' => 'empty/content',
            'button_text' => __('Create Collection'),
            'button_url' => cp_route('collections.create'),
            'can' => $user->can('create', 'ApolloCMS\Contracts\Entries\Collection')
        ])

    @endunless

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Collections'),
        'url' => ApolloCMS::docsUrl('collections-and-entries')
    ])

@endsection
