@extends('apollocms::layout')
@section('title', __('Scaffold Collection'))

@section('content')
    <header class="mb-3">
        @include('apollocms::partials.breadcrumb', [
            'url' => cp_route('collections.show', $collection->handle()),
            'title' => $collection->title()
        ])
        <h1> {{ __('Scaffold Resources') }}</h1>
    </header>

    <collection-scaffolder
        title="{{ $collection->title() }}"
        handle="{{ $collection->handle() }}"
        route="{{ url()->current() }}" >
    </collection-scaffolder>
@stop
