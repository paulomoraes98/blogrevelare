<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
<meta name="robots" content="noindex,nofollow">

<title>@yield('title', $title ?? __('Here')) ‹ {{ ApolloCMS::pro() ? config('apollocms.cp.custom_cms_name', 'ApolloCMS') : 'ApolloCMS' }}</title>

@if (ApolloCMS::pro() && config('apollocms.cp.custom_favicon_url'))
    @include('apollocms::partials.favicon', ['favicon_url' => config('apollocms.cp.custom_favicon_url')])
@else
    <link rel="icon" type="image/png" href="{{ ApolloCMS::cpAssetUrl('img/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ ApolloCMS::cpAssetUrl('img/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ ApolloCMS::cpAssetUrl('img/favicon.ico') }}" sizes="16x16 32x32"/>
@endif

<link href="{{ ApolloCMS::cpAssetUrl('css/cp.css') }}?v={{ ApolloCMS::version() }}" rel="stylesheet" />

@if (ApolloCMS::pro() && config('apollocms.cp.custom_css_url'))
<link href="{{ config('apollocms.cp.custom_css_url') }}?v={{ ApolloCMS::version() }}" rel="stylesheet" />
@endif

@foreach (ApolloCMS::availableExternalStyles(request()) as $url)
    <link href="{{ $url }}" rel="stylesheet" />
@endforeach

@foreach (ApolloCMS::availableStyles(request()) as $package => $paths)
    @foreach ($paths as $path)
        <link href="{{ ApolloCMS::vendorAssetUrl("$package/css/$path") }}" rel="stylesheet" />
    @endforeach
@endforeach

@stack('head')
