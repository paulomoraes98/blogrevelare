<div class="logo pt-7">
    @if ($customLogo)
        <img src="{{ $customLogo }}" alt="{{ config('apollocms.cp.custom_cms_name') }}" class="white-label-logo">
    @else
        @cp_svg('apollocms-wordmark')
    @endif
</div>
