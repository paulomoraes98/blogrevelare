<script src="{{ ApolloCMS::cpAssetUrl('js/manifest.js') }}?v={{ ApolloCMS::version() }}"></script>
<script src="{{ ApolloCMS::cpAssetUrl('js/vendor.js') }}?v={{ ApolloCMS::version() }}"></script>
<script src="{{ ApolloCMS::cpAssetUrl('js/app.js') }}?v={{ ApolloCMS::version() }}"></script>

@foreach (ApolloCMS::availableExternalScripts(request()) as $url)
    <script src="{{ $url }}"></script>
@endforeach

@foreach (ApolloCMS::availableScripts(request()) as $package => $paths)
    @foreach ($paths as $path)
        <script src="{{ ApolloCMS::vendorAssetUrl("$package/js/$path") }}"></script>
    @endforeach
@endforeach

<script>
    ApolloCMS.config(@json(array_merge(ApolloCMS::jsonVariables(request()), [
        'wrapperClass' => $__env->getSection('wrapper_class', 'max-w-xl')
    ])));
    ApolloCMS.start();
</script>

