<html>
    <head>
        @include('apollocms::partials.head')
    </head>
    <body>
        <div id="apollocms">
            <live-preview-popout>
            </live-preview-popout>
        </div>

        @include('apollocms::partials.scripts')
    </body>
</html>
