@extends('apollocms::layout')
@section('title', __('Users'))
@section('wrapper_class', 'max-w-full')

@section('content')

    <header class="flex items-center mb-3">
        <h1 class="flex-1">
            {{ __('Users') }}
        </h1>

        @can('configure fields')
            <dropdown-list class="mr-1">
                <dropdown-item :text="__('Edit Blueprint')" redirect="{{ cp_route('users.blueprint.edit') }}"></dropdown-item>
            </dropdown-list>
        @endcan

        @if (ApolloCMS::pro() && $user->can('create', 'ApolloCMS\Contracts\Auth\User'))
            <a href="{{ cp_route('users.create') }}" class="btn-primary ml-2">{{ __('Create User') }}</a>
        @endif
    </header>

    <user-listing
        listing-key="users"
        initial-sort-column="email"
        initial-sort-direction="asc"
        :filters="{{ $filters->toJson() }}"
        run-action-url="{{ cp_route('users.actions.run') }}"
        bulk-actions-url="{{ cp_route('users.actions.bulk') }}"
    ></user-listing>

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Users'),
        'url' => ApolloCMS::docsUrl('users')
    ])

@endsection
