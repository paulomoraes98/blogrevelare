@extends('apollocms::layout')
@section('title', __('Edit Blueprint'))

@section('content')

    @include('apollocms::partials.breadcrumb', [
        'url' => cp_route('users.index'),
        'title' => __('Users'),
    ])

    <blueprint-builder
        action="{{ cp_route('users.blueprint.update') }}"
        :initial-blueprint="{{ json_encode($blueprintVueObject) }}"
    ></blueprint-builder>

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Blueprints'),
        'url' => ApolloCMS::docsUrl('blueprints')
    ])

@endsection
