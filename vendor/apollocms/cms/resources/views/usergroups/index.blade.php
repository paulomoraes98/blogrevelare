@extends('apollocms::layout')
@section('title', __('User Groups'))

@section('content')

    @unless($groups->isEmpty())

        <div class="flex mb-3">
            <h1 class="flex-1">
                {{ __('User Groups') }}
            </h1>
            <a href="{{ cp_route('user-groups.create') }}" class="btn-primary">{{ __('Create User Group') }}</a>
        </div>

        <user-group-listing :initial-rows="{{ json_encode($groups) }}"></user-group-listing>

    @else

        @include('apollocms::partials.empty-state', [
            'title' => __('User Groups'),
            'description' => __('apollocms::messages.user_groups_intro'),
            'svg' => 'empty/users',
            'button_text' => __('Create User Group'),
            'button_url' => cp_route('user-groups.create'),
        ])

    @endunless

    @include('apollocms::partials.docs-callout', [
        'topic' => __('User Groups'),
        'url' => ApolloCMS::docsUrl('users#user-groups')
    ])

@endsection
