@extends('apollocms::layout')
@section('title', __('Forms'))

@section('content')

    @unless($forms->isEmpty())

        <div class="flex items-center mb-3">
            <h1 class="flex-1">{{ __('Forms') }}</h1>

            @if (ApolloCMS::pro() && $user->can('create', 'ApolloCMS\Contracts\Forms\Form'))
                <a href="{{ cp_route('forms.create') }}" class="btn-primary">{{ __('Create Form') }}</a>
            @endif
        </div>

        <form-listing :forms="{{ json_encode($forms) }}"></form-listing>

    @else

        @include('apollocms::partials.empty-state', [
            'title' => __('Forms'),
            'description' => __('apollocms::messages.form_configure_intro'),
            'svg' => 'empty/form',
            'button_text' => __('Create Form'),
            'button_url' => cp_route('forms.create'),
            'can' => $user->can('create', 'ApolloCMS\Contracts\Forms\Form')
        ])

    @endunless

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Forms'),
        'url' => ApolloCMS::docsUrl('forms')
    ])

@endsection
