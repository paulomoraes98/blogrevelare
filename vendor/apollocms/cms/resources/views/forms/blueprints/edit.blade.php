@extends('apollocms::layout')
@section('title', __('Edit Blueprint'))

@section('content')

    @include('apollocms::partials.breadcrumb', [
        'url' => cp_route('forms.show', $form->handle()),
        'title' => $form->title(),
    ])

    <blueprint-builder
        action="{{ cp_route('forms.blueprint.update', $form->handle()) }}"
        :initial-blueprint="{{ json_encode($blueprintVueObject) }}"
        :use-sections="false"
        :is-form-blueprint="true"
    ></blueprint-builder>

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Blueprints'),
        'url' => ApolloCMS::docsUrl('blueprints')
    ])

@endsection
