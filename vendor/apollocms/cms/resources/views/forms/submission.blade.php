@extends('apollocms::layout')
@section('title', ApolloCMS::crumb('Submission ' . $submission->id(), $submission->form->title(), 'Forms'))

@section('content')

    @include('apollocms::partials.breadcrumb', [
        'url' => cp_route('forms.show', $submission->form->handle()),
        'title' => $submission->form->title()
    ])

    <publish-form
        title="{{ $title }}"
        :blueprint='@json($blueprint)'
        :meta='@json($meta)'
        :values='@json($values)'
        read-only
    ></publish-form>

@endsection
