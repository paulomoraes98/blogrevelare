@extends('apollocms::layout')
@section('title', __('Edit Blueprint'))

@section('content')

    @include('apollocms::partials.breadcrumb', [
        'url' => cp_route('taxonomies.blueprints.index', $taxonomy),
        'title' => __('Blueprints')
    ])

    <blueprint-builder
        show-title
        action="{{ cp_route('taxonomies.blueprints.update', [$taxonomy, $blueprint]) }}"
        :initial-blueprint="{{ json_encode($blueprintVueObject) }}"
    ></blueprint-builder>

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Blueprints'),
        'url' => ApolloCMS::docsUrl('blueprints')
    ])

@endsection
