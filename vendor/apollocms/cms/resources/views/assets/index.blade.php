@extends('apollocms::layout')
@section('title', __('Assets'))

@section('content')

    @include('apollocms::partials.empty-state', [
        'title' => __('Asset Containers'),
        'description' => __('apollocms::messages.asset_container_intro'),
        'svg' => 'empty/asset-container',
        'button_text' => __('Create Asset Container'),
        'button_url' => cp_route('asset-containers.create'),
        'can' => $user->can('create', \ApolloCMS\Contracts\Assets\AssetContainer::class)
    ])

    @include('apollocms::partials.docs-callout', [
        'topic' => __('Assets'),
        'url' => 'assets'
    ])

@endsection
