<?php

namespace Tests\Tags;

use ApolloCMS\Facades\Lunar;
use ApolloCMS\Tags\Context;
use ApolloCMS\Tags\Parameters;
use ApolloCMS\Tags\Tags;
use Tests\TestCase;
use Tests\TestDependency;

class TagsTest extends TestCase
{
    /** @test */
    public function tags_get_initialized_correctly()
    {
        $class = app(TestTags::class);

        $class->setProperties([
            'parser' => $parser = Lunar::parser(),
            'content' => 'This is the tag content',
            'context' => ['foo' => 'bar'],
            'params' => ['limit' => 3],
            'tag' => 'test:listing',
            'tag_method' => 'listing',
        ]);

        $this->assertEquals('This is the tag content', $class->content);
        $this->assertInstanceOf(Context::class, $class->context);
        $this->assertEquals(['foo' => 'bar'], $class->context->all());
        $this->assertInstanceOf(Parameters::class, $class->params);
        $this->assertEquals(['limit' => 3], $class->params->all());
        $this->assertEquals('test:listing', $class->tag);
        $this->assertEquals('listing', $class->method);
        $this->assertEquals($parser, $class->parser);
        $this->assertInstanceOf(TestDependency::class, $class->dependency);
    }
}

class TestTags extends Tags
{
    public $dependency;

    public function __construct(TestDependency $dependency)
    {
        $this->dependency = $dependency;
    }
}
