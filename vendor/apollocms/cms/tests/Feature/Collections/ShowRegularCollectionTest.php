<?php

namespace Tests\Feature\Collections;

use Facades\Tests\Factories\EntryFactory;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\User;
use Tests\PreventSavingApocheItemsToDisk;

class ShowRegularCollectionTest extends ShowCollectionTest
{
    use PreventSavingApocheItemsToDisk;

    public function createCollection($handle)
    {
        return tap(Collection::make('test'))->save();
    }

    /** @test */
    public function it_shows_the_entry_listing_page_if_you_have_permission()
    {
        $this->setTestRoles(['test' => ['access cp', 'view test entries']]);
        $user = tap(User::make()->assignRole('test'))->save();
        $collection = $this->createCollection('test');
        EntryFactory::id('1')->collection($collection)->create();

        $this
            ->actingAs($user)
            ->get($collection->showUrl())
            ->assertOk()
            ->assertViewIs('apollocms::collections.show')
            ->assertViewHas('collection', $collection);
    }
}
