<?php

namespace Tests\Feature\Collections;

use Facades\Tests\Factories\EntryFactory;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\User;
use Tests\PreventSavingApocheItemsToDisk;

class ShowStructuredCollectionTest extends ShowCollectionTest
{
    use PreventSavingApocheItemsToDisk;

    public function createCollection($handle)
    {
        $collection = tap(Collection::make('test')->structureContents(['max_depth' => 10]))->save();
        $collection->structure()->makeTree('en')->save();

        return $collection;
    }

    /** @test */
    public function it_shows_the_structure_tree_if_you_have_permission()
    {
        $this->withoutExceptionHandling();
        $this->setTestRoles(['test' => ['access cp', 'view test entries']]);
        $user = tap(User::make()->assignRole('test'))->save();
        $collection = $this->createCollection('test');
        EntryFactory::id('1')->collection($collection)->create();

        $this
            ->actingAs($user)
            ->get($collection->showUrl())
            ->assertOk()
            ->assertViewIs('apollocms::collections.show')
            ->assertViewHas('collection', $collection)
            ->assertViewHas('structure', $collection->structure());
    }
}
