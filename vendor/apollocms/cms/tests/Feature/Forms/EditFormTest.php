<?php

namespace Tests\Feature\Forms;

use ApolloCMS\Facades\Form;
use ApolloCMS\Facades\User;
use Tests\FakesRoles;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

class EditFormTest extends TestCase
{
    use FakesRoles;
    use PreventSavingApocheItemsToDisk;

    protected function resolveApplicationConfiguration($app)
    {
        parent::resolveApplicationConfiguration($app);

        $app['config']['apollocms.forms.forms'] = $this->fakeApocheDirectory.'/forms';
    }

    /** @test */
    public function it_shows_the_edit_page_if_you_have_permission()
    {
        $this->setTestRoles(['test' => ['access cp', 'configure forms']]);
        $user = User::make()->assignRole('test')->save();
        $form = tap(Form::make('test'))->save();

        $this
            ->actingAs($user)
            ->get(cp_route('forms.edit', $form->handle()))
            ->assertSuccessful()
            ->assertViewHas('form', $form);
    }

    /** @test */
    public function it_denies_access_if_you_dont_have_permission()
    {
        $this->setTestRoles(['test' => ['access cp']]);
        $user = tap(User::make()->assignRole('test'))->save();
        $form = tap(Form::make('test'))->save();

        $this
            ->from('/original')
            ->actingAs($user)
            ->get(cp_route('forms.edit', $form->handle()))
            ->assertRedirect('/original')
            ->assertSessionHas('error');
    }
}
