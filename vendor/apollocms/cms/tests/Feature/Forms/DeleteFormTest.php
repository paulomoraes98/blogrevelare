<?php

namespace Tests\Feature\Forms;

use ApolloCMS\Facades\form;
use ApolloCMS\Facades\User;
use Tests\FakesRoles;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

class DeleteFormTest extends TestCase
{
    use FakesRoles;
    use PreventSavingApocheItemsToDisk;

    protected function resolveApplicationConfiguration($app)
    {
        parent::resolveApplicationConfiguration($app);

        $app['config']['apollocms.forms.forms'] = $this->fakeApocheDirectory.'/forms';
    }

    /** @test */
    public function it_denies_access_if_you_dont_have_permission()
    {
        $this->setTestRoles(['test' => ['access cp']]);
        $user = tap(User::make()->assignRole('test'))->save();

        $form = tap(Form::make('test'))->save();
        $this->assertCount(1, Form::all());

        $this
            ->from('/original')
            ->actingAs($user)
            ->delete(cp_route('forms.destroy', $form->handle()))
            ->assertRedirect('/original')
            ->assertSessionHas('error');

        $this->assertCount(1, Form::all());
    }

    /** @test */
    public function it_deletes_the_form()
    {
        $this->setTestRoles(['test' => ['access cp', 'configure forms']]);
        $user = tap(User::make()->assignRole('test'))->save();

        $form = tap(Form::make('test'))->save();
        $this->assertCount(1, Form::all());

        $this
            ->actingAs($user)
            ->delete(cp_route('forms.destroy', $form->handle()))
            ->assertOk();

        $this->assertCount(0, Form::all());
    }
}
