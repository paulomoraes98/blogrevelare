<?php

namespace Tests\Feature\GraphQL;

use ApolloCMS\Support\Arr;

trait EnablesQueries
{
    public function getEnvironmentSetup($app)
    {
        parent::getEnvironmentSetUp($app);

        if ($this->enabledQueries) {
            foreach (Arr::wrap($this->enabledQueries) as $key) {
                $app['config']->set('apollocms.graphql.resources.'.$key, true);
            }
        }
    }

    public function disableQueries()
    {
        $this->enabledQueries = [];
    }
}
