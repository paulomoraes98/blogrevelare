<?php

namespace Tests\Feature\GraphQL\Fieldtypes;

use Facades\ApolloCMS\Fields\BlueprintRepository;
use Facades\Tests\Factories\EntryFactory;
use ApolloCMS\Facades\Blueprint;
use Tests\Feature\GraphQL\EnablesQueries;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

abstract class FieldtypeTestCase extends TestCase
{
    use PreventSavingApocheItemsToDisk;
    use EnablesQueries;

    protected $enabledQueries = ['collections'];

    public function setUp(): void
    {
        parent::setUp();
        BlueprintRepository::partialMock();
    }

    protected function createEntryWithFields($arr)
    {
        $entry = EntryFactory::collection('test')->id('1')->data(
            collect($arr)->map->value->all()
        )->create();

        $blueprint = Blueprint::makeFromFields(
            collect($arr)->map->field->all()
        );

        BlueprintRepository::shouldReceive('in')->with('collections/test')->andReturn(collect([
            'blueprint' => $blueprint->setHandle('blueprint'),
        ]));

        return $entry;
    }

    protected function assertGqlEntryHas($query, $expected)
    {
        $query = <<<GQL
{
    entry(id: "1") {
        ... on Entry_Test_Blueprint {
            $query
        }
    }
}
GQL;

        $this
            ->withoutExceptionHandling()
            ->post('/graphql', ['query' => $query])
            ->assertGqlOk()
            ->assertExactJson(['data' => [
                'entry' => $expected,
            ]]);
    }
}
