<?php

namespace Tests\Feature\Users;

use Illuminate\Support\Facades\Event;
use ApolloCMS\Events\UserRegistered;
use ApolloCMS\Events\UserRegistering;
use ApolloCMS\Facades\User;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

class UserRegistrationTest extends TestCase
{
    use PreventSavingApocheItemsToDisk;

    /** @test */
    public function events_dispatched_when_user_registered()
    {
        Event::fake();

        $this
            ->post(route('apollocms.register'), ['email'=>'foo@bar.com', 'password'=>'password', 'password_confirmation'=>'password'])
            ->assertRedirect();

        Event::assertDispatched(UserRegistering::class);
        Event::assertDispatched(UserRegistered::class);
    }

    /** @test */
    public function user_not_saved_when_user_registration_returns_false()
    {
        Event::fake([UserRegistered::class]);

        Event::listen(UserRegistering::class, function () {
            return false;
        });

        $this
            ->post(route('apollocms.register'), ['email'=>'foo@bar.com', 'password'=>'password', 'password_confirmation'=>'password'])
            ->assertRedirect();

        $this->assertNull(User::findByEmail('foo@bar.com'));
        Event::assertNotDispatched(UserRegistered::class);
    }
}
