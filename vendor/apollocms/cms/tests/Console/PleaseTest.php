<?php

namespace Tests\Console;

use ApolloCMS\Console\Please\Kernel;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Tests\TestCase;

class PleaseTest extends TestCase
{
    /** @test */
    public function it_can_run_an_artisan_command_with_apollocms_prefix()
    {
        $this->artisan('apollocms:static:clear');

        $this->expectException(CommandNotFoundException::class);
        $this->artisan('static:clear');
    }

    /** @test */
    public function it_can_run_a_please_command_without_apollocms_prefix()
    {
        $this->please('static:clear');

        $this->expectException(CommandNotFoundException::class);
        $this->please('apollocms:static:clear');
    }

    public function please($command, $parameters = [])
    {
        return $this->app[Kernel::class]->call($command, $parameters);
    }
}
