<?php

namespace Tests;

use Illuminate\Support\Collection;
use ApolloCMS\Auth\File\Role as FileRole;
use ApolloCMS\Auth\File\RoleRepository;
use ApolloCMS\Contracts\Auth\RoleRepository as RepositoryContract;
use ApolloCMS\Facades\Role;

trait FakesRoles
{
    protected function setTestRoles($roles)
    {
        $roles = collect($roles)
            ->mapWithKeys(function ($permissions, $handle) {
                $handle = is_string($permissions) ? $permissions : $handle;
                $permissions = is_string($permissions) ? [] : $permissions;

                return [$handle => $permissions];
            })
            ->map(function ($permissions, $handle) {
                return $permissions instanceof FileRole
                    ? $permissions->handle($handle)
                    : Role::make()->handle($handle)->addPermission($permissions);
            });

        $fake = new class($roles) extends RoleRepository {
            protected $roles;

            public function __construct($roles)
            {
                $this->roles = $roles;
            }

            public function all(): Collection
            {
                return $this->roles;
            }
        };

        app()->instance(RepositoryContract::class, $fake);
        Role::swap($fake);
    }
}
