<?php

namespace Tests\GraphQL;

use Facades\ApolloCMS\Fields\BlueprintRepository;
use Illuminate\Support\Facades\Storage;
use Rebing\GraphQL\Support\Facades\GraphQL;
use ApolloCMS\Facades\AssetContainer;
use ApolloCMS\Fields\Blueprint;
use ApolloCMS\GraphQL\Types\AssetInterface;
use ApolloCMS\GraphQL\Types\AssetType;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

/** @group graphql */
class AssetInterfaceTest extends TestCase
{
    use PreventSavingApocheItemsToDisk;

    /** @test */
    public function it_adds_types()
    {
        GraphQL::spy();

        tap(Storage::fake('test'))->getDriver()->getConfig()->set('url', '/assets');
        AssetContainer::make('one')->disk('test')->save();
        AssetContainer::make('two')->disk('test')->save();
        $one = tap($this->partialMock(Blueprint::class), function ($m) {
            $m->shouldReceive('handle')->andReturn('article');
            $m->shouldReceive('addGqlTypes')->once();
        });
        $two = tap($this->partialMock(Blueprint::class), function ($m) {
            $m->shouldReceive('handle')->andReturn('art_directed');
            $m->shouldReceive('addGqlTypes')->once();
        });
        BlueprintRepository::shouldReceive('find')->with('assets/one')->andReturn($one);
        BlueprintRepository::shouldReceive('find')->with('assets/two')->andReturn($two);

        AssetInterface::addTypes();

        GraphQL::shouldHaveReceived('addType')->with(AssetInterface::class)->once();
        GraphQL::shouldHaveReceived('addTypes')->withArgs(function ($args) {
            $this->assertEveryItemIsInstanceOf(AssetType::class, $args);
            $this->assertEquals($expected = [
                'Asset_One',
                'Asset_Two',
            ], $actual = collect($args)->map->name->all());

            return $actual === $expected;
        });
    }
}
