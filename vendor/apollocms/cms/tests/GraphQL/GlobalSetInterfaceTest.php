<?php

namespace Tests\GraphQL;

use Facades\ApolloCMS\Fields\BlueprintRepository;
use Facades\Tests\Factories\GlobalFactory;
use Rebing\GraphQL\Support\Facades\GraphQL;
use ApolloCMS\Fields\Blueprint;
use ApolloCMS\GraphQL\Types\GlobalSetInterface;
use ApolloCMS\GraphQL\Types\GlobalSetType;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

/** @group graphql */
class GlobalSetInterfaceTest extends TestCase
{
    use PreventSavingApocheItemsToDisk;

    /** @test */
    public function it_adds_types()
    {
        GraphQL::spy();

        GlobalFactory::handle('social_media')->create();
        GlobalFactory::handle('company_details')->create();
        GlobalFactory::handle('without_blueprint')->create();
        $social = tap($this->partialMock(Blueprint::class), function ($m) {
            $m->shouldReceive('handle')->andReturn('article');
            $m->shouldReceive('addGqlTypes')->once();
        });
        $company = tap($this->partialMock(Blueprint::class), function ($m) {
            $m->shouldReceive('handle')->andReturn('art_directed');
            $m->shouldReceive('addGqlTypes')->once();
        });
        BlueprintRepository::shouldReceive('find')->with('globals.social_media')->andReturn($social);
        BlueprintRepository::shouldReceive('find')->with('globals.company_details')->andReturn($company);
        BlueprintRepository::shouldReceive('find')->with('globals.without_blueprint')->andReturnNull();

        GlobalSetInterface::addTypes();

        GraphQL::shouldHaveReceived('addType')->with(GlobalSetInterface::class)->once();
        GraphQL::shouldHaveReceived('addTypes')->withArgs(function ($args) {
            $this->assertEveryItemIsInstanceOf(GlobalSetType::class, $args);
            $this->assertEquals($expected = [
                'GlobalSet_SocialMedia',
                'GlobalSet_CompanyDetails',
                'GlobalSet_WithoutBlueprint',
            ], $actual = collect($args)->map->name->all());

            return $actual === $expected;
        });
    }
}
