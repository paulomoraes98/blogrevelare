<?php

namespace Tests\Modifiers;

use ApolloCMS\Modifiers\Modify;
use Tests\TestCase;

class RemoveQueryParamTest extends TestCase
{
    protected $baseUrl = 'https://www.google.com/search';
    protected $queryParamKey = 'q';

    /** @test */
    public function it_removes_an_existing_query_param()
    {
        $this->assertSame($this->baseUrl, $this->modify("{$this->baseUrl}?q=apollocms", $this->queryParamKey));
        $this->assertSame("{$this->baseUrl}#test", $this->modify("{$this->baseUrl}?q=apollocms#test", $this->queryParamKey));
        $this->assertSame("{$this->baseUrl}?sourceid=chrome", $this->modify("{$this->baseUrl}?q=apollocms&sourceid=chrome", $this->queryParamKey));
        $this->assertSame("{$this->baseUrl}?sourceid=chrome", $this->modify("{$this->baseUrl}?sourceid=chrome&q=apollocms", $this->queryParamKey));
    }

    /** @test */
    public function it_does_nothing_if_the_query_param_key_does_not_exist()
    {
        $this->assertSame($this->baseUrl, $this->modify($this->baseUrl, $this->queryParamKey));
        $this->assertSame("{$this->baseUrl}#test", $this->modify("{$this->baseUrl}#test", $this->queryParamKey));
        $this->assertSame("{$this->baseUrl}?sourceid=chrome", $this->modify("{$this->baseUrl}?sourceid=chrome", $this->queryParamKey));
    }

    /** @test */
    public function it_does_nothing_if_no_parameters_are_passed()
    {
        $this->assertSame($this->baseUrl, $this->modify($this->baseUrl));
    }

    private function modify(string $url, ?string $queryParamKey = null)
    {
        if (is_null($queryParamKey)) {
            return Modify::value($url)->removeQueryParam()->fetch();
        }

        return Modify::value($url)->removeQueryParam($queryParamKey)->fetch();
    }
}
