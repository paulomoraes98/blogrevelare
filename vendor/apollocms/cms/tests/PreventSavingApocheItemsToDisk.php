<?php

namespace Tests;

use ApolloCMS\Facades\Path;
use ApolloCMS\Facades\Apoche;

trait PreventSavingApocheItemsToDisk
{
    protected $fakeApocheDirectory = __DIR__.'/__fixtures__/dev-null';

    protected function preventSavingApocheItemsToDisk()
    {
        $this->fakeApocheDirectory = Path::tidy($this->fakeApocheDirectory);

        Apoche::stores()->each(function ($store) {
            $dir = Path::tidy(__DIR__.'/__fixtures__');
            $relative = str_after(str_after($store->directory(), $dir), '/');
            $store->directory($this->fakeApocheDirectory.'/'.$relative);
        });
    }

    protected function deleteFakeApocheDirectory()
    {
        app('files')->deleteDirectory($this->fakeApocheDirectory);

        mkdir($this->fakeApocheDirectory);
        touch($this->fakeApocheDirectory.'/.gitkeep');
    }
}
