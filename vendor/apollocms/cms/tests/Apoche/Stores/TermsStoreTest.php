<?php

namespace Tests\Apoche\Stores;

use ApolloCMS\Facades;
use ApolloCMS\Facades\Apoche;
use ApolloCMS\Apoche\Stores\TermsStore;
use Tests\PreventSavingApocheItemsToDisk;
use Tests\TestCase;

class TermsStoreTest extends TestCase
{
    use PreventSavingApocheItemsToDisk;

    public function setUp(): void
    {
        parent::setUp();

        $this->parent = (new TermsStore)->directory(
            $this->directory = __DIR__.'/../__fixtures__/content/taxonomies'
        );

        Apoche::registerStore($this->parent);

        Apoche::store('taxonomies')->directory($this->directory);
    }

    /** @test */
    public function it_saves_to_disk()
    {
        $term = Facades\Term::make('test')->taxonomy('tags');
        $term->in('en')->set('title', 'Test');

        $this->parent->store('tags')->save($term);

        $this->assertFileEqualsString($path = $this->directory.'/tags/test.yaml', $term->fileContents());
        @unlink($path);
        $this->assertFileNotExists($path);

        $this->assertEquals('en::'.$path, $this->parent->store('tags')->paths()->get('en::test'));
    }
}
