<?php

namespace Tests\Apoche;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Stores\ChildStore;
use ApolloCMS\Apoche\Stores\CollectionsStore;
use ApolloCMS\Apoche\Stores\EntriesStore;

class ApocheTest extends TestCase
{
    public function setUp(): void
    {
        $this->apoche = new Apoche;
    }

    /** @test */
    public function sites_can_be_defined_and_retrieved()
    {
        $this->assertNull($this->apoche->sites());

        $return = $this->apoche->sites(['one', 'two']);

        $this->assertEquals($this->apoche, $return);
        $this->assertInstanceOf(Collection::class, $this->apoche->sites());
        $this->assertEquals(['one', 'two'], $this->apoche->sites()->all());
    }

    /** @test */
    public function default_site_can_be_retrieved()
    {
        $this->apoche->sites(['foo', 'bar']);

        $this->assertEquals('foo', $this->apoche->defaultSite());
    }

    /** @test */
    public function stores_can_be_registered()
    {
        $this->apoche->sites(['en']); // store expects the apoche to have site(s)
        $this->assertTrue($this->apoche->stores()->isEmpty());

        $return = $this->apoche->registerStore(
            new CollectionsStore($this->apoche, \Mockery::mock(Filesystem::class))
        );

        $this->assertEquals($this->apoche, $return);
        tap($this->apoche->stores(), function ($stores) {
            $this->assertEquals(1, $stores->count());
            $this->assertEquals('collections', $stores->keys()->first());
            $this->assertInstanceOf(CollectionsStore::class, $stores->first());
            $this->assertInstanceOf(CollectionsStore::class, $this->apoche->store('collections'));
        });
    }

    /** @test */
    public function multiple_stores_can_be_registered_at_once()
    {
        $this->apoche->sites(['en']); // store expects the apoche to have site(s)
        $this->assertTrue($this->apoche->stores()->isEmpty());

        $return = $this->apoche->registerStores([
            new CollectionsStore($this->apoche, \Mockery::mock(Filesystem::class)),
            new EntriesStore($this->apoche, \Mockery::mock(Filesystem::class)),
        ]);

        $this->assertEquals($this->apoche, $return);
        tap($this->apoche->stores(), function ($stores) {
            $this->assertEquals(2, $stores->count());
            $this->assertEquals(['collections', 'entries'], $stores->keys()->all());
            $this->assertInstanceOf(CollectionsStore::class, $stores['collections']);
            $this->assertInstanceOf(EntriesStore::class, $stores['entries']);
            $this->assertInstanceOf(CollectionsStore::class, $this->apoche->store('collections'));
            $this->assertInstanceOf(EntriesStore::class, $this->apoche->store('entries'));
        });
    }

    /** @test */
    public function an_aggregate_stores_child_store_can_be_retrieved_directly()
    {
        $this->apoche->sites(['en']); // stores expect the apoche to have site(s)
        $store = (new EntriesStore($this->apoche, \Mockery::mock(Filesystem::class)))->setChildStoreCreator(function () {
            return new ChildStore($this->apoche, \Mockery::mock(Filesystem::class));
        });
        $one = $store->store('one');
        $two = $store->store('two');
        $this->apoche->registerStore($store);

        $this->assertEquals($one, $this->apoche->store('entries::one'));
        $this->assertEquals($two, $this->apoche->store('entries::two'));
    }

    /** @test */
    public function it_generates_an_id()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_clears_its_cache()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_refreshes_itself()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_gets_its_cache_file_size()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_can_record_its_build_time()
    {
        $this->markTestIncomplete();
    }
}
