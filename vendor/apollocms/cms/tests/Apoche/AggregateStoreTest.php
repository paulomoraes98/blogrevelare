<?php

namespace Tests\Apoche;

use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Stores\AggregateStore;
use ApolloCMS\Apoche\Stores\ChildStore;
use Tests\TestCase;

class AggregateStoreTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $apoche = (new Apoche)->sites(['en', 'fr']);

        $this->app->instance(Apoche::class, $apoche);

        $this->store = new TestAggregateStore;
    }

    /** @test */
    public function it_gets_and_sets_child_stores()
    {
        $this->assertEquals([], $this->store->stores()->all());

        $childOne = $this->store->store('one');
        $childTwo = $this->store->store('two');

        $this->assertInstanceOf(ChildStore::class, $childOne);
        $this->assertEquals(['one' => $childOne, 'two' => $childTwo], $this->store->stores()->all());
    }
}

class TestAggregateStore extends AggregateStore
{
    protected $childStore = TestChildStore::class;

    public function key()
    {
        return 'test';
    }

    public function discoverStores()
    {
        //
    }
}

class TestChildStore extends ChildStore
{
}
