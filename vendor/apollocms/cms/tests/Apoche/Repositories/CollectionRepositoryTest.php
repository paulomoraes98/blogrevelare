<?php

namespace Tests\Apoche\Repositories;

use Illuminate\Support\Collection as IlluminateCollection;
use ApolloCMS\Entries\Collection;
use ApolloCMS\Facades\Collection as CollectionAPI;
use ApolloCMS\Apoche\Repositories\CollectionRepository;
use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Stores\CollectionsStore;
use ApolloCMS\Apoche\Stores\EntriesStore;
use ApolloCMS\Apoche\Stores\NavigationStore;
use Tests\TestCase;

class CollectionRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $apoche = (new Apoche)->sites(['en', 'fr']);
        $this->app->instance(Apoche::class, $apoche);
        $this->directory = __DIR__.'/../__fixtures__/content/collections';
        $apoche->registerStores([
            (new CollectionsStore($apoche, app('files')))->directory($this->directory),
            (new EntriesStore($apoche, app('files')))->directory($this->directory),
            (new NavigationStore($apoche, app('files')))->directory(__DIR__.'/../__fixtures__/content/navigation'),
        ]);

        $this->repo = new CollectionRepository($apoche);
    }

    /** @test */
    public function it_gets_all_collections()
    {
        $collections = $this->repo->all();

        $this->assertInstanceOf(IlluminateCollection::class, $collections);
        $this->assertCount(4, $collections);
        $this->assertEveryItemIsInstanceOf(Collection::class, $collections);

        $ordered = $collections->sortBy->handle()->values();
        $this->assertEquals(['alphabetical', 'blog', 'numeric', 'pages'], $ordered->map->handle()->all());
        $this->assertEquals(['Alphabetical', 'Blog', 'Numeric', 'Pages'], $ordered->map->title()->all());
    }

    /** @test */
    public function it_gets_a_collection_by_handle()
    {
        tap($this->repo->findByHandle('alphabetical'), function ($collection) {
            $this->assertInstanceOf(Collection::class, $collection);
            $this->assertEquals('alphabetical', $collection->handle());
            $this->assertEquals('Alphabetical', $collection->title());
        });

        tap($this->repo->findByHandle('blog'), function ($collection) {
            $this->assertInstanceOf(Collection::class, $collection);
            $this->assertEquals('blog', $collection->handle());
            $this->assertEquals('Blog', $collection->title());
        });

        tap($this->repo->findByHandle('numeric'), function ($collection) {
            $this->assertInstanceOf(Collection::class, $collection);
            $this->assertEquals('numeric', $collection->handle());
            $this->assertEquals('Numeric', $collection->title());
        });

        tap($this->repo->findByHandle('pages'), function ($collection) {
            $this->assertInstanceOf(Collection::class, $collection);
            $this->assertEquals('pages', $collection->handle());
            $this->assertEquals('Pages', $collection->title());
        });

        $this->assertNull($this->repo->findByHandle('unknown'));
    }

    /** @test */
    public function it_saves_a_collection_to_the_apoche_and_to_a_file()
    {
        $collection = CollectionAPI::make('new');
        $collection->cascade(['foo' => 'bar']);
        $this->assertNull($this->repo->findByHandle('new'));

        $this->repo->save($collection);

        $this->assertNotNull($item = $this->repo->findByHandle('new'));
        $this->assertEquals(['foo' => 'bar'], $item->cascade()->all());
        $this->assertTrue(file_exists($this->directory.'/new.yaml'));
        @unlink($this->directory.'/new.yaml');
    }
}
