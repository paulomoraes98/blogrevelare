<?php

namespace Tests\Apoche\Repositories;

use ApolloCMS\Contracts\Structures\Tree;
use ApolloCMS\Apoche\Repositories\NavTreeRepository;
use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Stores\NavTreeStore;
use Tests\TestCase;

class NavTreeRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $apoche = (new Apoche)->sites(['en', 'fr']);
        $this->app->instance(Apoche::class, $apoche);
        $this->store = $this->mock(NavTreeStore::class)
            ->shouldReceive('key')->andReturn('nav-trees')
            ->getMock();
        $apoche->registerStores([$this->store]);
        $this->repo = new NavTreeRepository($apoche);
    }

    /** @test */
    public function it_gets_a_nav_tree()
    {
        $this->store
            ->shouldReceive('getItem')
            ->with('links::en')
            ->andReturn($tree = $this->mock(Tree::class));

        $this->assertSame($tree, $this->repo->find('links', 'en'));
    }

    /** @test */
    public function it_saves_a_nav_tree_through_the_store()
    {
        $tree = $this->mock(Tree::class);
        $this->store->shouldReceive('save')->with($tree)->once();

        $this->assertTrue($this->repo->save($tree));
    }
}
