<?php

namespace Tests\Apoche\Repositories;

use Illuminate\Support\Collection as IlluminateCollection;
use ApolloCMS\Facades\Collection;
use ApolloCMS\Facades\Taxonomy as TaxonomyAPI;
use ApolloCMS\Apoche\Repositories\TaxonomyRepository;
use ApolloCMS\Apoche\Apoche;
use ApolloCMS\Apoche\Stores\CollectionsStore;
use ApolloCMS\Apoche\Stores\TaxonomiesStore;
use ApolloCMS\Taxonomies\Taxonomy;
use Tests\TestCase;

class TaxonomyRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $apoche = (new Apoche)->sites(['en', 'fr']);
        $this->app->instance(Apoche::class, $apoche);
        $this->directory = __DIR__.'/../__fixtures__/content/taxonomies';
        $apoche->registerStores([
            (new TaxonomiesStore($apoche, app('files')))->directory($this->directory),
            (new CollectionsStore($apoche, app('files')))->directory(__DIR__.'/../__fixtures__/content/collections'),
        ]);

        $this->repo = new TaxonomyRepository($apoche);
    }

    /** @test */
    public function it_gets_all_taxonomies()
    {
        $taxonomies = $this->repo->all();

        $this->assertInstanceOf(IlluminateCollection::class, $taxonomies);
        $this->assertCount(2, $taxonomies);
        $this->assertEveryItemIsInstanceOf(Taxonomy::class, $taxonomies);

        $ordered = $taxonomies->sortBy->handle()->values();
        $this->assertEquals(['categories', 'tags'], $ordered->map->handle()->all());
        $this->assertEquals(['Categories', 'Tags'], $ordered->map->title()->all());
    }

    /** @test */
    public function it_gets_a_taxonomy_by_handle()
    {
        tap($this->repo->findByHandle('categories'), function ($taxonomy) {
            $this->assertInstanceOf(Taxonomy::class, $taxonomy);
            $this->assertEquals('categories', $taxonomy->handle());
            $this->assertEquals('Categories', $taxonomy->title());
        });

        tap($this->repo->findByHandle('tags'), function ($taxonomy) {
            $this->assertInstanceOf(Taxonomy::class, $taxonomy);
            $this->assertEquals('tags', $taxonomy->handle());
            $this->assertEquals('Tags', $taxonomy->title());
        });

        $this->assertNull($this->repo->findByHandle('unknown'));
    }

    /** @test */
    public function it_gets_a_taxonomy_by_uri()
    {
        tap($this->repo->findByUri('/categories'), function ($taxonomy) {
            $this->assertInstanceOf(Taxonomy::class, $taxonomy);
            $this->assertEquals('categories', $taxonomy->handle());
            $this->assertEquals('Categories', $taxonomy->title());
            $this->assertNull($taxonomy->collection());
        });
    }

    /** @test */
    public function it_gets_a_taxonomy_by_uri_with_collection()
    {
        tap($this->repo->findByUri('/blog/categories'), function ($taxonomy) {
            $this->assertInstanceOf(Taxonomy::class, $taxonomy);
            $this->assertEquals('categories', $taxonomy->handle());
            $this->assertEquals('Categories', $taxonomy->title());
            $this->assertEquals(Collection::findByHandle('blog'), $taxonomy->collection());
        });
    }

    /** @test */
    public function it_saves_a_taxonomy_to_the_apoche_and_to_a_file()
    {
        $taxonomy = TaxonomyAPI::make('new');
        $taxonomy->cascade(['foo' => 'bar']);
        $this->assertNull($this->repo->findByHandle('new'));

        $this->repo->save($taxonomy);

        $this->assertNotNull($item = $this->repo->findByHandle('new'));
        $this->assertEquals(['foo' => 'bar'], $item->cascade()->all());
        $this->assertTrue(file_exists($this->directory.'/new.yaml'));
        @unlink($this->directory.'/new.yaml');
    }
}
