<?php

use Illuminate\Support\Facades\Route;
use ApolloCMS\Captcha\CaptchaController;

if (config('captcha.enable_api_routes')) {
    Route::get(config('apollocms.routes.action').'/captcha/sitekey', [CaptchaController::class, 'sitekey']);
}
