<?php

namespace ApolloCMS\Captcha\Listeners;

use ApolloCMS\Captcha\Captcha;
use Illuminate\Validation\ValidationException;
use ApolloCMS\Entries\Entry;
use ApolloCMS\Events\EntrySaving;
use ApolloCMS\ApolloCMS;

class ValidateEntry
{
    protected $captcha;

    public function __construct(Captcha $captcha)
    {
        $this->captcha = $captcha;
    }

    public function handle(EntrySaving $event)
    {
        /** @var Entry */
        $entry = $event->entry;

        if (! $this->shouldVerify($entry)) {
            return $entry;
        }

        if ($this->captcha->verify()->invalidResponse()) {
            throw ValidationException::withMessages(['captcha' => config('captcha.error_message')]);
        }

        return $entry;
    }

    protected function shouldVerify(Entry $entry)
    {
        if (ApolloCMS::isCpRoute()) {
            return false;
        }

        if (! $config = $this->getCollectionConfig($entry->collectionHandle())) {
            return false;
        }

        $skipFields = array_merge(['_token'], $config['skip_fields'] ?? []);

        return ! empty(request()->except($skipFields));
    }

    protected function getCollectionConfig(string $handle)
    {
        $configs = collect(config('captcha.collections', []))->mapWithKeys(function ($val, $key) {
            return is_numeric($key) ? [$val => true] : [$key => $val];
        });

        if (! $config = $configs->get($handle)) {
            return null;
        }

        return is_array($config) ? $config : $handle;
    }
}
