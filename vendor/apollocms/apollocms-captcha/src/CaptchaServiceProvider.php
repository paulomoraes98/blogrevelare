<?php

namespace ApolloCMS\Captcha;

use ApolloCMS\Captcha\Listeners\ValidateEntry;
use ApolloCMS\Captcha\Listeners\ValidateFormSubmission;
use ApolloCMS\Events\EntrySaving;
use ApolloCMS\Events\FormSubmitted;
use ApolloCMS\Providers\AddonServiceProvider;

class CaptchaServiceProvider extends AddonServiceProvider
{
    protected $viewNamespace = 'captcha';

    protected $tags = [
       CaptchaTags::class,
    ];

    protected $listen = [
        FormSubmitted::class => [ValidateFormSubmission::class],
        EntrySaving::class => [ValidateEntry::class],
    ];

    protected $routes = [
        'web' => __DIR__.'/../routes/web.php',
    ];

    public function register()
    {
        $this->app->bind(Captcha::class, function () {
            $service = config('captcha.service');
            $class = "ApolloCMS\\Captcha\\{$service}";

            throw_unless(class_exists($class), new \Exception('Invalid Captcha service.'));

            return new $class;
        });
    }
}
