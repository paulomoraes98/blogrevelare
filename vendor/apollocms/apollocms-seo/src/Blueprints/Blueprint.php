<?php

namespace ApolloCMS\AposeoSeo\Blueprints;

interface Blueprint
{
    /**
     * Return an instance of a blueprint, populated with fields
     *
     * @return ApolloCMS\Facades\Blueprint
     */
    public static function requestBlueprint();
}
