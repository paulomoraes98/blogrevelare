<?php

namespace ApolloCMS\AposeoSeo\Blueprints\CP;

use ApolloCMS\AposeoSeo\Blueprints\Blueprint as AposeoBlueprint;
use ApolloCMS\Facades\Blueprint as ApolloCMSBlueprint;

class SitemapSettingsBlueprint implements AposeoBlueprint
{
    /**
     * @inheritDoc
     */
    public static function requestBlueprint()
    {
        return ApolloCMSBlueprint::make()->setContents([
            'sections' => [
                'main' => [
                    'fields' => [
                        [
                            'handle' => 'enable_sitemap',
                            'field' => [
                                'type' => 'toggle',
                                'display' => __('apollocms-seo::sitemap.fields.enable_sitemap.display'),
                                'default' => true,
                            ],
                        ],
                        [
                            'handle' => 'sitemap_cache_expiration',
                            'field' => [
                                'type' => 'select',
                                'display' => __('apollocms-seo::sitemap.fields.sitemap_cache_expiration.display'),
                                'instructions' => __('apollocms-seo::sitemap.fields.sitemap_cache_expiration.instruct'),
                                'default' => '180',
                                'options' => [
                                    'null' => 'Forever',
                                    '60' => '1 Hour',
                                    '180' => '3 Hours',
                                    '720' => '12 Hours',
                                    '1140' => '1 Day',
                                    '10080' => '1 Week',
                                    '40320' => '1 Month',
                                    '120960' => '3 Months',
                                    '483840' => '1 Year',
                                ],
                            ],
                        ],
                        [
                            'handle' => 'exclude_content_section',
                            'field' => [
                                'type' => 'section',
                                'display' => __('apollocms-seo::sitemap.fields.exclude_content_section.display'),
                                'listable' => 'hidden',
                            ],
                        ],
                        [
                            'handle' => 'exclude_collections',
                            'field' => [
                                'type' => 'collections',
                                'display' => __('apollocms-seo::sitemap.fields.exclude_collections.display'),
                                'instructions' => __('apollocms-seo::sitemap.fields.exclude_collections.instruct'),
                                'width' => 50,
                            ],
                        ],
                        [
                            'handle' => 'exclude_taxonomies',
                            'field' => [
                                'type' => 'taxonomies',
                                'display' => __('apollocms-seo::sitemap.fields.exclude_taxonomies.display'),
                                'instructions' => __('apollocms-seo::sitemap.fields.exclude_taxonomies.instruct'),
                                'width' => 50,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
