<?php

namespace ApolloCMS\AposeoSeo\Blueprints\CP;

use ApolloCMS\AposeoSeo\Blueprints\Blueprint as AposeoBlueprint;
use ApolloCMS\Facades\Blueprint as ApolloCMSBlueprint;

class MarketingSettingsBlueprint implements AposeoBlueprint
{
    /**
     * @inheritDoc
     */
    public static function requestBlueprint()
    {
        return ApolloCMSBlueprint::make()->setContents([
            'sections' => [
                'main' => [
                    'fields' => [
                        [
                            'handle' => 'gtm_section',
                            'field' => [
                                'type' => 'section',
                                'listable' => 'hidden',
                                'display' => __('apollocms-seo::marketing.fields.gtm_section.display'),
                                'instructions' => __('apollocms-seo::marketing.fields.gtm_section.instruct'),
                            ],
                        ],
                        [
                            'handle' => 'enable_gtm_script',
                            'field' => [
                                'type' => 'toggle',
                                'display' => __('apollocms-seo::marketing.fields.enable_gtm_script.display'),
                                'instructions' => __('apollocms-seo::marketing.fields.enable_gtm_script.instruct'),
                                'width' => 33,
                            ],
                        ],
                        [
                            'handle' => 'gtm_identifier',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::marketing.fields.gtm_identifier.display'),
                                'instructions' => __('apollocms-seo::marketing.fields.gtm_identifier.instruct'),
                                'width' => 66,
                                'if' => [
                                    'enable_gtm_script' => 'equals true',
                                ],
                            ],
                        ],
                        [
                            'handle' => 'site_verification_section',
                            'field' => [
                                'type' => 'section',
                                'display' => __('apollocms-seo::marketing.fields.site_verification_section.display'),
                                'listable' => 'hidden',
                            ],
                        ],
                        [
                            'handle' => 'google_verification_code',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::marketing.fields.google_verification_code.display'),
                                'width' => 50,
                            ],
                        ],
                        [
                            'handle' => 'bing_verification_code',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::marketing.fields.bing_verification_code.display'),
                                'width' => 50,
                            ],
                        ],
                        [
                            'handle' => 'yandex_verification_code',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::marketing.fields.yandex_verification_code.display'),
                                'width' => 50,
                            ],
                        ],
                        [
                            'handle' => 'baidu_verification_code',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::marketing.fields.baidu_verification_code.display'),
                                'width' => 50,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
