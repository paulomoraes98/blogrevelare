<?php

namespace ApolloCMS\AposeoSeo\Blueprints\CP\Redirects;

use ApolloCMS\AposeoSeo\Blueprints\Blueprint as AposeoBlueprint;
use ApolloCMS\Facades\Blueprint as ApolloCMSBlueprint;

class RedirectBlueprint implements AposeoBlueprint
{
    /**
     * @inheritDoc
     */
    public static function requestBlueprint()
    {
        return ApolloCMSBlueprint::make()->setContents([
            'sections' => [
                'main' => [
                    'fields' => [
                        [
                            'handle' => 'source_url',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::redirects.redirect.source_url'),
                            ],
                        ],
                        [
                            'handle' => 'target_url',
                            'field' => [
                                'type' => 'text',
                                'display' => __('apollocms-seo::redirects.redirect.target_url'),
                            ],
                        ],
                        [
                            'handle' => 'status_code',
                            'field' => [
                                'type' => 'radio',
                                'inline' => true,
                                'options' => [
                                    '301',
                                    '302',
                                ],
                                'display' => __('apollocms-seo::redirects.redirect.status_code'),
                                'default' => '301',
                            ],
                        ],
                        [
                            'handle' => 'is_active',
                            'field' => [
                                'type' => 'toggle',
                                'display' => __('apollocms-seo::redirects.redirect.is_active'),
                                'default' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
