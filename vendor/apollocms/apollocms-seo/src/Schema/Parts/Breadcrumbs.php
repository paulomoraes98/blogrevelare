<?php

namespace ApolloCMS\AposeoSeo\Schema\Parts;

use ApolloCMS\SchemaOrg\Schema;
use ApolloCMS\Facades\Config;
use ApolloCMS\Facades\Entry;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\Term;
use ApolloCMS\Facades\URL;
use ApolloCMS\Support\Str;
use ApolloCMS\AposeoSeo\Schema\SchemaIds;
use ApolloCMS\AposeoSeo\Schema\Parts\Contracts\SchemaPart;

class Breadcrumbs implements SchemaPart
{
    /**
     * Similar to how NavTags->breadcrumbs works
     */
    public function list()
    {
        $crumbs = [];

        $url = URL::makeAbsolute(URL::getCurrent());
        $url = Str::removeLeft($url, Site::current()->absoluteUrl());
        $url = Str::ensureLeft($url, '/');

        $segments = explode('/', $url);
        $segments[0] = '/';

        // Create crumbs from segments
        $crumbs = collect($segments)->map(function () use (&$segments) {
            $uri = URL::tidy(join('/', $segments));
            array_pop($segments);

            return $uri;
        })->mapWithKeys(function ($uri) {
            $entry = Entry::findByUri($uri, Site::current()->handle());
            if ($entry) {
                return [$uri => $entry];
            }
            $term = Term::findByUri($uri, Site::current()->handle());
            if ($term) {
                return [$uri => $term];
            }

            return [$uri => null];
        })->filter();

        return $crumbs->reverse();
    }

    public function data()
    {
        $breadcrumbs = Schema::breadcrumbList();
        $crumbs = $this->list();

        $position = 1;
        $listItems = [];
        foreach ($crumbs as $crumb) {
            $listItem = Schema::listItem();
            $listItem->position($position);
            $item = Schema::thing();
            $item->name($crumb->get('title'));
            $item->setProperty('id', $crumb->absoluteUrl());
            $listItem->item($item);
            $listItems[] = $listItem;
            $position++;
        }

        $breadcrumbs->itemListElement($listItems);
        return $breadcrumbs;
    }

    public static function id()
    {
        return URL::makeAbsolute(Config::getSiteUrl()) . SchemaIds::BREADCRUMBS;
    }
}
