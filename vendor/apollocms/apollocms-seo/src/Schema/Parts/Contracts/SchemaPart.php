<?php

namespace ApolloCMS\AposeoSeo\Schema\Parts\Contracts;

interface SchemaPart
{
    public function data();

    public static function id();
}
