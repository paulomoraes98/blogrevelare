<?php

namespace ApolloCMS\AposeoSeo\Schema\Parts;

use ApolloCMS\SchemaOrg\Schema;
use ApolloCMS\Facades\Config;
use ApolloCMS\Facades\URL;
use ApolloCMS\AposeoSeo\Schema\SchemaIds;
use ApolloCMS\AposeoSeo\Schema\Parts\SiteOwner;
use ApolloCMS\AposeoSeo\Schema\Parts\Contracts\SchemaPart;

class WebSite implements SchemaPart
{
    public function __construct($context = [])
    {
        $this->context = $context;
    }

    public function data()
    {
        $site = Schema::webSite();
        $site->url(URL::makeAbsolute(Config::getSiteUrl()));
        $site->setProperty('publisher', ['@id' => SiteOwner::id()]);
        $site->setProperty('@id', self::id());
        return $site;
    }

    public static function id()
    {
        return URL::makeAbsolute(Config::getSiteUrl()) . SchemaIds::WEB_SITE;
    }
}
