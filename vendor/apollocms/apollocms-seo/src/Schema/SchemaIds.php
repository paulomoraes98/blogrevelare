<?php

namespace ApolloCMS\AposeoSeo\Schema;

class SchemaIds
{
    const BREADCRUMBS = '#breadcrumbs';

    const SITE_OWNER = '#site-owner';

    const WEB_SITE = '#website';

    const WEB_PAGE = '#web_page';
}
