<?php

namespace ApolloCMS\AposeoSeo\Schema;

use ApolloCMS\SchemaOrg\Graph;
use ApolloCMS\AposeoSeo\Blueprints\CP\GeneralSettingsBlueprint;
use ApolloCMS\AposeoSeo\Facades\PageDataParser;
use ApolloCMS\AposeoSeo\Schema\SchemaIds;
use ApolloCMS\AposeoSeo\Schema\Parts\Breadcrumbs;
use ApolloCMS\AposeoSeo\Schema\Parts\SiteOwner;
use ApolloCMS\AposeoSeo\Schema\Parts\WebPage;
use ApolloCMS\AposeoSeo\Schema\Parts\WebSite;

class SchemaGraph
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $context;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $globals;

    /**
     * @var Graph
     */
    protected $graph;

    public function __construct($context)
    {
        $this->context = $context;
        $this->graph = new Graph();

        $this->globals = PageDataParser::getSettingsBlueprintWithValues($context, 'general', new GeneralSettingsBlueprint());

        $this->populateData();
    }

    private function populateData()
    {
        $siteOwner = new SiteOwner($this->globals);
        $webSite = new WebSite($this->globals);
        $webPage = new WebPage($this->context);
        $webPageData = $webPage->data();

        // // If breadcrumbs are enabled - add them to the graph
        if (!empty($this->globals->get('enable_breadcrumbs', 0)) && $this->context->get('url', '') !== '/') {
            $breadcrumbs = new Breadcrumbs();
            $webPageData->breadcrumb($breadcrumbs->data());
        }

        $this->graph->add($siteOwner->data());
        $this->graph->add($webSite->data());
        $this->graph->add($webPageData);
    }

    public function build()
    {
        return $this->graph;
    }
}
