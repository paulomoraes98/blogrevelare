<?php

namespace ApolloCMS\AposeoSeo\Content;

use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;

class ContentDefaultsGetter
{
    /**
     * Get a key from the defaults for this content
     *
     * @param string $type
     * @param string $handle
     * @param ApolloCMS\Sites\Site $site
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public static function get(string $type, string $handle, \ApolloCMS\Sites\Site $site, string $key, $default)
    {
        $defaults = new ContentDefaults($type, $handle, $site);
        return $defaults->data()->get($key, $default);
    }
}
