<?php

namespace ApolloCMS\AposeoSeo\Content;

use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;

class ContentDefaults
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $handle;

    /**
     * @var \ApolloCMS\Sites\Site
     */
    public $site;

    /**
     * @return void
     */
    public function __construct(string $type, string $handle, \ApolloCMS\Sites\Site $site)
    {
        $this->type = $type;
        $this->handle = $handle;
        $this->site = $site ?: Site::current();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function data()
    {
        return AposeoStorage::getYaml("defaults/{$this->type}_{$this->handle}", $this->site, true);
    }
}
