<?php

namespace ApolloCMS\AposeoSeo\Listeners;

use Illuminate\Support\Facades\Cache;
use ApolloCMS\Support\Str;
use ApolloCMS\Facades\Site;

class ContentSitemapCacheInvalidationListener
{
    public function handle($event)
    {
        $blank_event = new \ReflectionClass($event);
        $content_type = Str::contains($blank_event->getShortName(), 'Term') ? 'term' : 'entry';

        if ($content_type === 'term') {
            $term = $event->term;
            $handle = $term->taxonomy()->handle();
        } else {
            $entry = $event->entry;
            $handle = $entry->collection()->handle();
        }

        $site = Site::current();

        Cache::forget("apollocms-seo.sitemap-index.{$site->handle()}");
        Cache::forget("apollocms-seo.sitemap-{$handle}.{$site->handle()}");
    }
}
