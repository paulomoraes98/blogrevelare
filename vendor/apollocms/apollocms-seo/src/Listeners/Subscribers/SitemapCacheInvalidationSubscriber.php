<?php

namespace ApolloCMS\AposeoSeo\Listeners\Subscribers;

use ApolloCMS\AposeoSeo\Listeners\ContentSitemapCacheInvalidationListener;

class SitemapCacheInvalidationSubscriber
{
    /**
     * Subscribe to content change events to
     * clear the sitemap caches
     *
     * @var array
     */
    protected $events =
    [
        \ApolloCMS\Events\EntrySaved::class,
        \ApolloCMS\Events\EntryDeleted::class,
        \ApolloCMS\Events\TermSaved::class,
        \ApolloCMS\Events\TermDeleted::class,
    ];

    /**
     * Register the invalidation listener for the events
     */
    public function subscribe($events)
    {
        foreach ($this->events as $event) {
            $events->listen($event, ContentSitemapCacheInvalidationListener::class . '@handle');
        }
    }
}
