<?php

namespace ApolloCMS\AposeoSeo\Listeners;

use Illuminate\Support\Facades\Cache;

class DefaultsSitemapCacheInvalidationListener
{
    public function handle(\ApolloCMS\AposeoSeo\Events\AposeoContentDefaultsSaved $event)
    {
        $defaults = $event->defaults;
        $site = $defaults->site->handle();
        $handle = $defaults->handle;

        Cache::forget("apollocms-seo.sitemap-index.{$site}");
        Cache::forget("apollocms-seo.sitemap-{$handle}.{$site}");
    }
}
