<?php

namespace ApolloCMS\AposeoSeo\Fieldtypes;

use ApolloCMS\Facades\Site;
use ApolloCMS\Fields\Fieldtype;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;

class AposeoSeoMetaDescriptionFieldtype extends Fieldtype
{
    protected $selectable = false;
}
