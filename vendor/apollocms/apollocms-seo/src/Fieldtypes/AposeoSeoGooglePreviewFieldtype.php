<?php

namespace ApolloCMS\AposeoSeo\Fieldtypes;

use ApolloCMS\Facades\Site;
use ApolloCMS\Fields\Fieldtype;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;

class AposeoSeoGooglePreviewFieldtype extends Fieldtype
{
    protected $selectable = false;

    /**
     * Load the global seo settings from storage
     */
    public function preload()
    {
        $site = Site::selected();
        $data = AposeoStorage::getYaml('general', $site, true);
        return [
            'site_name' => $data->get('site_name', ''),
            'site_url' => $site->absoluteUrl(),
            'title_separator' => $data->get('title_separator', '|'),
        ];
    }
}
