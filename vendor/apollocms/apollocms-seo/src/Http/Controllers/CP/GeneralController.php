<?php

namespace ApolloCMS\AposeoSeo\Http\Controllers\CP;

use ApolloCMS\CP\Breadcrumbs;
use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\User;
use ApolloCMS\AposeoSeo\Blueprints\CP\GeneralSettingsBlueprint;
use ApolloCMS\AposeoSeo\Events\AposeoGlobalsUpdated;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;
use ApolloCMS\AposeoSeo\Http\Controllers\CP\Contracts\Publishable;

class GeneralController extends Controller implements Publishable
{
    public function index()
    {
        $this->authorize('view aposeo general settings');

        $data = $this->getData();

        $blueprint = $this->getBlueprint();
        $fields = $blueprint->fields()->addValues($data)->preProcess();

        $crumbs = Breadcrumbs::make([
            ['text' => 'Aposeo SEO', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings')],
            ['text' => 'General Settings', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings/general')],
        ]);

        return view('apollocms-seo::cp.settings.general', [
            'blueprint' => $blueprint->toPublishArray(),
            'crumbs' => $crumbs,
            'meta' => $fields->meta(),
            'title' => 'General Settings | Aposeo SEO',
            'values' => $fields->values(),
        ]);
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('update aposeo general settings');

        $blueprint = $this->getBlueprint();

        $fields = $blueprint->fields()->addValues($request->all());
        $fields->validate();

        $this->putData($fields->process()->values()->toArray());

        AposeoGlobalsUpdated::dispatch('general');
    }

    /**
     * Redirects from the top level SEO nav item
     */
    public function settingsRedirect()
    {
        $groups = collect([
            'general',
            'marketing',
            'defaults',
            'social',
            'sitemap',
        ]);

        $first_group = $groups->filter(function ($group) {
            return User::current()->can("view aposeo {$group} settings");
        })->first();

        if (!empty($first_group)) {
            return redirect()->route("apollocms.cp.apollocms-seo.{$first_group}.index");
        }

        // If no permissions are found use ApolloCMS to inform the user
        $this->authorize('view aposeo general settings');
    }

    /**
     * @inheritdoc
     */
    public function getBlueprint()
    {
        return GeneralSettingsBlueprint::requestBlueprint();
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        return AposeoStorage::getYaml('general', Site::selected());
    }

    /**
     * @inheritdoc
     */
    public function putData($data)
    {
        return AposeoStorage::putYaml('general', Site::selected(), $data);
    }
}
