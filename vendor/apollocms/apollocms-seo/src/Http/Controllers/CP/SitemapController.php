<?php

namespace ApolloCMS\AposeoSeo\Http\Controllers\CP;

use ApolloCMS\CP\Breadcrumbs;
use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Blueprints\CP\SitemapSettingsBlueprint;
use ApolloCMS\AposeoSeo\Events\AposeoGlobalsUpdated;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;
use ApolloCMS\AposeoSeo\Http\Controllers\CP\Contracts\Publishable;

class SitemapController extends Controller implements Publishable
{
    public function index()
    {
        $this->authorize('view aposeo sitemap settings');

        $data = $this->getData();

        $blueprint = $this->getBlueprint();
        $fields = $blueprint->fields()->addValues($data)->preProcess();

        $crumbs = Breadcrumbs::make([
            ['text' => 'Aposeo SEO', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings')],
            ['text' => 'Sitemap Settings', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings/sitemap')],
        ]);

        return view('apollocms-seo::cp.settings.sitemap', [
            'blueprint' => $blueprint->toPublishArray(),
            'crumbs' => $crumbs,
            'meta' => $fields->meta(),
            'title' => 'Sitemap Settings | Aposeo SEO',
            'values' => $fields->values(),
        ]);
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('update aposeo sitemap settings');

        $blueprint = $this->getBlueprint();

        $fields = $blueprint->fields()->addValues($request->all());
        $fields->validate();

        $this->putData($fields->process()->values()->toArray());

        AposeoGlobalsUpdated::dispatch('sitemap');
    }

    /**
     * @inheritdoc
     */
    public function getBlueprint()
    {
        return SitemapSettingsBlueprint::requestBlueprint();
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        return AposeoStorage::getYaml('sitemap', Site::selected());
    }

    /**
     * @inheritdoc
     */
    public function putData($data)
    {
        return AposeoStorage::putYaml('sitemap', Site::selected(), $data);
    }
}
