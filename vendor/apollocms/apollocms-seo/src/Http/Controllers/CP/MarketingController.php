<?php

namespace ApolloCMS\AposeoSeo\Http\Controllers\CP;

use ApolloCMS\CP\Breadcrumbs;
use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Blueprints\CP\MarketingSettingsBlueprint;
use ApolloCMS\AposeoSeo\Events\AposeoGlobalsUpdated;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;
use ApolloCMS\AposeoSeo\Http\Controllers\CP\Contracts\Publishable;

class MarketingController extends Controller implements Publishable
{
    public function index()
    {
        $this->authorize('view aposeo marketing settings');

        $data = $this->getData();

        $blueprint = $this->getBlueprint();
        $fields = $blueprint->fields()->addValues($data)->preProcess();

        $crumbs = Breadcrumbs::make([
            ['text' => 'Aposeo SEO', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings')],
            ['text' => 'Marketing Settings', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings/marketing')],
        ]);

        return view('apollocms-seo::cp.settings.marketing', [
            'blueprint' => $blueprint->toPublishArray(),
            'crumbs' => $crumbs,
            'meta' => $fields->meta(),
            'title' => 'Marketing Settings | Aposeo SEO',
            'values' => $fields->values(),
        ]);
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('update aposeo marketing settings');

        $blueprint = $this->getBlueprint();

        $fields = $blueprint->fields()->addValues($request->all());
        $fields->validate();

        $this->putData($fields->process()->values()->toArray());

        AposeoGlobalsUpdated::dispatch('marketing');
    }

    /**
     * @inheritdoc
     */
    public function getBlueprint()
    {
        return MarketingSettingsBlueprint::requestBlueprint();
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        return AposeoStorage::getYaml('marketing', Site::selected());
    }

    /**
     * @inheritdoc
     */
    public function putData($data)
    {
        return AposeoStorage::putYaml('marketing', Site::selected(), $data);
    }
}
