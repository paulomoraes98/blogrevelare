<?php

namespace ApolloCMS\AposeoSeo\Http\Controllers\CP;

use ApolloCMS\CP\Breadcrumbs;
use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Blueprints\CP\SocialSettingsBlueprint;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;
use ApolloCMS\AposeoSeo\Events\AposeoGlobalsUpdated;
use ApolloCMS\AposeoSeo\Http\Controllers\CP\Contracts\Publishable;

class SocialController extends Controller implements Publishable
{
    public function index()
    {
        $this->authorize('view aposeo social settings');

        $data = $this->getData();

        $blueprint = $this->getBlueprint();
        $fields = $blueprint->fields()->addValues($data)->preProcess();

        $crumbs = Breadcrumbs::make([
            ['text' => 'Aposeo SEO', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings')],
            ['text' => 'Social Settings', 'url' => url(config('apollocms.cp.route') . '/apollocms-seo/settings/social')],
        ]);

        return view('apollocms-seo::cp.settings.social', [
            'blueprint' => $blueprint->toPublishArray(),
            'crumbs' => $crumbs,
            'meta' => $fields->meta(),
            'title' => 'Social Settings | Aposeo SEO',
            'values' => $fields->values(),
        ]);
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('update aposeo social settings');

        $blueprint = $this->getBlueprint();

        $fields = $blueprint->fields()->addValues($request->all());
        $fields->validate();

        $this->putData($fields->process()->values()->toArray());

        AposeoGlobalsUpdated::dispatch('social');
    }

    /**
     * @inheritdoc
     */
    public function getBlueprint()
    {
        return SocialSettingsBlueprint::requestBlueprint();
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        return AposeoStorage::getYaml('social', Site::selected());
    }

    /**
     * @inheritdoc
     */
    public function putData($data)
    {
        return AposeoStorage::putYaml('social', Site::selected(), $data);
    }
}
