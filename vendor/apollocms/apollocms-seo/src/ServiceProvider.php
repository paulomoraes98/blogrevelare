<?php

namespace ApolloCMS\AposeoSeo;

use Illuminate\Support\Facades\Route;
use ApolloCMS\Providers\AddonServiceProvider;
use ApolloCMS\Facades\CP\Nav;
use ApolloCMS\Facades\Permission;
use ApolloCMS\Events\EntryBlueprintFound;
use ApolloCMS\Events\ResponseCreated;
use ApolloCMS\Events\TermBlueprintFound;
use ApolloCMS\AposeoSeo\Events\AposeoContentDefaultsSaved;
use ApolloCMS\AposeoSeo\Fieldtypes\AposeoSeoMetaTitleFieldtype;
use ApolloCMS\AposeoSeo\Fieldtypes\AposeoSeoMetaDescriptionFieldtype;
use ApolloCMS\AposeoSeo\Fieldtypes\AposeoSeoGooglePreviewFieldtype;
use ApolloCMS\AposeoSeo\Listeners\AppendEntrySeoFieldsListener;
use ApolloCMS\AposeoSeo\Listeners\AppendTermSeoFieldsListener;
use ApolloCMS\AposeoSeo\Listeners\DefaultsSitemapCacheInvalidationListener;
use ApolloCMS\AposeoSeo\Listeners\Subscribers\SitemapCacheInvalidationSubscriber;
use ApolloCMS\AposeoSeo\Http\Controllers\CP\Controller as AposeoSettingsController;
use ApolloCMS\AposeoSeo\Http\Middleware\RedirectsMiddleware;
use ApolloCMS\AposeoSeo\Modifiers\ParseLocaleModifier;
use ApolloCMS\AposeoSeo\Tags\AposeoSeoTags;

class ServiceProvider extends AddonServiceProvider
{
    protected $fieldtypes = [
        AposeoSeoMetaTitleFieldtype::class,
        AposeoSeoMetaDescriptionFieldtype::class,
        AposeoSeoGooglePreviewFieldtype::class,
    ];

    protected $listen = [
        EntryBlueprintFound::class => [
            AppendEntrySeoFieldsListener::class,
        ],
        TermBlueprintFound::class => [
            AppendTermSeoFieldsListener::class,
        ],
        AposeoContentDefaultsSaved::class => [
            DefaultsSitemapCacheInvalidationListener::class,
        ],
    ];

    protected $middlewareGroups = [
        'apollocms.web' => [
            RedirectsMiddleware::class,
        ],
    ];

    protected $modifiers = [
        ParseLocaleModifier::class,
    ];

    protected $routes = [
        'cp' => __DIR__ . '/../routes/cp.php',
        'web' => __DIR__ . '/../routes/web.php',
    ];

    protected $scripts = [
        __DIR__ . '/../public/js/apollocms-seo.js',
    ];

    protected $stylesheets = [
        __DIR__ . '/../public/css/apollocms-seo.css',
    ];

    protected $subscribe = [
        SitemapCacheInvalidationSubscriber::class,
    ];

    protected $tags = [
        AposeoSeoTags::class,
    ];

    public function boot()
    {
        parent::boot();

        // Set up views path
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'apollocms-seo');

        // Set up translations
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'apollocms-seo');

        // Load in custom addon config
        $this->mergeConfigFrom(__DIR__ . '/../config/apollocms-seo.php', 'apollocms-seo');
        $this->publishes([
            __DIR__ . '/../config/apollocms-seo.php' => config_path('apollocms-seo.php'),
        ], 'config');

        // Set up permissions
        $this->bootPermissions();

        // Set up navigation
        $this->bootNav();
    }

    /**
     * Add our custom navigation items to the CP nav
     *
     * @return void
     */
    public function bootNav()
    {
        $routeCollection = Route::getRoutes();

        // Add SEO item to nav
        Nav::extend(function ($nav) {
            // Top level SEO item
            $nav->create('SEO')
                ->can('configure aposeo settings')
                ->section('Tools')
                ->route('apollocms-seo.settings')
                ->icon('seo-search-graph')
                ->children([
                    // Settings categories
                    $nav->item(__('apollocms-seo::general.index'))
                        ->route('apollocms-seo.general.index')
                        ->can('view aposeo general settings'),
                    $nav->item(__('apollocms-seo::defaults.index'))
                        ->route('apollocms-seo.defaults.index')
                        ->can('view aposeo defaults settings'),
                    $nav->item(__('apollocms-seo::marketing.singular'))
                        ->route('apollocms-seo.marketing.index')
                        ->can('view aposeo marketing settings'),
                    $nav->item(__('apollocms-seo::social.singular'))
                        ->route('apollocms-seo.social.index')
                        ->can('view aposeo social settings'),
                    $nav->item(__('apollocms-seo::sitemap.singular'))
                        ->route('apollocms-seo.sitemap.index')
                        ->can('view aposeo sitemap settings'),
                ]);

            $nav->create(__('apollocms-seo::redirects.plural'))
                ->can('view aposeo redirects')
                ->section('Tools')
                ->route('apollocms-seo.redirects.index')
                ->icon('arrow-right')
                ->view('apollocms-seo::cp.nav.redirects')
                ->children([
                    $nav->item(__('apollocms-seo::redirects.manual.plural'))
                        ->can('view aposeo redirects')
                        ->route('apollocms-seo.redirects.manual-redirects.index'),
                ]);
        });
    }

    /**
     * Add permissions for AposeoSEO settings
     *
     * @return void
     */
    public function bootPermissions()
    {
        $settings_groups = [
            [
                'value' => 'general',
                'label' => 'Generais',
            ],
            [
                'value' => 'marketing',
                'label' => 'Marketing',
            ],
            [
                'value' => 'social',
                'label' => 'Social',
            ],
            [
                'value' => 'sitemap',
                'label' => 'Sitemap',
            ],
            [
                'value' => 'defaults',
                'label' => 'Padrões',
            ],
        ];

        Permission::group('apollocms-seo', 'Aposeo SEO', function () use ($settings_groups) {
            Permission::register('configure aposeo settings', function ($permission) use ($settings_groups) {
                $permission->children([
                    Permission::make('view aposeo {settings_group} settings')
                        ->replacements('settings_group', function () use ($settings_groups) {
                            return collect($settings_groups)->map(function ($group) {
                                return [
                                    'value' => $group['value'],
                                    'label' => $group['label'],
                                ];
                            });
                        })
                        ->label('Visualizar Configurações :settings_group')
                        ->children([
                            Permission::make('update aposeo {settings_group} settings')
                                ->label('Editar Configurações :settings_group'),
                        ]),
                    Permission::make('view aposeo redirects')
//                        ->label(__('apollocms-seo::redirects.permissions.view'))
                        ->label('Visualizar Redirecionamentos')
                        ->children([
                            Permission::make('edit aposeo redirects')
//                                ->label(__('apollocms-seo::redirects.permissions.edit')),
                                ->label('Editar Redirecionamentos'),
                            Permission::make('create aposeo redirects')
//                                ->label(__('apollocms-seo::redirects.permissions.create')),
                                ->label('Criar Redirecionamentos'),
                        ]),
                ]);
            })->label('Configure Aposeo Settings');
        });
    }
}
