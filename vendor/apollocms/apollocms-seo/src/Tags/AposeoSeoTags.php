<?php

namespace ApolloCMS\AposeoSeo\Tags;

use ApolloCMS\Facades\Entry;
use ApolloCMS\Facades\Site;
use ApolloCMS\Tags\Tags;
use ApolloCMS\AposeoSeo\Schema\SchemaGraph;
use ApolloCMS\AposeoSeo\Facades\AposeoStorage;
use ApolloCMS\AposeoSeo\Facades\PageDataParser;

class AposeoSeoTags extends Tags
{
    protected static $handle = 'apollocms-seo';

    /**
     * Return the <head /> tag content required for on-page SEO
     *
     * @return string
     */
    public function head()
    {
        $data = PageDataParser::getData(collect($this->context));

        $view = view('apollocms-seo::tags.head', $data);

        if ($this->params->get('debug')) {
            return $view;
        }

        return preg_replace(
            [
                "/<!--(.|\s)*?-->/",
                "/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/",
            ],
            [
                '',
                "\n",
            ],
            $view
        );
    }

    /**
     * Return the body content
     */
    public function body()
    {
        $data = PageDataParser::getData(collect($this->context));
        return view('apollocms-seo::tags.body', $data);
    }

    /**
     * Return the footer content
     */
    public function footer()
    {
        $data = PageDataParser::getData(collect($this->context));
        return view('apollocms-seo::tags.footer', $data);
    }

    /**
     * Return the data for hreflang tags
     */
    public function hreflang()
    {
        $ctx = collect($this->context);

        if (!$ctx->get('id')) {
            return null;
        }

        $data = Entry::find($ctx->get('id'));

        if (!$data) {
            return null;
        }

        $alternates = $data->sites()->filter(function ($locale) use ($data) {
            return !empty($data->in($locale));
        })->reject(function ($locale) use ($data) {
            return $locale === $data->locale();
        });

        if ($alternates->count() > 0) {
            $data = $alternates->map(function ($locale) use ($data) {
                $site = Site::get($locale);
                $localized_data = $data->in($locale);
                return [
                    'url' => $localized_data->absoluteUrl(),
                    'locale' => $site->locale(),
                ];
            });
            return view('apollocms-seo::tags.hreflang', ['hreflang_tags' => $data]);
        }
    }

    /**
     * Return the schema graph object
     *
     * @return string
     */
    public function graph()
    {
        $ctx = collect($this->context);
        $graph = new SchemaGraph($ctx);
        return $graph->build();
    }

    /**
     * Return the list of social icons created in the 'Social' menu
     *
     * @return string
     */
    public function socials()
    {
        $data = PageDataParser::getData(collect($this->context));
        $socials = $data->get('aposeo_social_settings')->get('social_links');
        if ($socials->raw()) {
            return $this->parseLoop($socials->raw());
        }
        return $this->parseLoop([]);
    }

    /**
     * Return the robots tag content
     * (done here to prevent a bunch of ifs and butts in the template file)
     *
     * @return string
     */
    public function robotsTag()
    {
        $ctx = collect($this->context);
        $attrs = [];

        $global_no_index = $ctx->get('aposeo_general_settings')['no_index_site'];

        if ($ctx->get('no_index_page') || $global_no_index->raw()) {
            array_push($attrs, 'noindex');
        }

        if ($ctx->get('no_follow_links')) {
            array_push($attrs, 'nofollow');
        }

        if (!empty($attrs)) {
            $attrs_string = implode(', ', $attrs);
            return "<meta name=\"robots\" content=\"{$attrs_string}\">";
        }

        return false;
    }

    /**
     * Return a generated canonical URL - this should contain pagination vars
     * if any are set
     *
     * @return string
     */
    public function generatedCanonical()
    {
        $data = collect($this->context);
        $vars = $data->get('get');
        $current_url = $data->get('permalink');
        if ($vars && $page = collect($vars)->get('page')) {
            $current_url .= '?page=' . urlencode($page);
        }
        return $current_url;
    }
}
