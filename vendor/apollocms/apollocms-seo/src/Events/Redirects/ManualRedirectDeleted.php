<?php

namespace ApolloCMS\AposeoSeo\Events\Redirects;

use ApolloCMS\Events\Event;
use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class ManualRedirectDeleted extends Event implements ProvidesCommitMessage
{
    public function __construct()
    {
        // No op
    }

    /**
     * @return string
     */
    public function commitMessage()
    {
        return 'Aposeo manual redirect deleted';
    }
}
