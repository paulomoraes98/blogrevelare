<?php

namespace ApolloCMS\AposeoSeo\Events\Redirects;

use ApolloCMS\Events\Event;
use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class ManualRedirectSaved extends Event implements ProvidesCommitMessage
{
    public $redirect;

    public function __construct($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * @return string
     */
    public function commitMessage()
    {
        return 'Aposeo manual redirect saved';
    }
}
