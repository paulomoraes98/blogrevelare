<?php

namespace ApolloCMS\AposeoSeo\Events;

use ApolloCMS\Events\Event;
use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class AposeoGlobalsUpdated extends Event implements ProvidesCommitMessage
{
    /**
     * @var string
     */
    public $handle;

    public function __construct(string $handle)
    {
        $this->handle = $handle;
    }

    /**
     * @return string
     */
    public function commitMessage()
    {
        return 'Aposeo globals saved';
    }
}
