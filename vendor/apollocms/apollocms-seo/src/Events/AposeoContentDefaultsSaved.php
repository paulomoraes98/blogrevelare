<?php

namespace ApolloCMS\AposeoSeo\Events;

use ApolloCMS\Events\Event;
use ApolloCMS\Contracts\Git\ProvidesCommitMessage;

class AposeoContentDefaultsSaved extends Event implements ProvidesCommitMessage
{
    /**
     * @var ApolloCMS\AposeoSeo\Content\Defaults
     */
    public $defaults;

    /**
     * @param ApolloCMS\AposeoSeo\Content\Defaults $defaults
     */
    public function __construct($defaults)
    {
        $this->defaults = $defaults;
    }

    /**
     * @return string
     */
    public function commitMessage()
    {
        return 'Aposeo content defaults saved';
    }
}
