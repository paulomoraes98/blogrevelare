<?php

namespace ApolloCMS\AposeoSeo\Modifiers;

use ApolloCMS\Modifiers\Modifier;
use WhiteCube\Lingua\Service as Lingua;

class ParseLocaleModifier extends Modifier
{
    protected static $handle = 'aposeo_parse_locale';

    public static function index($value)
    {
        $parsed = preg_replace('/\.utf8/i', '', $value);

        // Convert to W3C
        $lang = Lingua::create($parsed);
        $code = $lang->toW3C();

        return $code;
    }
}
