<?php

namespace ApolloCMS\AposeoSeo\Actions\Redirects;

use ApolloCMS\Actions\Action;
use ApolloCMS\Facades\Site;
use ApolloCMS\AposeoSeo\Redirects\Repositories\RedirectsRepository;

class DeleteManualRedirectsAction extends DeleteRedirectsAction
{
    public function run($items, $values)
    {
        $items->each(function ($redirect) {
            $this->repository()->delete($redirect);
        });
    }

    private function repository()
    {
        return new RedirectsRepository('redirects/manual', Site::selected());
    }
}
