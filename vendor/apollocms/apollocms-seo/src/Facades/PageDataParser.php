<?php

namespace ApolloCMS\AposeoSeo\Facades;

use ApolloCMS\AposeoSeo\Parsers\PageDataParser as Parser;
use Illuminate\Support\Facades\Facade;

class PageDataParser extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return Parser::class;
    }
}
