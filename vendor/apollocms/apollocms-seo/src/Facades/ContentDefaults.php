<?php

namespace ApolloCMS\AposeoSeo\Facades;

use ApolloCMS\AposeoSeo\Content\ContentDefaultsGetter;
use Illuminate\Support\Facades\Facade;

class ContentDefaults extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return ContentDefaultsGetter::class;
    }
}
