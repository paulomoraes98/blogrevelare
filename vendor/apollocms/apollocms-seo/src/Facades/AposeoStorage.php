<?php

namespace ApolloCMS\AposeoSeo\Facades;

use ApolloCMS\AposeoSeo\Storage\GlobalsStorage;
use Illuminate\Support\Facades\Facade;

class AposeoStorage extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return GlobalsStorage::class;
    }
}
