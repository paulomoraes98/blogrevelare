<?php

namespace ApolloCMS\AposeoSeo\Storage;

use ApolloCMS\Sites\Site;

interface Storage
{
    public static function getYaml(string $handle, Site $site, bool $returnCollection);

    public static function putYaml(string $handle, Site $site, array $data);
}
