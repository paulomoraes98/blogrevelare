<?php

use ApolloCMS\Facades\Site;
use ApolloCMS\Facades\URL;

Route::namespace('\ApolloCMS\AposeoSeo\Http\Controllers\Web')
    ->name('aposeo-xml-sitemap')
    ->group(function () {
        Route::get('sitemap.xml', 'SitemapController@index')
            ->name('index');
        Route::get('sitemap_{handle}.xml', 'SitemapController@single')
            ->name('single');
        Route::get('aposeo-sitemap.xsl', 'SitemapController@xsl')
            ->name('xsl');

        // Add sitemap routes for non-domain-level multisites
        $roots = Site::all()->map(function ($site) {
            return URL::makeRelative($site->url());
        })->filter(function ($root) {
            return $root !== '/';
        })->unique();

        $roots->each(function ($root) {
            Route::get("${root}/sitemap.xml", 'SitemapController@index')
                ->name("${root}.index");
            Route::get("${root}/sitemap_{handle}.xml", 'SitemapController@single')
                ->name("${root}.single");
            Route::get("${root}/aposeo-sitemap.xsl", 'SitemapController@xsl')
                ->name("${root}.xsl");
        });
    });
