@extends('apollocms::layout')

@section('content')
    <breadcrumbs :crumbs='@json($crumbs)'></breadcrumbs>
    <aposeo-redirects-publish-form
        title="{{ $title }}"
        action="{{ cp_route('apollocms-seo.redirects.manual-redirects.store') }}"
        redirect-url={{ cp_route('apollocms-seo.redirects.manual-redirects.index') }}
        :blueprint='@json($blueprint)'
        :meta='@json($meta)'
        :values='@json($values)'
    ></aposeo-redirects-publish-form>
@stop
