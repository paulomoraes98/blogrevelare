@extends('apollocms::layout')

@section('content')
    <breadcrumbs :crumbs='@json($crumbs)'></breadcrumbs>

    <div class="flex items-center mb-3">
        <h1 class="flex-1">{{ $title }}</h1>
        <div>
            <a href="{{ cp_route('apollocms-seo.redirects.manual-redirects.create') }}" class="btn-primary">{{ __('apollocms-seo::redirects.actions.create') }}</a>
        </div>
    </div>

    <aposeo-manual-redirects-listing
        :initial-redirects='@json($redirects)'
        :initial-columns='@json($columns)'
        create-url='{{ cp_route('apollocms-seo.redirects.manual-redirects.create') }}'
        bulk-actions-url='{{ cp_route('apollocms-seo.redirects.manual-redirects.actions') }}'
    ></aposeo-manual-redirects-listing>
@stop
