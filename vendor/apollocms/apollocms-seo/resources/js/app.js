import MetaTitleFieldtype from './components/fieldtypes/MetaTitleFieldtype';
import MetaDescriptionFieldtype from './components/fieldtypes/MetaDescriptionFieldtype';
import GooglePreviewFieldtype from './components/fieldtypes/GooglePreviewFieldtype';
import ManualRedirectsListing from './components/cp/redirects/manual/Listing';
import RedirectsPublishForm from './components/cp/redirects/PublishForm';

ApolloCMS.booting(() => {
    // Fieldtypes
    ApolloCMS.component('aposeo_seo_meta_title-fieldtype', MetaTitleFieldtype);
    ApolloCMS.component('aposeo_seo_meta_description-fieldtype', MetaDescriptionFieldtype);
    ApolloCMS.component('aposeo_seo_google_preview-fieldtype', GooglePreviewFieldtype);

    // Redirects components
    ApolloCMS.component('aposeo-manual-redirects-listing', ManualRedirectsListing);
    ApolloCMS.component('aposeo-redirects-publish-form', RedirectsPublishForm);
});
