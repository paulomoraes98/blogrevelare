<?php

return [

    'singular' => 'Social',
    'plural' => 'Sociais',

    // CP
    'fields' => [
        'social_section' => [
            'display' => 'Configuracão de Social Media',
            'instruct' => 'Coloque todos os links de mídia social relacionados na tabela abaixo.'
        ],
        'social_links' => [
            'display' => 'Social Media Links',
            'add_new' => 'Adicione um novo ícone social',
            'icon' => 'Social ícone',
            'url' => 'URL'
        ],
        'opengraph' => [
            'display' => 'OpenGraph',
        ],
        'og_image_site' => [
            'display' => 'Imagem de compartilhamento OpenGraph padrão',
            'instruct' => 'Carregue uma imagem padrão para quando o site for compartilhado em plataformas de mídia social que suportam OpenGraph. O tamanho recomendado é 1200 px x 630 px.'
        ],
        'twitter' => [
            'display' => 'Twitter',
        ],
        'twitter_username' => [
            'display' => 'Twitter Username',
            'instruct' => 'Seu nome de usuário do Twitter (incluindo o símbolo @)'
        ],
        'twitter_meta_section_site' => [
            'display' => 'Twitter Share Data',
            'instruct' => 'Estes são os dados padrão que serão usados quando este site for compartilhado no Twitter. Esses dados podem ser substituídos no nível da coleção e/ou da página.'
        ],
        'twitter_card_type_site' => [
            'display' => 'Card Type',
            'instruct' => 'Selecione o tipo de cartão do Twitter que deve ser usado em todo o site.'
        ],
        'twitter_default_image_section' => [
            'display' => 'Default Twitter Images',
            'instruct' => 'Faça upload de imagens padrão para usar ao compartilhar páginas neste site no Twitter.'
        ],
        'twitter_summary_image_site' => [
            'display' => 'Default Summary Image',
            'instruct' => 'Faça upload de uma imagem padrão para mostrar no Twitter quando esta página for compartilhada. O tamanho recomendado é 240px x 240px.'
        ],
        'twitter_summary_large_image_site' => [
            'display' => 'Default Large Summary Image',
            'instruct' => 'Faça upload de uma imagem padrão para mostrar no Twitter quando esta página for compartilhada usando um cartão grande. O tamanho recomendado é 876 px x 438 px.'
        ],
    ]

];
