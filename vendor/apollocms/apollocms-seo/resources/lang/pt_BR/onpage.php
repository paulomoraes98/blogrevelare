<?php

return [
    'fields' => [
        'meta_section' => [
            'display' => 'Meta Data',
            'instruct' => 'Edite os metadados para esta página específica.',
        ],
        'meta_title' => [
            'display' => 'Meta Title',
        ],
        'meta_description' => [
            'display' => 'Meta Description',
        ],
        'use_meta_keywords' => [
            'display' => 'Use Meta Keywords',
            'instruct' => 'Você pode alternar este campo para usar palavras-chave meta na página; no entanto, você deve se concentrar na criação de conteúdo de qualidade em vez de usar palavras-chave.'
        ],
        'meta_keywords' => [
            'display' => 'Meta Keywords',
            'instruct' => 'Pressione Enter para adicionar uma nova palavra-chave.'
        ],
        'google_preview' => [
            'display' => 'Google search preview',
        ],
        'urls_section' => [
            'display' => 'URL Options',
        ],
        'canonical_url' => [
            'display' => 'Canonical URL',
        ],
        'localized_urls' => [
            'display' => 'URLs alternativos',
            'instruct' => 'Crie uma lista de urls para localidades alternativas.',
            'add_row' => 'Adicionar um novo local',
            'fields' => [
                'locale' => [
                    'display' => 'Localidade',
                ],
                'url' => [
                    'display' => 'URL'
                ]
            ]
        ],
        'indexing_section' => [
            'display' => 'Indexing e Sitemaps',
        ],
        'no_index_page' => [
            'display' => 'No Index',
            'instruct' => 'Impedir que esta página seja indexada por motores de busca.',
        ],
        'no_follow_links' => [
            'display' => 'Nofollow Links',
            'instruct' => 'Habilitar isso impedirá que os rastreadores de sites sigam os links na página.',
        ],
        'sitemap_priority' => [
            'display' => 'Sitemap Priority',
            'instruct' => 'Defina a prioridade desta página no mapa do site (1.0 sendo o mais importante).',
        ],
        'sitemap_changefreq' => [
            'display' => 'Frequência',
            'instruct' => 'Defina a frequência com que esta página será alterada para o mapa do site.',
        ],
        'share_section_og' => [
            'display' => 'OpenGraph Sharing Data',
            'instruct' => 'Controle a aparência desta página quando compartilhada em sites que interpretam dados do Open Graph (Facebook, WhatsApp, LinkedIn etc).',
        ],
        'og_title' => [
            'display' => 'OpenGraph Title',
        ],
        'og_description' => [
            'display' => 'OpenGraph Description',
        ],
        'og_image' => [
            'display' => 'OpenGraph Image',
        ],
        'share_section_twitter' => [
            'display' => 'Twitter Sharing Data',
            'instruct' => 'Controle a aparência desta página quando compartilhada no Twitter, esses dados serão automaticamente herdados dos dados OG, mas você pode usar os campos a seguir para substituir pelo Twitter.',
        ],
        'twitter_title' => [
            'display' => 'Twitter Title',
        ],
        'twitter_description' => [
            'display' => 'Twitter Description',
        ],
        'twitter_card_type_page' => [
            'display' => 'Twitter Card Type',
            'instruct' => 'Selecione qual tipo de cartão do Twitter deve ser exibido quando esta página for compartilhada.',
        ],
        'twitter_summary_image' => [
            'display' => 'Twitter Summary Card Image',
            'instruct' => 'Faça upload de uma imagem para mostrar no twitter quando esta página for compartilhada. O tamanho recomendado é 240px x 240px.',
        ],
        'twitter_summary_large_image' => [
            'display' => 'Twitter Summary Card Large Image',
            'instruct' => 'Faça upload de uma imagem para mostrar no twitter quando esta página for compartilhada. O tamanho recomendado é 876 px x 438 px.',
        ],
        'scripts_section' => [
            'display' => 'Scripts Personalizados',
            'instruct' => 'Coloque todos os scripts personalizados nas caixas a seguir para adicioná-los a esta entrada.'
        ],
        'head_snippets' => [
            'display' => 'Códigos no Head',
            'instruct' => 'Códigos personalizados para serem colocados no cabeçalho, lembre-se de envolver seus scripts com as ta`gs <script>`.',
        ],
        'footer_snippets' => [
            'display' => 'Códigos no Footer',
            'instruct' => 'Códigos personalizados para serem colocados no rodapé, lembre-se de envolver seus scripts com as tags `<script>`.',
        ],
        'schema_objects' => [
            'display' => 'JSON-LD Schema',
            'instruct' => 'Cole seus objetos de esquema personalizados aqui (Recipe, Event etc...) - oos objetos precisarão ser agrupados em `<script type="application/ld+json">` tags.',
        ],
    ]
];
