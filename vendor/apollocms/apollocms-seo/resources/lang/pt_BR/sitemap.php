<?php

return [

    'singular' => 'Sitemap',
    'plural' => 'Sitemaps',

    // CP
    'fields' => [
        'enable_sitemap' => [
            'display' => 'Habilitar Sitemap?',
        ],
        'sitemap_cache_expiration' => [
            'display' => 'Sitemap Cache Expiration',
            'instruct' => 'Defina a quantidade de tempo antes que o mapa do site seja gerado novamente.',
        ],
        'exclude_content_section' => [
            'display' => 'Excluir conteúdo',
        ],
        'exclude_collections' => [
            'display' => 'Excluir Coleções',
            'instruct' => 'Selecione as coleções que você gostaria de excluir do mapa do site.',
        ],
        'exclude_taxonomies' => [
            'display' => 'Excluir Taxonomias',
            'instruct' => 'Selecione as taxonomias que você gostaria de excluir do mapa do site.',
        ],
    ]
];
