<?php

return [

    'singular' => 'Marketing',
    'plural' => 'Marketings',

    // CP
    'fields' => [
        'gtm_section' => [
            'display' => 'Google Tag Manager',
            'instruct' => 'Gerencie suas configurações do gerenciador de tags do Google aqui.'
        ],
        'enable_gtm_script' => [
            'display' => 'Habilitar Google Tag Manager Script',
            'instruct' => 'Alterne se o script GTM é colocado no site.'
        ],
        'gtm_identifier' => [
            'display' => 'Google Tag Manager ID',
            'instruct' => 'Copie seu identificador de gerenciador de tags do Google aqui.',
        ],
        'site_verification_section' => [
            'display' => 'Site Verification',
        ],
        'google_verification_code' => [
            'display' => 'Google Verification Code',
        ],
        'bing_verification_code' => [
            'display' => 'Bing Verification Code',
        ],
        'yandex_verification_code' => [
            'display' => 'Yandex Verification Code',
        ],
        'baidu_verification_code' => [
            'display' => 'Baidu Verification Code',
        ],
    ]
];
