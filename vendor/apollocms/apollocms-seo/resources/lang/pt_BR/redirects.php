<?php

return [

    'singular' => 'Redirecionamento',
    'plural' => 'Redirecionamentos',

    // Control Panel
    'index' => 'Confgurações dos Redirecionamentos',

    // Types
    'manual' => [
        'singular' => 'Redirecionamento Manual',
        'plural' => 'Redirecionamentos Manuais'
    ],

    // Redirect attributes
    'redirect' => [
        'source_url' => 'Antigo URL',
        'target_url' => 'Novo URL',
        'status_code' => 'Status Code',
        'is_active' => 'Está Ativo?'
    ],

    // Permissions
    'permissions' => [
        'view' => 'Visualizar Redirecionamentos',
        'edit' => 'Editar Redirecionamentos',
        'create' => 'Criar Redirecionamentos'
    ],

    // Pages
    'pages' => [
        'create' => 'Criar redirecionamento',
        'edit' => 'Editar redirecionamento',
        'manual' => 'Redirecionamentos manuais',
    ],

    // Actions
    'actions' => [
        'create' => 'Criar redirecionamento',
        'manual' => 'Redirecionamentos manuais'
    ]

];
