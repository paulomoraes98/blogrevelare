<?php

return [

    'singular' => 'Geral',
    'plural' => 'Gerais',

    // Control Panel
    'index' => 'Configurações Gerais',

    'fields' => [
        'titles_section' => [
            'display' => 'Títulos',
            'instruct' => 'Controle como os títulos do seu site aparecem',
        ],
        'title_separator' => [
            'display' => 'Separador de Título',
            'instruct' => 'Defina o caractere para separar os nomes do site e da página no meta título'
        ],
        'site_name' => [
            'display' => 'Nome do Website',
            'instruct' => 'Defina o nome do site. Isso será usado em meta títulos gerados, bem como na propriedade do nome do site OpenGraph'
        ],
        'favicon_section' => [
            'display' => 'Favicon',
            'instruct' => 'Faça upload de um favicon para mostrar nos resultados da pesquisa e no navegador. Recomenda-se que seu favicon seja: <ul> <li> Um múltiplo de 48px quadrado nas dimensões </li> <li> Um formato de arquivo favicon compatível, recomendamos o uso de `.png`</li></ul>'
        ],
        'global_favicon' => [
            'display' => 'Global Favicon',
        ],
        'knowledge_graph_section' => [
            'display' => 'Base Knowledge Graph Data',
        ],
        'company_or_person' => [
            'display' => 'Empresa ou Pessoa Física?',
            'instruct' => 'Selecione se o conteúdo deste site representa uma empresa ou uma pessoa'
        ],
        'target_name' => [
            'display' => 'Nome da Empresa ou Pessoa',
            'instruct' => 'Insira o nome da pessoa / empresa aqui'
        ],
        'company_logo' => [
            'display' => 'Logo da Empresa',
        ],
        'breadcrumbs_section' => [
            'display' => 'Breadcrumbs',
            'instruct' => 'Habilitar breadcrumbs schema para páginas (veja [https://developers.google.com/search/docs/data-types/breadcrumb](https://developers.google.com/search/docs/data-types/breadcrumb)).',
        ],
        'enable_breadcrumbs' => [
            'display' => 'Habilitar Breadcrumbs?',
        ],
        'no_index_section' => [
            'display' => 'No Index',
            'instruct' => 'Defina como `true` para excluir o **site inteiro** da indexação do mecanismo de pesquisa - isso também pode ser configurado por página.',
        ],
        'no_index_site' => [
            'display' => 'No Index',
            'instruct' => 'Impeça a indexação em todo o site.',
        ],
    ]

];
