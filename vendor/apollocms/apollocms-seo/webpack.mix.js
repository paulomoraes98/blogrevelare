const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js/apollocms-seo.js').vue({ version: 2 });
mix.styles('resources/css/app.css', 'public/css/apollocms-seo.css');
