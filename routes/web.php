<?php

use App\Http\Controllers\LikeController;
use App\Http\Controllers\ArtigoController;

Route::post('artigos/{id}/like', [LikeController::class, 'like'] )->name('like');
Route::post('artigos/{id}/unlike', [LikeController::class, 'unlike'] )->name('unlike');
Route::get('artigos/listar', [ArtigoController::class, 'index'] )->name('index');