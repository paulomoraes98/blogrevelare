/* Like */
let botao = document.querySelector('button[slug="like"]');

document.querySelectorAll('.btn_like').forEach(button => {
    button.addEventListener('click', (event) => {
        event.preventDefault();

        const postSlug = event.target?.dataset?.postSlug;
        console.log(document.querySelector('input[name="_token"]').value);

        if(!postSlug) return;

        likeSave(postSlug);
    })
});

function likeSave(slug){
    if(!slug) return;

    setLike(slug);
}

function setLike(slug, ajax = true) {
    let likes = JSON.parse(localStorage.getItem("likes_blog_revelare"));

    if(likes) {
        if(likes[slug]){
            likes[slug] = false;
            if(ajax) unlike(slug);
        }else{
            likes[slug] = true;
            if(ajax) like(slug);
        }
    } else {
        likes = {
            [slug]: true
        };
        if(ajax) like(slug);
    }

    if(likes[slug]){
        document.querySelector(`button[data-post-slug="${slug}"]`).classList.add('liked');
    }else{
        document.querySelector(`button[data-post-slug="${slug}"]`).classList.remove('liked');
    }

    localStorage.setItem("likes_blog_revelare", JSON.stringify(likes));
}

function verifyLike(){
    const slug = document.querySelector('input[name="slug"]').value;

    if(!slug) return;

    let likes = JSON.parse(localStorage.getItem("likes_blog_revelare"));

    if(likes) {
        if(likes[slug]){
            document.querySelector(`button[data-post-slug="${slug}"]`).classList.add('liked');
        }else{
            document.querySelector(`button[data-post-slug="${slug}"]`).classList.remove('liked');
        }
    }



    localStorage.setItem("likes_blog_revelare", JSON.stringify(likes));
}

const like = async (slug) => {
    const header = new Headers();
    header.append('X-CSRF-TOKEN', document.querySelector('input[name="_token"]').value);

    console.log(header);
    const response = await fetch(`${window.location.origin}/artigos/${slug}/like`, {
        method: 'post',
        headers: header
    });

    if(response.status !== 200){
        setLike(slug, false);
    }
};
const unlike = async (slug) => {
    const header = new Headers();
    header.append('X-CSRF-TOKEN', document.querySelector('input[name="_token"]').value);

    console.log(header);

    const response = await fetch(`${window.location.origin}/artigos/${slug}/unlike`, {
        method: 'post',
        headers: header
    });

    if(response.status !== 200){
        setLike(slug, false);
    }
}

document.addEventListener('DOMContentLoaded', () => {
    verifyLike();
})
/* /Like */

/* Listar */
/*const listarArtigos = async ($qtd) => {
    $qtd = document.getElementById("listar").rows.length;
    //console.log($qtd);
    const dados = await fetch('listar/qtd/' + $qtd);
}

listarArtigos();*/