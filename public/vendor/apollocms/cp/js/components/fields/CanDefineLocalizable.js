export default {

    props: {
        canDefineLocalizable: {
            type: Boolean,
            default: () => {
                return ApolloCMS.$config.get('sites').length > 1;
            }
        }
    }

}
