export default class FieldConditions {

    add(name, condition) {
        ApolloCMS.$store.commit('apollocms/condition', {name, condition});
    }

}
