<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ApolloCMS\Entries\Entry;
use Symfony\Component\Console\Input\Input;

class LikeController extends Controller
{
    public function like($id) {
        $entries=
        Entry::query()
        ->where('collection', 'artigos')
        ->where('slug', $id)->first();

        $entries->like += 1;

        $entries->save();

        try{
            return back();
        }catch(Exception $e){
            return back();
        }
    }

    public function unlike($id) {
        $entries=
        Entry::query()
        ->where('collection', 'artigos')
        ->where('slug', $id)->first();

        $entries->like -= 1;

        if($entries->like < 0) $entries->like = 0;

        $entries->save();

        try{
            return back();
        }catch(Exception $e){
            return back();
        }
    }
}
