<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ApolloCMS\Entries\Entry;
use Symfony\Component\Console\Input\Input;

class ArtigoController extends Controller
{
    public function index(Request $request) {
        $artigos = Entry::query()
        ->where('collection', 'artigos')
        ->paginate(3);

        return view('teste', ['artigos' => $artigos]);
        // ->with('artigos',$artigos); 
    }
}