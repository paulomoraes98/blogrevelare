/**
 * When extending the control panel, be sure to uncomment the necessary code for your build process:
 * https://apollocms.siteturbo.com.br/extending/control-panel
 */

/** Example Fieldtype

import ExampleFieldtype from './components/fieldtypes/ExampleFieldtype.vue';

ApolloCMS.booting(() => {
    ApolloCMS.$components.register('example-fieldtype', ExampleFieldtype);
});
*/

ApolloCMS.booting(() => {
    ApolloCMS.$conditions.add('superadmin', () => ApolloCMS.user.super);
});
